#ifndef CONCODE_H
#define	CONCODE_H

//Device classes
#define fcoil_device_class                      13          
#define bank_48V_device_class                   14
#define LCD_device_class                        15
#define sentinel_device_class                   16
#define bank_24V_device_class                   17
#define bank_12V_device_class                   18
#define BPG_device_class                        19
#define panic_device_class                      20
#define TQ_BMS_device_class                     24

#define TPS_bank_device_class                   100
#define TPS_field_device_class                  101

//SPI bus speed constants
#define SPI_FAST                        0
#define SPI_NORMAL                      1
#define SPI_SLOW                        2
#define SPI_MAX                         3

//Result codes for "CAN_buffer_check"
#define CAN_BUFFER_EMPTY                0
#define CAN_BUFFER0_FULL                1
#define CAN_BUFFER1_FULL                2
#define CAN_BUFFER0_1_FULL              3

//Result codes for "CAN_interval_accumulator
#define CAN_ACC_UPDATED                 0
#define CAN_ACC_ADDED                   1
#define CAN_ACC_FULL                    2
#define CAN_ACC_INVALID_DATA            3
#define CAN_ACC_SUCCESS                 4

//Result codes for serial streams
#define STRING_BUFFER_EMPTY             256
#define STRING_BUFFER_FULL              1

// CAN available
#define Argo_CAN                        0
#define Lithium_CAN                     1

#define GLOBAL_NO_ACTION                0   
#define GLOBAL_OFF                      1
#define GLOBAL_ON                       2

//Battery chemistry list
#define CHEM_LION                       0
#define CHEM_AGM                        1
#define CHEM_FLOOD                      2
#define CHEM_GEL                        3
#define CHEM_CF_AGM                     4
#define CHEM_TPPL_AGM                   5

#define UART_RS232                      1
#define UART_RS485                      2

#define INTERFACE_NONE                  0
#define INTERFACE_CAN                   1
#define INTERFACE_RS232                 2
#define INTERFACE_RS485                 3
#define INTERFACE_IO                    4

#define VICTRON                         1
    #define SMART_256V                  1

#define TORQEEDO                        2
    #define POWER26_104                 1
    #define POWER3500                   2

#define LITHIONICS                      3
    #define NEVERDIE                    1

#define GENZ                            4
#define MASTERVOLT                      5
#define SUPERB                          6

#define BMS_OK                          0
#define BMS_BANK_ON                     1
#define BMS_BANK_OFF                    2
#define BMS_48HR_ON                     3
#define BMS_48HR_OFF                    4
#define BMS_STORAGE_ON                  5
#define BMS_STORAGE_OFF                 6


#define CURRENT_MIN                     0
#define CURRENT_MAX                     1

#define CHARGE_START                    0
#define CHARGE_STOP                     1

#define SWITCH_OK                       0
#define SWITCH_STUCK_ON                 1
#define SWITCH_STUCK_OFF                2
#define SWITCH_POWER_OFF                3

#define ALARM_NONE                      0
#define ALARM_SIGNAL1_NO_DISCHARGE      1
#define ALARM_SIGNAL2_NO_CHARGE         2
#define ALARM_HIGH_VOLTAGE              3
#define ALARM_LOW_VOLTAGE               4
#define ALARM_HIGH_CURRENT              5
#define ALARM_HIGH_TEMPERATURE_AMB      6
#define ALARM_LOW_TEMPERATURE_AMB       7
#define ALARM_USER_DISCONNECT           8
#define ALARM_PANIC_DISCONNECT          9
#define ALARM_HIGH_TEMPERATURE_BAT      10
#define ALARM_LOW_TEMPERATURE_BAT       11
#define ALARM_MISSING_TEMPERATURE       12
#define ALARM_SWITCH_STUCK_ON           13
#define ALARM_SWITCH_STUCK_OFF          14
#define ALARM_BATTERY_MISSING           15
#define ALARM_BMS_ERROR                 16
#define ALARM_BMS_BLOCK_MISSING         17
#define ALARM_BMS_ENUM_FAILED           18
#define ALARM_BMS_ENUM_TIMEOUT          19
#define ALARM_BMS_WARNING               20


#define RS485_DATA_OK                   0
#define RS485_DATA_INVALID              1
#define RS485_DATA_LENGTH_ERROR         2
#define RS485_DATA_MISSING              3

#define TQ_ID_GROUP_RECORD              0x01
#define TQ_STATUS_RECORD                0x04
#define TQ_TEMPERATURE_RECORD           0x20
#define TQ_VOLTAGE_RECORD               0x21
#define TQ_ERROR_COUNT_RECORD           0x22

#endif	

