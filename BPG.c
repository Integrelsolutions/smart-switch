/*
Battery protection gateway HV1.000
Author: W.G
Company: Triskel Marine Ltd
Date: 01/03/19
*/ 

//Compiler directives
#include <p24FJ128GA204.h>

#include "xc.h"
#include "BPG.h"
#include "FRAM_variables.h"
#include "constant_codes.h"
#include "Version.h"
#include "math.h"
#include "libpic30.h"
#include "string.h"
#include "stdio.h"
#include "stdlib.h"
//config

// CONFIG4
#pragma config DSWDTPS = DSWDTPS1F      // Deep Sleep Watchdog Timer Postscale Select bits (1:68719476736 (25.7 Days))
#pragma config DSWDTOSC = LPRC          // DSWDT Reference Clock Select (DSWDT uses LPRC as reference clock)
#pragma config DSBOREN = ON             // Deep Sleep BOR Enable bit (DSBOR Enabled)
#pragma config DSWDTEN = ON             // Deep Sleep Watchdog Timer Enable (DSWDT Enabled)
#pragma config DSSWEN = ON              // DSEN Bit Enable (Deep Sleep is controlled by the register bit DSEN)
#pragma config PLLDIV = PLL4X           // USB 96 MHz PLL Prescaler Select bits (PLL x4)
#pragma config I2C1SEL = DISABLE        // Alternate I2C1 enable bit (I2C1 uses SL1 and SDA1 pins)
#pragma config IOL1WAY = ON             // PPS IOLOCK Set Only Once Enable bit (Once set, the IOLOCK bit cannot be cleared)

// CONFIG3
#pragma config WPFP = WPFP127           // Write Protection Flash Page Segment Boundary (Page 127 (0x1FC00))
#pragma config SOSCSEL = OFF             // SOSC Selection bits (SOSC circuit selected)
#pragma config WDTWIN = PS25_0          // Window Mode Watchdog Timer Window Width Select (Watch Dog Timer Window Width is 25 percent)
#pragma config PLLSS = PLL_PRI          // PLL Secondary Selection Configuration bit (PLL is fed by the Primary oscillator)
#pragma config BOREN = ON               // Brown-out Reset Enable (Brown-out Reset Enable)
#pragma config WPDIS = WPDIS            // Segment Write Protection Disable (Disabled)
#pragma config WPCFG = WPCFGDIS         // Write Protect Configuration Page Select (Disabled)
#pragma config WPEND = WPENDMEM         // Segment Write Protection End Page Select (Write Protect from WPFP to the last page of memory)

// CONFIG2
#pragma config POSCMD = NONE            // Primary Oscillator Select (Primary Oscillator Disabled)
#pragma config WDTCLK = LPRC            // WDT Clock Source Select bits (WDT uses LPRC)
#pragma config OSCIOFCN = ON           // OSCO Pin Configuration (OSCO/CLKO/RA3 functions as CLKO (FOSC/2))
#pragma config FCKSM = CSDCMD           // Clock Switching and Fail-Safe Clock Monitor Configuration bits (Clock switching and Fail-Safe Clock Monitor are disabled)
#pragma config FNOSC = FRCPLL           // Initial Oscillator Select (Fast RC Oscillator with Postscaler (FRCDIV))
#pragma config ALTCMPI = CxINC_RB       // Alternate Comparator Input bit (C1INC is on RB13, C2INC is on RB9 and C3INC is on RA0)
#pragma config WDTCMX = WDTCLK          // WDT Clock Source Select bits (WDT clock source is determined by the WDTCLK Configuration bits)
#pragma config IESO = ON                // Internal External Switchover (Enabled)

// CONFIG1
#pragma config WDTPS = PS32768          // Watchdog Timer Postscaler Select (1:32,768)
#pragma config FWPSA = PR128            // WDT Prescaler Ratio Select (1:128)
#pragma config WINDIS = OFF             // Windowed WDT Disable (Standard Watchdog Timer)
#pragma config FWDTEN = OFF             // Watchdog Timer Disable (WDT enabled in hardware)
#pragma config ICS = PGx1               // Emulator Pin Placement Select bits (Emulator functions are shared with PGEC1/PGED1)
#pragma config LPCFG = OFF              // Low power regulator control (Disabled - regardless of RETEN)
#pragma config GWRP = OFF               // General Segment Write Protect (Write to program memory allowed)
#pragma config GCP = ON                 // General Segment Code Protect (Code protection is enabled)
#pragma config JTAGEN = OFF             // JTAG Port Enable (Enabled)

// #pragma config statements should precede project file includes.
// Use project enums instead of #define for ON and OFF.



//Variable declaration
unsigned long long int EUI48;                           //EUI48 number
unsigned short int unique_16bit_ID, missing_block_timeout;                     //16bit unique ID for PGN field
unsigned char Lithium_present, alarm_condition, sleep_mode;
unsigned char updated_parameter_status, switch_stuck, switch_retry, latest_block_count;
unsigned short int power_save_mode;
unsigned char torqeedo_data_TX[255];
unsigned char torqeedo_data_RX[255];
double battery_missing_voltage;
CAN_data_packet CAN_buffer_store[2][2], CAN_send_packet;                    //CAN data storage for the two onboard MCP2515 buffers
battery_bank_data bank_data[bank_sensor_max];
field_coil_data fcoil_data[fcoil_max];
battery_protection_gateway_data BPG_data[BPG_max];
battery_protection_gateway_parameters new_parameters;
Torqeedo_BMS_data TQ_BMS_data;
Torqeedo_BMS_ID TQ_BMS_ID_group[16];
panic_data panic_commands;
//--------------------------------------------
// Main loop
// Entry: None
// Exit: Never
//--------------------------------------------
int main(void)
{    

    unsigned short int no_Argo_data_counter, master_search;
    unsigned char buffer_state, Argo_data_present, switch_pressed, debounce_time_out, signal1_io_timer, signal2_io_timer;
    unsigned char temperature_bat_low_timer, temperature_bat_high_timer, temperature_amb_low_timer, temperature_amb_high_timer, voltage_low_timer, voltage_high_timer, current_high_timer, new_parameters_received, current_parameter_counter;
    unsigned char temperature_bat_low_reset_timer, temperature_bat_high_reset_timer, temperature_amb_low_reset_timer, temperature_amb_high_reset_timer, voltage_low_reset_timer, voltage_high_reset_timer, current_high_reset_timer;
    unsigned char ID_loop, updated_parameter_status, log_timer, general_data, send_message, block_cycle, active_block_count;

    
    
        CLKDIVbits.RCDIV = 0;
        CLKDIVbits.DOZEN = 0;
                                                                //Turn off un-used PIC peripherals
        AD1CON1bits.ADON = 0;                                   //All analogue inputs off (they're on by default)                            
        AD1CSSL = 0;
        ANSA = 0x00;
        ANSB = 0x00;
        ANSC = 0x00;
        LATA = 0;                                               //Latches off
        LATB = 0;
        LATC = 0;
        //CNPU2bits.CN24PUE = 1;
        //CNPU2bits.CN23PUE = 1;        
                                                                //Chip pin setup and descriptions
        TRISBbits.TRISB9 = 0;                                   // 1. SDA 1  (out)
        TRISCbits.TRISC6 = 0;                                   // 2. Relay 1 (out)
        TRISCbits.TRISC7 = 0;                                   // 3. Relay 2 (out)
        TRISCbits.TRISC8 = 0;                                   // 4. TQ power off OD (out))
        TRISCbits.TRISC9 = 0;                                   // 5. Diode swap (out)
                                                                // 6. Vbat
                                                                // 7. Vcap
        TRISBbits.TRISB10 = 0;                                  // 8. MOSFET short (out)
        TRISBbits.TRISB11 = 0;                                  // 9. Pre-charge relay (out)
        TRISBbits.TRISB12 = 1;                                  // 10. BMS signal 1 sense ANA8 (in)
        TRISBbits.TRISB13 = 1;                                  // 11. BMS signal 2 sense ANA7 (in)
        TRISAbits.TRISA10 = 1;                                  // 12. Factory reset (in)
        TRISAbits.TRISA7 = 1;                                   // 13. Switch position (in)
        TRISBbits.TRISB14 = 1;                                  // 14. 48V power sense ANA6 (in)
        TRISBbits.TRISB15 = 0;                                  // 15. Switch position LED (out)
                                                                // 16. AVss
                                                                // 17. AVdd
                                                                // 18. MCLR
        TRISAbits.TRISA0 = 1;                                   // 19. Battery thermistor 1 ANA0 (in)
        TRISAbits.TRISA1 = 1;                                   // 20. Battery thermistor 2 ANA1 (in)
        TRISBbits.TRISB0 = 0;                                   // 21. PGD1/Power LED (out)
        TRISBbits.TRISB1 = 0;                                   // 22. PGC1 (out)
        TRISBbits.TRISB2 = 0;                                   // 23. Argo CAN select (out)
        TRISBbits.TRISB3 = 0;                                   // 24. FRAM select (out)
        TRISCbits.TRISC0 = 0;                                   // 25. MOSI (out)
        TRISCbits.TRISC1 = 0;                                   // 26. MCLK (out)
        TRISCbits.TRISC2 = 1;                                   // 27. MISO (in)
                                                                // 28. Vdd
                                                                // 29. Vss
        TRISAbits.TRISA2 = 0;                                   // 30. Battery CAN select (out)    
        TRISAbits.TRISA3 = 0;                                   // 31. Regulator CAN control (out)
        TRISAbits.TRISA8 = 0;                                   // 32. RS485 control (out)
        //TRISBbits.TRISB4 = 1;                                 // 33. N/C (in)
        //TRISAbits.TRISA4 = 1;                                 // 34. Remote switch sense (in)
        TRISAbits.TRISA9 = 0;                                   // 35. Battery CAN control (out)
        TRISCbits.TRISC3 = 0;                                   // 36. Generial I/O (Molex)(out)
        TRISCbits.TRISC4 = 0;                                   // 37. RS485 TX RP20 (out)
        TRISCbits.TRISC5 = 1;                                   // 38. RS485 RX RP21 (in)
                                                                // 39. Vss
                                                                // 40. Vdd
        TRISBbits.TRISB5 = 0;                                   // 41. General I/O (jumper) (out)
        TRISBbits.TRISB6 = 0;                                   // 42. Remote LED (out)
        TRISBbits.TRISB7 = 0;                                   // 43. RS485 direction (out)
        TRISBbits.TRISB8 = 0;                                   // 44. SCL 1 (out)

        relay1_control = relay_off;
        relay2_control = relay_off;
                                                                //Set up PIC peripherals we are using
        __builtin_write_OSCCONL(OSCCON & 0xBF);                 //Unlock to allow pin changes

            RPINR19bits.U2RXR = 21;					//Maps RS485 UART2 RX to RP21 (Pin 38 RX)
            RPOR10bits.RP20R = 5;             		//Maps RS485 UART2 TX to RP20 (Pin 37 TX)            
            
            RPINR20bits.SDI1R = 18;					//Maps SPI1 SDI to RP18 (Pin 27 SDI)
            RPOR8bits.RP16R = 7;					//Maps SPI1 SDO to RP16 (Pin 25 SDO)
            RPOR8bits.RP17R = 8;                    //Maps SPI1 SCK to RP17 (Pin 26 SCK)
           
            
                        
        __builtin_write_OSCCONL(OSCCON & 0x40);                 //Lock to protect pin changes        
        


        BPG_init();
        memset(&bank_data,0,sizeof(bank_data));
        memset(&fcoil_data,0,sizeof(fcoil_data));
        memset(&new_parameters,0,sizeof(new_parameters));
        memset(&BPG_data, 0, sizeof(BPG_data));
        memset(&panic_commands, 0, sizeof(panic_commands));
        
        load_FRAM_into_globals();

        battery_missing_voltage = BPG_data[0].low_voltage_disconnect / 2;
        no_Argo_data_counter = 0;
        Argo_data_present = 0;
        switch_pressed = 0;
        debounce_time_out = 0;
        signal1_io_timer = 0;
        signal2_io_timer = 0;
        voltage_low_timer = 0;
        temperature_amb_low_timer = 0;
        voltage_high_timer = 0;
        temperature_amb_high_timer = 0;
        temperature_bat_low_timer = 0;
        temperature_bat_high_timer = 0;
        voltage_low_reset_timer = 0;
        voltage_high_reset_timer = 0;
        temperature_amb_low_reset_timer = 0;
        temperature_amb_high_reset_timer = 0;
        temperature_bat_low_reset_timer = 0;
        temperature_bat_high_reset_timer = 0;
        log_timer = 0;
        current_high_reset_timer = 0;
        switch_stuck = SWITCH_OK;
        switch_retry = 0;
        current_parameter_counter = 0;
        current_high_timer = 0;
        missing_block_timeout = 0;
        sleep_mode = 0;
        new_parameters_received = 0;
        updated_parameter_status = 0;
        alarm_condition = ALARM_NONE;
        power_save_mode = 0;
        block_cycle = 1;
        active_block_count = BPG_data[0].BMS_block_count;
        latest_block_count = active_block_count;
        if (auto_detect_BMS_enabled) FRAM_use_interface_io(0,1); 
        timer_setup(1);                       
        while(1) 
        {            
            if (FRAM_interface_type(0,0) == INTERFACE_CAN) incoming_Lithium_poll();
            if (FRAM_interface_type(0,0) == INTERFACE_RS232) incoming_RS232_poll();

            buffer_state = incoming_Argo_poll(); //Check for field coil, bank data and other BPGs
           
            if (buffer_state != CAN_BUFFER_EMPTY) Argo_data_present = 1;

            if (debounce_time_out == 0)         //Allow the remote push to make switch to toggle the position with noise debounce to stop chatter
            {
                if ((FRAM_allow_remote_switch(0,0)) && (remote_override_switch == 0)) switch_pressed = 1;
                if ((FRAM_allow_remote_switch(0,0)) && (remote_override_switch == 1) && (switch_pressed == 1))
                {
                    switch_pressed = 0;
                    
                    if (remote_switch_controls_TQ)
                    {
                        if (BPG_data[0].BMS_live_block_count > 0) 
                        {
                            send_BMS_RS485_command(BMS_BANK_OFF);                            
                        } else {
                            send_BMS_RS485_command(BMS_BANK_ON);                            
                        }
                    } else {
                        operate_switch(!switch_position, GLOBAL_NO_ACTION);
                        BPG_data[0].last_disconnect_reason = ALARM_USER_DISCONNECT;
                        
                    }
                    debounce_time_out = 1;
                }
            }

                                
            
            //1 second loop starts here
    
            if (timer_trigger_flag)
            {               
                timer_setup(1);               
                
                BPG_data[0].bank_switch_voltage = PIC_ADC_voltage_read(bank_voltage_channel);
                BPG_data[0].IO_signal1_voltage_no_discharge = PIC_ADC_voltage_read(signal1_voltage_channel);
                BPG_data[0].IO_signal2_voltage_no_charge = PIC_ADC_voltage_read(signal2_voltage_channel);
                BPG_data[0].ambient_temperature = read_PCB_temperature();
                BPG_data[0].battery_thermistor_1 = PIC_ADC_temperature_read(battery_thermistor_1_channel);
                BPG_data[0].battery_thermistor_2 = PIC_ADC_temperature_read(battery_thermistor_2_channel);
                BPG_data[0].maximum_bank_current = scan_bank_current(CURRENT_MAX);
                BPG_data[0].minimum_bank_current = scan_bank_current(CURRENT_MIN);
                BPG_data[0].current_switch_position = switch_position;
                BPG_data[0].fan_state = 0;


                if (BPG_data[0].interface_type_to_use == INTERFACE_RS485) 
                {
                    if (BPG_data[0].BMS_block_count > 0)
                    {
                        if (BMS_query(block_cycle) == 0)
                        {
                            //battery missing
                            active_block_count = active_block_count - 1;
                        }
                        block_cycle++;
                        if (block_cycle > BPG_data[0].BMS_block_count) 
                        {
                            block_cycle = 1;
                            BPG_data[0].BMS_live_block_count = active_block_count;
                            if ((active_block_count < BPG_data[0].BMS_block_count) && (active_block_count != 0))
                            {
                                if (missing_block_timeout > block_timeout_period)
                                {
                                    latest_block_count = active_block_count;
                                    active_block_count = BPG_data[0].BMS_block_count;
                                    alarm_condition = ALARM_BMS_BLOCK_MISSING;
                                } else {
                                    missing_block_timeout++;
                                    latest_block_count = BPG_data[0].BMS_block_count;
                                    active_block_count = BPG_data[0].BMS_block_count;
                                }
                            } else {
                                missing_block_timeout = 0;
                                if (alarm_condition == ALARM_BMS_BLOCK_MISSING) alarm_condition = ALARM_NONE;
                                latest_block_count = BPG_data[0].BMS_block_count;
                                active_block_count = BPG_data[0].BMS_block_count;
                            }
                        }
                    }                           
                }
                
                if ((BPG_data[0].IO_signal1_voltage_no_discharge > FRAM_io_trigger_voltage(0,0)) || (BPG_data[0].IO_signal2_voltage_no_charge > FRAM_io_trigger_voltage(0,0)))
                {
                    if ((auto_detect_BMS_enabled) && (FRAM_use_interface_io(0,0) == 0)) FRAM_use_interface_io(1,1);                    
                }
                
                if (remote_switch_controls_TQ)
                {
                    if (BPG_data[0].BMS_live_block_count == 0) 
                    {
                        LED_remote = 0;
                    } else {
                        LED_remote = 1;
                    }                
                } else {
                    if (power_save_mode == 0)
                    {
                        if (switch_position)
                        {
                            LED_switch_position = 1;
                            LED_remote = 1;

                        } else {
                            LED_switch_position = 0;
                            LED_remote = 0;
                        }
                    } else {
                        if (switch_position) 
                        {
                            LED_switch_position = !LED_switch_position;
                            LED_remote = !LED_remote;
                        }
                    }
                }
                
                //Manual toggle has resolved a stuck switch so clear result
                if ((switch_position == 0) && (switch_stuck == SWITCH_STUCK_ON))
                {
                    switch_retry = 0;
                    switch_stuck = SWITCH_OK;
                    if (alarm_condition == ALARM_SWITCH_STUCK_ON) alarm_condition = ALARM_NONE;
                }
                if ((switch_position == 1) && (switch_stuck == SWITCH_STUCK_OFF))
                {
                    switch_retry = 0;
                    switch_stuck = SWITCH_OK;
                    if (alarm_condition == ALARM_SWITCH_STUCK_OFF) alarm_condition = ALARM_NONE;
                }
                //Battery's have reconnected so clear condition
                if ((switch_stuck == SWITCH_POWER_OFF) && (BPG_data[0].bank_switch_voltage > battery_missing_voltage)) 
                {
                    switch_retry = 0;
                    switch_stuck = SWITCH_OK;
                }

                check_factory_reset_switch();
                //Thermistor1,2 and fan state
                if (debounce_time_out == 1)
                {
                    debounce_time_out++;
                } else {
                    if (debounce_time_out == 2) debounce_time_out = 0;
                }
                

                //alarm_condition = ALARM_NONE;

                if (Argo_data_present == 0) 
                {
                    if (no_Argo_data_counter < FRAM_no_Argo_activity_sleep(0,0))
                    {
                        no_Argo_data_counter++;
                    } else {
                        //Place BPG into sleep mode with wake up monitoring of voltage
                        power_save_mode = 1;
                        LED_power = 0;
                        LED_active = 0;                        
                    }
                } else {
                    no_Argo_data_counter = 0;
                    power_save_mode = 0;
                }

                //Check local alarm conditions
                if (FRAM_use_interface_io(0,0))
                {

                    //low voltage disconnect needs to be overridden if RPM is detected
                    if (BPG_data[0].IO_signal1_voltage_no_discharge < FRAM_io_trigger_voltage(0,0))
                    {
                        if (signal1_io_timer == 0) signal1_io_timer = 1;
                    } else {
                        signal1_io_timer = 0;
                        if (alarm_condition == ALARM_SIGNAL1_NO_DISCHARGE) alarm_condition = ALARM_NONE;
                    }
                    if (BPG_data[0].IO_signal2_voltage_no_charge < FRAM_io_trigger_voltage(0,0))
                    {
                        if (signal2_io_timer == 0) signal2_io_timer = 1;
                    } else {
                        signal2_io_timer = 0;
                        if (alarm_condition == ALARM_SIGNAL2_NO_CHARGE) 
                        {
                            alarm_condition = ALARM_NONE;                        
                            for (send_message = 0; send_message < 10; send_message++)
                            {
                                send_charge_control(CHARGE_START);
                                n_second_delay(1);
                            }
                        }
                    }

                    if (signal1_io_timer != 0) 
                    {
                        if (signal1_io_timer > FRAM_io_activation_delay(0,0))
                        {                           
                            if (switch_position) operate_switch(0, GLOBAL_NO_ACTION);
                            alarm_condition = ALARM_SIGNAL1_NO_DISCHARGE;
                        } else {
                            
                            signal1_io_timer++;
                        }
                    }
                    if (signal2_io_timer != 0) 
                    {
                        if (signal2_io_timer > FRAM_io_activation_delay(0,0))
                        {
                            //if (switch_position) operate_switch(0);
                            send_charge_control(CHARGE_STOP);
                            alarm_condition = ALARM_SIGNAL2_NO_CHARGE;
                        } else {
                            signal2_io_timer++;
                        }
                    }
                    
                }
                if ((BPG_data[0].battery_thermistor_1 > FRAM_high_temperature_disconnect(0,0)) || (BPG_data[0].battery_thermistor_2 > FRAM_high_temperature_disconnect(0,0)))
                {
                    if (temperature_bat_high_timer == 0) temperature_bat_high_timer = 1;
                } else {
                    temperature_bat_high_timer = 0;
                    if (alarm_condition == ALARM_HIGH_TEMPERATURE_BAT) 
                    {
                        temperature_bat_high_reset_timer++;
                        if (temperature_bat_high_reset_timer == alarm_persistence_time)
                        {
                            alarm_condition = ALARM_NONE;
                            temperature_bat_high_reset_timer = 0;
                        }
                    }
                }
                if ((BPG_data[0].battery_thermistor_1 < FRAM_low_temperature_disconnect(0,0)) || (BPG_data[0].battery_thermistor_2 < FRAM_low_temperature_disconnect(0,0)))
                {
                    if (temperature_bat_low_timer == 0) temperature_bat_low_timer = 1;
                } else {
                    temperature_bat_low_timer = 0;
                    if (alarm_condition == ALARM_LOW_TEMPERATURE_BAT)
                    {
                        temperature_bat_low_reset_timer++;
                        if (temperature_bat_low_reset_timer == alarm_persistence_time)
                        {
                            alarm_condition = ALARM_NONE;
                            temperature_bat_low_reset_timer = 0;
                        }
                    }
                }                
                if (BPG_data[0].ambient_temperature > ambient_max)
                {
                    if (temperature_amb_high_timer == 0) temperature_amb_high_timer = 1;
                } else {
                    temperature_amb_high_timer = 0;
                    if (alarm_condition == ALARM_HIGH_TEMPERATURE_AMB) 
                    {
                        temperature_amb_high_reset_timer++;
                        if (temperature_amb_high_reset_timer == alarm_persistence_time)
                        {
                            alarm_condition = ALARM_NONE;
                            temperature_amb_high_reset_timer = 0;
                        }
                    }
                }
                if (BPG_data[0].ambient_temperature < ambient_min)
                {
                    if (temperature_amb_low_timer == 0) temperature_amb_low_timer = 1;
                } else {
                    temperature_amb_low_timer = 0;
                    if (alarm_condition == ALARM_LOW_TEMPERATURE_AMB)
                    {
                        temperature_amb_low_reset_timer++;
                        if (temperature_amb_low_reset_timer == alarm_persistence_time)
                        {
                            alarm_condition = ALARM_NONE;
                            temperature_amb_low_reset_timer = 0;
                        }
                    }
                }
                if (BPG_data[0].bank_switch_voltage > FRAM_high_voltage_disconnect(0,0))
                {
                    if (voltage_high_timer == 0) voltage_high_timer = 1;
                } else {
                    voltage_high_timer = 0;
                    if (alarm_condition == ALARM_HIGH_VOLTAGE) 
                    {
                        voltage_high_reset_timer++;
                        if (voltage_high_reset_timer == alarm_persistence_time)
                        {
                            alarm_condition = ALARM_NONE;
                            voltage_high_reset_timer = 0;
                        }
                    }
                }
                if (BPG_data[0].bank_switch_voltage < FRAM_low_voltage_disconnect(0,0))
                {
                    if (voltage_low_timer == 0) voltage_low_timer = 1;
                } else {
                    voltage_low_timer = 0;
                    if (alarm_condition == ALARM_LOW_VOLTAGE) 
                    {
                        voltage_low_reset_timer++;
                        if (voltage_low_reset_timer == alarm_persistence_time)
                        {
                            alarm_condition = ALARM_NONE;
                            voltage_low_reset_timer = 0;
                        }
                    }
                }                

                if (BPG_data[0].maximum_bank_current > FRAM_high_current_disconnect(0,0))
                {
                    if (current_high_timer == 0) current_high_timer = 1;
                } else {
                    current_high_timer = 0;
                    if (alarm_condition == ALARM_HIGH_CURRENT) 
                    {
                        current_high_reset_timer++;
                        if (current_high_reset_timer == alarm_persistence_time)
                        {
                            alarm_condition = ALARM_NONE;
                            current_high_reset_timer = 0;
                        }
                    }
                }
                                              
                if (temperature_amb_high_timer != 0) 
                {
                    if (temperature_amb_high_timer > FRAM_temperature_time_hysteresis(0,0))
                    {                        
                        if (BPG_data[0].ambient_temperature > ambient_max)
                        {
                            if (switch_position) operate_switch(0,GLOBAL_NO_ACTION);
                            temperature_amb_high_reset_timer = 0;
                            alarm_condition = ALARM_HIGH_TEMPERATURE_AMB;                            
                        } 
                    } else {
                        temperature_amb_high_timer++;
                    }
                }
                if (temperature_amb_low_timer != 0) 
                {
                    if (temperature_amb_low_timer > FRAM_temperature_time_hysteresis(0,0))
                    {                        
                        if (BPG_data[0].ambient_temperature < ambient_min)
                        {
                            if (switch_position) operate_switch(0, GLOBAL_NO_ACTION);
                            temperature_amb_low_reset_timer = 0;
                            alarm_condition = ALARM_HIGH_TEMPERATURE_AMB;                            
                        } 
                    } else {
                        temperature_amb_low_timer++;
                    }
                }
                if (temperature_bat_high_timer != 0) 
                {
                    if (temperature_bat_high_timer > FRAM_temperature_time_hysteresis(0,0))
                    {                        
                        if ((BPG_data[0].battery_thermistor_1 > FRAM_high_temperature_disconnect(0,0)) || (BPG_data[0].battery_thermistor_2 > FRAM_high_temperature_disconnect(0,0)))
                        {
                            if (switch_position) operate_switch(0, GLOBAL_OFF);
                            temperature_bat_high_reset_timer = 0;
                            alarm_condition = ALARM_HIGH_TEMPERATURE_BAT;   
                         
                        } 
                    } else {
                        temperature_bat_high_timer++;
                    }
                }
                if (temperature_bat_low_timer != 0) 
                {
                    if (temperature_bat_low_timer > FRAM_temperature_time_hysteresis(0,0))
                    {                        
                        if ((BPG_data[0].battery_thermistor_1 < FRAM_low_temperature_disconnect(0,0)) || (BPG_data[0].battery_thermistor_2 < FRAM_low_temperature_disconnect(0,0)))
                        {
                            if (switch_position) operate_switch(0, GLOBAL_OFF);
                            temperature_bat_low_reset_timer = 0;
                            alarm_condition = ALARM_LOW_TEMPERATURE_BAT;                            
                        } 
                    } else {
                        temperature_bat_low_timer++;
                    }
                }
                
                if (voltage_high_timer != 0) 
                {
                    if (voltage_high_timer > 5)             //hard coded 5 second high voltage cut off
                    {                        
                        if (BPG_data[0].bank_switch_voltage > FRAM_high_voltage_disconnect(0,0))
                        {
                            if (switch_position) operate_switch(0, GLOBAL_OFF);
                            voltage_high_reset_timer = 0;
                            alarm_condition = ALARM_HIGH_VOLTAGE;                            
                        }                                              
                    } else {
                        voltage_high_timer++;
                    }
                }
                if (voltage_low_timer != 0) 
                {
                    if (voltage_low_timer > FRAM_voltage_time_hysteresis(0,0))
                    {                        
                        if (BPG_data[0].bank_switch_voltage < FRAM_low_voltage_disconnect(0,0))
                        {
                            if (switch_position) operate_switch(0, GLOBAL_NO_ACTION);
                            voltage_low_reset_timer = 0;
                            alarm_condition = ALARM_LOW_VOLTAGE;                            
                        }
                    } else {
                        voltage_low_timer++;
                    }
                }
                if (current_high_timer != 0) 
                {
                    if (current_high_timer > FRAM_current_time_hysteresis(0,0))
                    {                        
                        if (BPG_data[0].maximum_bank_current > FRAM_high_current_disconnect(0,0))
                        {
                            if (switch_position) operate_switch(0, GLOBAL_OFF);
                            current_high_reset_timer = 0;
                            alarm_condition = ALARM_HIGH_CURRENT;                            
                        }
                    } else {
                        current_high_timer++;
                    }
                }

                
                //Absolute overriding alarms, no hysteresis
                
                if ((BPG_data[0].battery_thermistor_1 < -60) || (BPG_data[0].battery_thermistor_2 < -60))
                {
                    alarm_condition = ALARM_MISSING_TEMPERATURE;
                } else {                    
                    if (alarm_condition == ALARM_MISSING_TEMPERATURE) alarm_condition = ALARM_NONE;
                }
                
                if (BPG_data[0].bank_switch_voltage < battery_missing_voltage)
                {
                    alarm_condition = ALARM_BATTERY_MISSING;                    
                } else {
                    if (alarm_condition == ALARM_BATTERY_MISSING) 
                    {
                        alarm_condition = ALARM_NONE;
                        switch_stuck = SWITCH_OK;
                        switch_retry = 0;
                    }
                }                
                
                if ((switch_position) && (alarm_condition == ALARM_LOW_VOLTAGE) && (switch_stuck == SWITCH_OK))
                {
                    voltage_low_timer = 0;
                    alarm_condition = ALARM_NONE;
                }
                if ((switch_position) && (alarm_condition == ALARM_LOW_TEMPERATURE_BAT) && (switch_stuck == SWITCH_OK))
                {
                    temperature_bat_low_timer = 0;
                    alarm_condition = ALARM_NONE;
                }                

                if (panic_commands.disconnect_battery_bank)
                {
                    if (switch_position) operate_switch(0, GLOBAL_NO_ACTION);
                    //alarm_condition = ALARM_PANIC_DISCONNECT;                            
                    panic_commands.disconnect_battery_bank = 0;
                }
                if (BPG_data[0].BMS_error != 0)
                {
                    if (switch_position) operate_switch(0, GLOBAL_NO_ACTION);
                    alarm_condition = ALARM_BMS_ERROR;                            
                    panic_commands.disconnect_battery_bank = 0;
                } else {
                    if (alarm_condition == ALARM_BMS_ERROR) alarm_condition = ALARM_NONE;
                }               
                
                BPG_data[0].alarm = alarm_condition;                
                
                if (alarm_condition != ALARM_NONE) BPG_data[0].last_disconnect_reason = alarm_condition;

                if ((alarm_condition == ALARM_NONE) && ((panic_commands.reconnect_battery_bank) || (panic_commands.reconnect_battery_bank_alarm_override)))
                {
                    if (switch_position == 0) operate_switch(1, GLOBAL_NO_ACTION);
                    panic_commands.reconnect_battery_bank = 0;
                    panic_commands.reconnect_battery_bank_alarm_override = 0;
                }
                

                              


                if (new_parameters.complete_parameter_set == BPG_parameter_message_complete)
                {   
                    if (BPG_data[0].updated_parameter_status == 0) copy_new_parameters();
                    BPG_data[0].updated_parameter_status = 1;
                    BPG_data[0].parameter_complete_counter++;
                }
                if ((new_parameters.complete_parameter_set == BPG_parameter_message_complete) && (BPG_data[0].parameter_complete_counter == 10))
                {
                    //update FRAM and globals with new_parameters;

                    BPG_data[0].updated_parameter_status = 0;
                    BPG_data[0].parameter_complete_counter = 0;
                    memset(&new_parameters,0,sizeof(new_parameters));
                }
                if ((new_parameters.complete_parameter_set != 0) && (new_parameters.complete_parameter_set != BPG_parameter_message_complete))
                {
                   
                    timer_setup(15);
                    do {
                        incoming_Argo_poll();                                        
                    } while ((timer_trigger_flag == 0) && (new_parameters.complete_parameter_set != BPG_parameter_message_complete));
                  
                    if ((timer_trigger_flag) && (new_parameters.complete_parameter_set != BPG_parameter_message_complete))
                    {
                        
                        new_parameters.complete_parameter_set = 0;
                        memset(&new_parameters,0,sizeof(new_parameters));
                    }
                    timer_setup(1);
                }                   
                
                if (alarm_condition != ALARM_NONE) panic_commands.system_sleep = 0;
                
                purge_offline_modules();
                
                
                if (panic_commands.system_sleep == 0) 
                {
                    send_BPG_data();               
                    if ((BPG_data[0].interface_type_to_use == INTERFACE_RS485) && (BPG_data[0].battery_manufacturer == TORQEEDO) && (BPG_data[0].BMS_block_count > 0))
                    {
                        TQ_device_send_data(block_cycle);                
                    }
                }
                
                master_search = 0xFFFF;
                for (ID_loop = 0; ID_loop < BPG_max; ID_loop++)
                {
                    if ((BPG_data[ID_loop].ID_number < master_search) && (BPG_data[ID_loop].ID_number != 0)) master_search = BPG_data[ID_loop].ID_number;
                }
                
                if ((master_search == unique_16bit_ID) && (panic_commands.system_sleep == 0)) collate_alarms();
                
                current_parameter_counter++;
                if (current_parameter_counter == 10)
                {
                    if (panic_commands.system_sleep == 0) send_BPG_parameters();
                    current_parameter_counter = 0;
                    log_timer++;
                    if (log_timer >= 60)
                    {
                        log_timer = 0;
                        write_log(BPG_data[0].alarm);
                        write_log(BPG_data[0].current_switch_position);
                        general_data = (unsigned char)(BPG_data[0].bank_switch_voltage * 4);
                        write_log(general_data);
                        general_data = (signed char)BPG_data[0].battery_thermistor_1;
                        write_log(general_data);
                    }
                }
                //Collate global alarm conditions
                //Status report via PGN
                //Check for parameter updates
                if (power_save_mode == 0) LED_power = !LED_power;
                if (FRAM_interface_type(0,0) == INTERFACE_RS485) incoming_RS485_poll();                
            }                                                                                          
        }
}


//************************* FRAM routines *************************

//------------------------------------
// Read from FRAM
// Entry: FRAM address to read
// Exit: Contents at the FRAM address
//------------------------------------
unsigned char FRAM_read_byte(unsigned int FRAM_address)
{
    unsigned char general;
    union {
        unsigned long address_long;
        unsigned char address_bytes[4];
    } FRAM_local_address;

        FRAM_local_address.address_long = FRAM_address;
        FRAM_select = 0;
        SPI_write(0x03);
        SPI_write(FRAM_local_address.address_bytes[1]);
        SPI_write(FRAM_local_address.address_bytes[0]);
        general = SPI_read();
        FRAM_select = 1;

        return (general);	
}

//---------------------------------------------------------
// Write to FRAM (one byte)
// Entry: FRAM address to write and the byte to be written
// Exit: None
//---------------------------------------------------------
void FRAM_write_byte(unsigned int FRAM_address, unsigned char byte)
{
   union {
      unsigned long address_long;
      unsigned char address_bytes[4];
   } FRAM_local_address;

        FRAM_local_address.address_long = FRAM_address;
        FRAM_select = 0;
        SPI_write(0x06);
        FRAM_select = 1;

        FRAM_select = 0;
        SPI_write(0x02);
        SPI_write(FRAM_local_address.address_bytes[1]);
        SPI_write(FRAM_local_address.address_bytes[0]);
        SPI_write(byte);
        FRAM_select = 1;
}


//-----------------------------------------------------------------------------------------
// Retrieve data from specified FRAM address
// Entry: FRAM variable location, length of data, data to be written if any, write or read)
// Exit: Data at the location / Data written
//-----------------------------------------------------------------------------------------
double FRAM_variable_double(unsigned int FRAM_location, double data_value, unsigned char write_enable, double default_value)
{
    unsigned int h;
    union {
        double variable_32bit;
	unsigned char variable_bytes[4];
    } FRAM_variable;

        FRAM_variable.variable_32bit = 0;

        if (write_enable > 0)
        {
            if (write_enable == 1) FRAM_variable.variable_32bit = data_value;
            if (write_enable == 2) FRAM_variable.variable_32bit = default_value;
            for (h = 0; h < 4; h++)
            {
                FRAM_write_byte(FRAM_location,FRAM_variable.variable_bytes[h]);
                FRAM_location++;
            }
        } else {
            for (h = 0; h < 4; h++)
            {
                FRAM_variable.variable_bytes[h] = FRAM_read_byte(FRAM_location);
                FRAM_location++;
            }
        }

        return FRAM_variable.variable_32bit;
}


//------------------------------------------------------------------------------------------
// Retrieve data from specified FRAM address
// Entry: FRAM variable location, length of data, data to be written if any, write or read)
// Exit: Data at the location / Data written
//------------------------------------------------------------------------------------------
unsigned long long int FRAM_variable_RW(unsigned long FRAM_location, unsigned long long int data_size, unsigned long long int data_value, unsigned char write_enable, unsigned long long int defaults)
{
    unsigned long long int h;
    union {
        unsigned long long int variable_64bit;
        unsigned char variable_bytes[8];
    } FRAM_variable;

        FRAM_variable.variable_64bit = 0;

        if (write_enable>0)
        {
            if (write_enable == 1) FRAM_variable.variable_64bit = data_value;
            if (write_enable == 2) FRAM_variable.variable_64bit = defaults;

            for (h = 0; h < data_size; h++)
            {
                FRAM_write_byte(FRAM_location,FRAM_variable.variable_bytes[h]);
                FRAM_location++;
            }
        } else {

            for (h = 0; h < data_size; h++)
            {
                FRAM_variable.variable_bytes[h] = FRAM_read_byte(FRAM_location);
                FRAM_location++;
            }
        }

        return FRAM_variable.variable_64bit;
}


//------------------------------------------------------------------------------
// Reset Ah, SoC and current offset if this is the first time the board has run
// Entry: None
// Exit: None
//------------------------------------------------------------------------------
void check_virgin_FRAM(void)
{
	unsigned char virgin_board;

        virgin_board = FRAM_virgin(0,0);

        if (virgin_board != 0x77)
        {
                factory_reset();
                FRAM_virgin(0x77,1);
        }        
}



//------------------
// Setup 2x UART
// Entry: Baud rate
// Exit: None
//------------------
void UART_init(unsigned long baud, unsigned char UART_channel)
{
    if (UART_channel == UART_RS232)
    {
        U1MODEbits.UARTEN = 0;					//Disable UART1
        U1BRG = (FCY / (4 * baud)) - 1;    		//115200bps
        U1MODEbits.USIDL = 0;					//Continue in idle mode
        U1MODEbits.IREN = 0;					//IrDA disabled
        U1MODEbits.UEN = 0b00;					//RX and TX rest not used
        U1MODEbits.ABAUD = 0;					//Auto baud off
        U1MODEbits.RXINV = 0;					//RX idle state is high
        U1MODEbits.BRGH = 1;					//BRGH is 4 cycles per bit
        U1MODEbits.PDSEL = 0b00;				//8bit no parity
        U1MODEbits.STSEL = 0b00;				//1 stop bit
        U1STA = 0;
        U1MODEbits.UARTEN = 1;					//Enable UART1
        U1STAbits.UTXEN = 1;					//TX enabled
    }
    if (UART_channel == UART_RS485)
    {
        U2MODEbits.UARTEN = 0;					//Disable UART1
        U2BRG = (FCY / (4 * baud)) - 1;    		//115200bps
        U2MODEbits.USIDL = 0;					//Continue in idle mode
        U2MODEbits.IREN = 0;					//IrDA disabled
        U2MODEbits.UEN = 0b00;					//RX and TX rest not used
        U2MODEbits.ABAUD = 0;					//Auto baud off
        U2MODEbits.RXINV = 0;					//RX idle state is high
        U2MODEbits.BRGH = 1;					//BRGH is 4 cycles per bit
        U2MODEbits.PDSEL = 0b00;				//8bit no parity
        U2MODEbits.STSEL = 0b00;				//1 stop bit
        U2STA = 0;
        U2MODEbits.UARTEN = 1;					//Enable UART1
        U2STAbits.UTXEN = 1;					//TX enabled
    }
    
}



//-----------------------
// Send a byte to the PC
// Entry: Byte to send
// Exit: None
//-----------------------
void RS232_send_char(unsigned char send_byte)
{
        while (U1STAbits.UTXBF == 1);
        U1TXREG = send_byte;
}


//------------------------------------------
// Receive a byte from the PC
// Entry: UART timeout in seconds
// Exit: Char received or 256 if a time out
//------------------------------------------
unsigned short int RS232_receive_char(unsigned short int timeout)
{
	unsigned short int return_value;

        return_value = 256;

        if (U1STAbits.OERR == 1)
        {
            U1STAbits.OERR = 0;
            return_value = U1RXREG;
            return_value = U1RXREG;
            return_value = U1RXREG;
        }
        if ((U1STAbits.FERR == 1) || (U1STAbits.PERR == 1)) return_value = U1RXREG;
        if (timeout != 0)
        {
            RX_timer_setup(timeout);            
        } else {
            if (U1STAbits.URXDA) 
            {
                return_value = U1RXREG;
                RS232_send_char(return_value);
                return return_value;
            }
        }
        

		while ((U1STAbits.URXDA == 0) && (RX_timer_trigger_flag));
		if (U1STAbits.URXDA) return_value = U1RXREG;
        //if (return_value != 256) RS232_send_char(return_value);
        if (return_value == 0) return_value = 256;
        if (return_value != 256) 
        {
            BPG_data[0].RS232_data_present = 1;
        } else {
            BPG_data[0].RS232_data_present = 0;
        }
        return return_value;
}



//------------------------------------------------------------------------
// Send a string to the PC, terminated with a null (/0)
// Entry: String to fill, max length of string generally "sizeof(string)"
// Exit: None
//------------------------------------------------------------------------
void RS232_send_string(const char *data_string, unsigned int length)
{
        
        while ((*data_string) && (length>0))	
        {
            RS232_send_char(*data_string++);
            length--;
        }
        
}

//--------------------------------------------------------------------------------------
// Receive a string from the PC
// Entry: String, max length of string generally "sizeof(string)", terminator to detect
// Exit: Returns 0 if nothing received and 1 if the string is filled
//     : Fills the string with what's received
//--------------------------------------------------------------------------------------
unsigned char RS232_receive_string(char *data_string, unsigned int length)
{
	unsigned short int returned_character;

        returned_character = RS232_receive_char(0);	
        if (returned_character == 256)
        {
            *data_string = 0;
            return 0;
        }

        do {
            if (returned_character != 256) *data_string = returned_character;
            if (returned_character == 0x0D) 
            {
                *data_string = 0;
                return 1;
            }
            returned_character = RS232_receive_char(1);
            
            if (returned_character != 256) 
            {
                data_string++;
                length--;
            }
        } while (length > 1);

        *data_string = 0;			
        return 1;
}


//-----------------------
// Send a byte to the PC
// Entry: Byte to send
// Exit: None
//-----------------------
void RS485_send_char(unsigned char send_byte)
{
        U2TXREG = send_byte;            
        while (U2STAbits.UTXBF == 1);
        
}


//------------------------------------------
// Receive a byte from the PC
// Entry: UART timeout in seconds
// Exit: Char received or 256 if a time out
//------------------------------------------
unsigned short int RS485_receive_char(double timeout)
{
	unsigned short int return_value;

        return_value = 256;

        if (U2STAbits.OERR == 1)
        {
            U2STAbits.OERR = 0;
            return_value = U2RXREG;
            return_value = U2RXREG;
            return_value = U2RXREG;
        }
        if ((U2STAbits.FERR == 1) || (U2STAbits.PERR == 1)) return_value = U2RXREG;
        if (timeout != 0)
        {
            RX_timer_setup(timeout);            
        } else {
            if (U2STAbits.URXDA) 
            {
                return_value = U2RXREG;                
                return return_value;
            }
        }
        

		while ((U2STAbits.URXDA == 0) && (RX_timer_trigger_flag == 0));

		if (U2STAbits.URXDA) return_value = U2RXREG;
        if (return_value != 256) 
        {
            BPG_data[0].RS485_data_present = 1;
        } else {
            BPG_data[0].RS485_data_present = 0;
        }

        return return_value;
}



//------------------------------------------------------------------------
// Send a string to the PC, terminated with a null (/0)
// Entry: String to fill, max length of string generally "sizeof(string)"
// Exit: None
//------------------------------------------------------------------------
void RS485_send_string(unsigned char command_length)
{
    unsigned char data_search, replaced_byte;
    
        if (torqeedo_data_TX[0] != 0xAC) return;
        
            
        RS485_direction = 1;
        data_search = 0;
        do {
            replaced_byte = 0;
            if ((data_search > 0) && (data_search < command_length - 1) && (torqeedo_data_TX[data_search] == 0xAC))
            {
                RS485_send_char(0xAE);
                RS485_send_char(0x2C);
                replaced_byte = 1;
            }
            if ((data_search > 0) && (data_search < command_length - 1) && (torqeedo_data_TX[data_search] == 0xAD))
            {
                RS485_send_char(0xAE);
                RS485_send_char(0x2D);
                replaced_byte = 1;
            }
            if ((data_search > 0) && (data_search < command_length - 1) && (torqeedo_data_TX[data_search] == 0xAE))
            {
                RS485_send_char(0xAE);
                RS485_send_char(0x2E);
                replaced_byte = 1;
            }
            if (replaced_byte == 0) RS485_send_char(torqeedo_data_TX[data_search]);                             
            data_search++;            
        } while ((data_search < command_length) && (data_search < 255)); 
        delay_ms(2);
        RS485_direction = 0;
}



//--------------------------------------------------------------------------------------
// Receive a string from the PC
// Entry: String, max length of string generally "sizeof(string)", terminator to detect
// Exit: Returns 0 if nothing received and 1 if the string is filled
//     : Fills the string with what's received
//--------------------------------------------------------------------------------------
unsigned char RS485_receive_string(void)
{
    unsigned char RS485_temp_buffer[255];
	unsigned short int returned_character;
    unsigned char data_search, replace_escape, search_escape;

        returned_character = RS485_receive_char(0.03);	
        if (returned_character != 0xAC) return RS485_DATA_MISSING;

        data_search = 0;
        do {
            
            RS485_temp_buffer[data_search] = (unsigned char)returned_character;
            if (returned_character == 0xAD)  
            {
                replace_escape = 0;
                search_escape = 0;
                do {                    
                    if (RS485_temp_buffer[search_escape] != 0xAE)
                    {
                        torqeedo_data_RX[replace_escape] = RS485_temp_buffer[search_escape];   
                        replace_escape++;
                        search_escape++;
                    } else {
                        search_escape++;
                        if (RS485_temp_buffer[search_escape] == 0x2C) torqeedo_data_RX[replace_escape] = 0xAC;
                        if (RS485_temp_buffer[search_escape] == 0x2D) torqeedo_data_RX[replace_escape] = 0xAD;
                        if (RS485_temp_buffer[search_escape] == 0x2E) torqeedo_data_RX[replace_escape] = 0xAE;
                        search_escape++;
                        replace_escape++;
                    }
                } while (search_escape < data_search);                                                
                return RS485_DATA_OK;
            }
            returned_character = RS485_receive_char(0.03);
            
            if (returned_character != 256) 
            {
                data_search++;                
            } else {
                return RS485_DATA_LENGTH_ERROR;
            }
        } while (data_search < 255);
                                                
        return RS485_DATA_INVALID;
}



//****************************************************************** CAN routines ************************************************************

//------------------------------------------------------------
// Set or unset the select line from a CAN bus
// Entry: Can interface number 0=Aux, 1=N2K, 2=J1939, 3=Argo.
//        Line state
// Exit: None but the correct line is set/unset
//------------------------------------------------------------
void CAN_select(unsigned char CAN_interface, unsigned char select_set)
{
        if (CAN_interface == Argo_CAN) Argo_select = select_set;
        if (CAN_interface == Lithium_CAN) Lithium_select = select_set;        
}


//--------------------------------------
// Reset and setup the MCP2515
// Entry: Can bus to reset
// Exit: None. The CAN bus is activated
//--------------------------------------
void CAN_reset(unsigned char CAN_interface, unsigned short int baud)
{

        CAN_select(CAN_interface, 0);
        SPI_write(0xC0);
        CAN_select(CAN_interface, 1);

        delay_ms(50);

        CAN_select(CAN_interface, 0);
        SPI_write(0x02);                                    //Write
        SPI_write(0x28);                                    //CNF3 address      10MHz @ 250Kb/s     16MHz @ 250Kb/s     16MHz @ 1Mb/s
        if (baud == 1000)
        {
            SPI_write(0x02);                                    //CNF3              0x07                0x05                0x00
            SPI_write(0x90);                                    //CNF2              0xBA                0xB8                0x90
            SPI_write(0x00);                                    //CNF1              0x00                0x01                0x02
        }        
        if (baud == 250)
        {
            SPI_write(0x05);                                    //CNF3              0x07                0x05                0x00
            SPI_write(0xB8);                                    //CNF2              0xBA                0xB8                0x90
            SPI_write(0x01);                                    //CNF1              0x00                0x01                0x02
        }
        if (baud == 125)
        {
            SPI_write(0x05);                                    //CNF3              0x07                0x05                0x00
            SPI_write(0xB8);                                    //CNF2              0xBA                0xB8                0x90
            SPI_write(0x03);                                    //CNF1              0x00                0x01                0x02
        }
        SPI_write(0x00);                                    //CANINTE off
        SPI_write(0x00);	                            //CANINTF off
        CAN_select(CAN_interface, 1);

        delay_ms(1);

        CAN_select(CAN_interface, 0);
        SPI_write(0x02);                                    //Write
        SPI_write(0x0D);                                    //TXRTSCTRL
        SPI_write(0x00);                                    //Turn off
        CAN_select(CAN_interface, 1);

        delay_ms(1);

        CAN_select(CAN_interface, 0);
        SPI_write(0x02);                                    //Write
        SPI_write(0x60);                                    //RXB0CNTRL address
        //FILHIT = 0 BUKT1 = 0 BUKT = 0 RXRTR = 0 N/A = 0 RXM = 10 N/A = 0
        SPI_write(0b01100100);
		CAN_select(CAN_interface, 1);

        delay_ms(1);

		CAN_select(CAN_interface, 0);
        SPI_write(0x02);                                    //Write
        SPI_write(0x70);                                    //RXB1CNTRL address
        SPI_write(0b01100000);                              //Extended only, BUKT on
		CAN_select(CAN_interface, 1);
 
        delay_ms(1);

		CAN_select(CAN_interface, 0);
        SPI_write(0x02);                                    //Write
        SPI_write(0x0F);                                    //CANCTRL
        SPI_write(0x00);                                    //resume normal mode and set retry
		CAN_select(CAN_interface, 1);
}



//-----------------------------------------------------------------------------
// CAN buffer check
// Entry: CAN interface to check
// Exit: Fills up to 2 buffers and flags which ones have data, returns success
//-----------------------------------------------------------------------------
unsigned char CAN_buffer_check(unsigned char CAN_interface)
{
	unsigned char CAN_status, CAN_data_available;

        CAN_data_available = CAN_BUFFER_EMPTY;
        CAN_select(CAN_interface, 0);
        SPI_write(0xB0);
        CAN_status = SPI_read();
        CAN_select(CAN_interface, 1);
        CAN_buffer_store[0][CAN_interface].CAN_PGN = 0;                                                                //Clear buffer 0 and 1 contents
        CAN_buffer_store[1][CAN_interface].CAN_PGN = 0;
        if (CAN_status & 0x40)
        {
            CAN_buffer(0, CAN_interface);
            CAN_data_available = CAN_BUFFER0_FULL;                                                 //Return buffer 0 is filled           
        }                                                                                                            //test bit 7 to check if buffer 1 is full
        if (CAN_status & 0x80)
        {
            CAN_buffer(1, CAN_interface);
            if (CAN_data_available == CAN_BUFFER_EMPTY) CAN_data_available = CAN_BUFFER1_FULL;     //Return Buffer 0 is empty but buffer 1 is filled
            if (CAN_data_available == CAN_BUFFER0_FULL) CAN_data_available = CAN_BUFFER0_1_FULL;   //Return Buffer 0 and buffer 1 are filled
        }

        if (CAN_data_available != CAN_BUFFER_EMPTY)
        {
            LED_active = !LED_active;            
        }

        
        return CAN_data_available;
}



//------------------------------------------------
// CAN buffer retrieval
// Entry: CAN interface and buffer that is filled
// Exit: None but the global buffers are filled
//------------------------------------------------
void CAN_buffer(unsigned char buffer_number, unsigned char CAN_interface)
{
	unsigned char i, ID_byte_1, ID_byte_2, ID_byte_3, ID_byte_4, ID_byte_assembly;
    unsigned short int SID_assembly;
    unsigned long EID_assembly, PGN_assembly, temp_assembly;
   
        CAN_select(CAN_interface, 0);

        if (buffer_number == 0) SPI_write(0x90);
        if (buffer_number == 1) SPI_write(0x94);

        ID_byte_1 = SPI_read();                 //SID10, SID9, SID8, SID7, SID6, SID5, SID4, SID3
        ID_byte_2 = SPI_read();                 //SID2, SID1, SID0, SRR, IDE, -, EID17, EID16
        ID_byte_3 = SPI_read();                 //EID15, EID14, EID13, EID12, EID11, EID10, EID9, EID8
        ID_byte_4 = SPI_read();                 //EID7, EID6, EID5, EID4, EID3, EID2, EID1, EID0
    
        SID_assembly = ID_byte_1;
        SID_assembly = SID_assembly << 3;
        ID_byte_assembly = (ID_byte_2 & 0xE0);
        ID_byte_assembly = ID_byte_assembly >> 5;
        SID_assembly = SID_assembly | ID_byte_assembly;

        EID_assembly = (ID_byte_2 & 0x03);
        EID_assembly = EID_assembly * 0x10000;
        temp_assembly = ID_byte_3;
        EID_assembly = EID_assembly + (temp_assembly * 0x100);
        EID_assembly = EID_assembly + ID_byte_4;

        PGN_assembly = SID_assembly;
        PGN_assembly = PGN_assembly * 0x40000;
        PGN_assembly = PGN_assembly + EID_assembly;  
        
        if (buffer_number == 0) CAN_buffer_store[0][CAN_interface].CAN_PGN = PGN_assembly;
        if (buffer_number == 1) CAN_buffer_store[1][CAN_interface].CAN_PGN = PGN_assembly;

        for (i = 0; i < 9; i++)
        {
            if (buffer_number == 0) CAN_buffer_store[0][CAN_interface].CAN_byte[i] = SPI_read();
            if (buffer_number == 1) CAN_buffer_store[1][CAN_interface].CAN_byte[i] = SPI_read();
        }

        CAN_select(CAN_interface, 1);
        

}


//-------------------------------------------------------------
// Transmit CAN message (PGN))
// Entry: CAN data packet to send, CAN interface to send it to
// Exit: None
//-------------------------------------------------------------
void CAN_send(CAN_data_packet CAN_send_data, unsigned char CAN_interface)
{
	unsigned char general, transmit_buffer;
    unsigned long SID11, EID18, EID_calculation;
    unsigned char SID_byte_1, SID_byte_2, EID_byte_1, EID_byte_2, EID_byte_3;   
    union {
        unsigned long PGN;
        unsigned char bytes[4];
    } long_construct;

        if (panic_commands.pause_CAN_bus_activity) return;
    
        transmit_buffer = 0x00;
        CAN_select(CAN_interface, 0);
        SPI_write(0xA0);                                                    //Read Status
        general = SPI_read();
        general = SPI_read();
        CAN_select(CAN_interface, 1);
        if (check_bit(general,2) == 0)                                      //Check to see if the buffer0 is full
        {
                transmit_buffer = 0x40;
        } else {
                if (check_bit(general,4) == 0)                                  //Check to see if the buffer1 is full
                {
                    transmit_buffer = 0x42;
                } else {
                    if (check_bit(general,6) == 0)                              //Check to see if the buffer2 is full
                    {
                        transmit_buffer = 0x44;
                    } else {
                       return;                                                  //Skip the send if it is, something clearly not right on the bus
                    }
                }
            }
        delay_ms(1);
        CAN_select(CAN_interface, 0);
        SPI_write(transmit_buffer);                                         //TX buffer, start at 0x31 (standard ID high), page 54 MCP2515 datasheet
        
        long_construct.PGN = CAN_send_data.CAN_PGN;                     //11 bits of SID + 18bit EID
        SID11 = long_construct.PGN / 0x40000; //>> 18;
        EID18 = (long_construct.PGN & 0x3FFFF);
        SID_byte_1 = SID11 >> 3;
        SID_byte_2 = SID11 << 5;
        SID_byte_2 |= 1 << 3;                                           //Extended header set
        EID_calculation = EID18 / 0x10000;
        EID_byte_1 = (unsigned char)EID_calculation;
        SID_byte_2 = SID_byte_2 | EID_byte_1;
        EID_byte_2 = (EID18 & 0xFF00) >> 8;
        EID_byte_3 = (EID18 & 0xFF);       
        
        SPI_write(SID_byte_1);
        SPI_write(SID_byte_2);
        SPI_write(EID_byte_2);
        SPI_write(EID_byte_3);
    
        SPI_write(0x08);                                                    //8 bytes to be sent (part of the standard header)
                                                                            
        for (general = 0; general < 8; general++)
        {
            SPI_write(CAN_send_data.CAN_byte[general]);
        }

        CAN_select(CAN_interface, 1);

        delay_ms(1);

        CAN_select(CAN_interface, 0);
        SPI_write(0x81);
        CAN_select(CAN_interface, 1);

        delay_ms(1);

        CAN_select(CAN_interface, 0);
        SPI_write(0x03);
        SPI_write(0x2D);
        general = SPI_read();
        CAN_select(CAN_interface, 1);

        if (!(check_bit(general,5)))
        {
            incoming_Argo_poll();
            return;                                                     //No error occured, assume success
        }
        
        if (CAN_interface == Argo_CAN) CAN_reset(CAN_interface, 250);                                           //Error sending, try reseting the CAN chip just in case
        if (CAN_interface == Lithium_CAN) CAN_reset(CAN_interface, Lithium_baud);
}


//**************************************************** SPI routines ********************************************************

//----------------------------------
// Setup the hardware SPI interface
// Entry: None
// Exit: None
//----------------------------------
void SPI_init(void)
{
    unsigned long system_frequency_KHz;
    
        system_frequency_KHz = FCY / 1000;
        SPI1CON1L = 0;
        SPI1CON1H = 0;
        SPI1CON2L = 0;
        SPI1CON2H = 0;

        SPI1IMSKL = 0;
        SPI1IMSKH = 0;
        SPI1STATL = 0;
        SPI1STATH = 0;
        
        SPI1CON1Lbits.SPIEN = 0;		//DisableSPI
        SPI1CON1Lbits.SPISIDL = 1;	//Idle mode
        SPI1BUFL = 0;			//Clear SPI buffer
        SPI1BUFH = 0;			//Clear SPI buffer
        SPI1CON1Lbits.DISSCK = 0;		//Enabled SCK
        SPI1CON1Lbits.DISSDO = 0;	//Enabled SDO
        SPI1CON1Lbits.DISSDI = 0;	//Enabled SDI
        SPI1CON1Lbits.MODE16 = 0;	//8 bits
        SPI1CON1Lbits.SMP = 0;		//Sample at end
        SPI1CON1Lbits.CKE = 1;		//Clock -> Idle to active
        SPI1CON1Lbits.CKP = 0;		//Idle state for clock is high
        SPI1CON1Lbits.SSEN = 0;		//Slave pin not used
        SPI1CON1Lbits.MODE = 0;		//8 bit mode
        SPI1BRGL = (system_frequency_KHz / (2 * 4000)) - 1;
        SPI1BRGH = 0;
        SPI1CON1bits.MSTEN = 1;		//Master mode
        SPI1CON1Lbits.SPIEN = 1;		//Enable SPI	

            
}

//-------------------------------------------------------
// Write 'data' to the SPI, enable line already asserted
// Entry: Data to write to the bus
// Exit: Data transfered asynchronously by the write
//-------------------------------------------------------
unsigned char SPI_write(unsigned char data)
{
        SPI1BUFL = data;
        while(!SPI1STATLbits.SPIRBF);	// wait for transfer to complete
        return SPI1BUFL; 
}

//------------------------------------------------------
// Read 'data' to the SPI, enable line already asserted
// Entry: None
// Exit: Data read from the bus
//------------------------------------------------------
unsigned char SPI_read(void)
{
        SPI1STATLbits.SPIROV = 0;
        SPI1BUFL = 0x00;                       // initiate bus cycle
        while(!SPI1STATLbits.SPIRBF);
        /* Check for Receive buffer full status bit of status register*/
        if (SPI1STATLbits.SPIRBF)
        { 
            SPI1STATLbits.SPIROV = 0;
            return (SPI1BUFL);                 /* return word read */
        }
        return 0;
}




//***************************************************** I2C routines ************************************************************

//--------------------------
// Setup and initialise I2C
// Entry: None
// Exit: None
//--------------------------
void I2C1_init(void)
{
	unsigned char buffer;
                                        //Setup hardware I2C
        I2C1CONLbits.I2CEN = 0;		//Disable I2C
        I2C1CONLbits.DISSLW = 1;
        I2C1CONLbits.A10M = 0;		//7 bits
        I2C1CONLbits.SCLREL = 1;		//No clock stretch	
        I2C1BRG = 0x9D;			//100KHz @ 8MHz
        I2C1ADD = 0;
        I2C1MSK = 0;
        buffer = I2C1RCV;
        I2C1TRN = 0;
        I2C1STAT = 0;
        I2C1CONLbits.ACKDT = 0;		//Sends an ACK
        I2C1CONLbits.I2CEN = 1;		//Enable I2C
}

//-----------------------------------------------------------
// Start I2C, this function generates an I2C start condition
// Entry: None
// Exit: None
//-----------------------------------------------------------
void I2C1_start(void)
{
        I2C1CONLbits.ACKDT = 0;		//Reset any previous Ack
        //delay_us(10);
        I2C1CONLbits.SEN = 1;		//Generate Start COndition
        time_out_trap();
        while ((I2C1CONLbits.SEN) && (time_out_flag == 0));	//Wait for Start COndition       
}

//--------------------------------------------------------------------------------
//Restart I2C, this function generates an I2C Restart condition (second start bit)
// Entry: None
// Exit: None
//--------------------------------------------------------------------------------
void I2C1_restart(void)
{
        I2C1CONLbits.RSEN = 1;		//Generate Restart		
        time_out_trap();
        while ((I2C1CONLbits.RSEN)  && (time_out_flag == 0));	//Wait for restart	
}


//-------------------------------------------------------------
// Stop bit I2C, this function generates an I2C stop condition
// Entry: None
// Exit: None
//-------------------------------------------------------------
void I2C1_stop(void)
{
        I2C1CONLbits.PEN = 1;		//Generate Stop Condition
        time_out_trap();
        while ((I2C1CONLbits.PEN)  && (time_out_flag == 0));	//Wait for Stop
        I2C1CONLbits.RCEN = 0;
        I2C1STATbits.IWCOL = 0;
        I2C1STATbits.BCL = 0;
        //delay_us(10);
}

//---------------------------------
// Wait for the I2C bus to be idle
// Entry: None
// Exit: None
//---------------------------------
void I2C1_idle(void)
{
        time_out_trap();
        while ((I2C1STATbits.TRSTAT) && (time_out_flag == 0));		//Wait for bus Idle        
}

//-------------------------------
// Listen for an ACK on the line
// Entry: None
// Exit: None
//-------------------------------
unsigned char I2C1_ackstatus(void)
{
        return (!I2C1STATbits.ACKSTAT);		//Return Ack Status
}

//-----------------------------
// Generate an ACK on the line
// Entry: None
// Exit: None
//-----------------------------
void I2C1_ack(void)
{
	unsigned int general;
	
        general = 0;
        I2C1CONLbits.ACKDT = 0;			//Set for ACk
        I2C1CONLbits.ACKEN = 1;
        time_out_trap();
        while((I2C1CONLbits.ACKEN)  && (time_out_flag == 0));		//wait for ACK to complete
        I2C1CONLbits.ACKEN = 0;
}

//--------------------------------
// Generate a NOT ACK on the line
// Entry: None
// Exit: None
//--------------------------------
void I2C1_notack(void)
{
        I2C1CONLbits.ACKDT = 1;			//Set for NotACk
        I2C1CONLbits.ACKEN = 1;
        time_out_trap();
        while((I2C1CONLbits.ACKEN) && (time_out_flag == 0));		//wait for ACK to complete
        I2C1CONLbits.ACKEN = 0;
        I2C1CONLbits.ACKDT = 0;			//Set for NotACk
}

//----------------------------------------------------------------------------------
// Write a byte to the I2C, this function transmits the byte passed to the function
// Entry: Byte to send to the bus
// Exit: None
//----------------------------------------------------------------------------------
void I2C1_write(unsigned char byte)
{
       // time_out_trap();
       // while ((I2C1STATbits.TBF)  && (time_out_flag == 0));		//wait for data transmission
        I2C1TRN = byte;				//Load byte to I2C1 Transmit buffer
        time_out_trap();
        while ((I2C1STATbits.TRSTAT) && (time_out_flag == 0));		//wait for data transmission
        //delay_us(2);
}

//----------------------------------------------------------------------------------
// Read a byte to the I2C, this function reads a byte fromt eh bus
// Entry: None
// Exit: Byte sampled on the bus
//----------------------------------------------------------------------------------
unsigned char I2C1_read(void)
{
        I2C1CONLbits.RCEN = 1;			//Enable Master receive
        time_out_trap();
        while((!I2C1STATbits.RBF) && (time_out_flag == 0));	//Wait for receive buffer to be full
        
        if ((time_out_flag) || (I2C1_ackstatus() == 0))
        {
            return 0;
        } else {
            return(I2C1RCV);			//Return data in buffer
        }
}






//*********************************************************** I2C devices ********************************************************


//*************************************************************** PIC ADC routines *******************************************************

//-----------------------------------------------------
//Intialise Analogue channels for temperature sampling
//Entry: None
//Exit: None
//-----------------------------------------------------
void PIC_ADC_init(void)
{
      
       ANSA = 0;
       ANSB = 0;
       ANSC = 0;

       ANSAbits.ANSA0 = 1; // ANA0 Battery thermistor 1
       ANSAbits.ANSA1 = 1;  // ANA1 Battery thermistor 2
       
       ANSBbits.ANSB14 = 1; // ANA6 48V main
       ANSBbits.ANSB13 = 1;  // ANA7 Signal 2
       ANSBbits.ANSB12 = 1;  // ANA8 Signal 1       
       
       AD1CHS = 0;
       AD1CON1 = 0;
       AD1CON1bits.ADSIDL = 0;
       AD1CON1bits.SSRC = 0b0111;
       AD1CON1bits.MODE12 = 1;   
       
       AD1CON2 = 0;
       AD1CON3 = 0;
       AD1CON3bits.SAMC = 0b11111;
       AD1CON3bits.ADCS = 0b00000100;
       //AD1CON3 = 0x1F02;
       AD1CON5 = 0;
       AD1CSSL = 0;
       AD1CON1bits.ADON = 1;
       

}

//------------------
// Read the PIC ADC
// Entry: None
// Exit: Raw Value
//------------------
double PIC_ADC_voltage_read(unsigned char channel)
{
    unsigned long voltage_average;
    unsigned char counter;
    double voltage_result;
    
        AD1CON1bits.ADON = 0;
        AD1CHS = channel;
        delay_us(10);
        AD1CON1bits.ADON = 1;
        delay_us(100);
        voltage_average = 0;
        counter = 16;
        do {
            AD1CON1bits.SAMP = 1;
            while (!AD1CON1bits.DONE);
            
            voltage_average = voltage_average + ADC1BUF0;
            counter--;

        } while (counter > 0);
        
        voltage_average = voltage_average / 16;
        voltage_result = (double)voltage_average / DC_voltage_ratio;
        return voltage_result;
}

double PIC_ADC_temperature_read(unsigned char channel)
{
    unsigned long temperature_average;
    unsigned char counter;
    double processed_temperature, log_temperature;
    double ADC_voltage, NTC_resistance;
    
        AD1CON1bits.ADON = 0;
        AD1CHS = channel;
        delay_us(10);
        AD1CON1bits.ADON = 1;
        delay_us(100);
        temperature_average = 0;
        counter = 16;
        do {
            AD1CON1bits.SAMP = 1;
            while (!AD1CON1bits.DONE);
            
            temperature_average = temperature_average + ADC1BUF0;
            counter--;

        } while (counter > 0);
        
        temperature_average = temperature_average / 16;

        ADC_voltage = temperature_average / 1241.21;

        NTC_resistance = 10000.0 / ((3.3 / ADC_voltage) - 1);
        log_temperature = log(NTC_resistance / 10000.0);
        processed_temperature = (1.0 / 298.15) + (1.0 / thermistor_beta) * log_temperature;
        processed_temperature = (1.0 / processed_temperature) - 273.15;        

        return processed_temperature;
}




//**************************************************** Timer routines *************************************************

//-----------------------------------------------------
// Delay in seconds (0-255)
// Entry: Number of seconds to delay
// Exit: None but no return until the time has elapsed
//-----------------------------------------------------
void n_second_delay(unsigned char length)
{
	unsigned char loop;

        for (loop = 0; loop < length; loop++)
        {
            delay_ms(1000);
        }
}


//----------------------------------
// Timer setup
// Entry: Number of seconds to wait
// Exit: None but timer is started
//----------------------------------
void timer_setup(double seconds)
{
    unsigned long maximum_seconds_check;
	union {
			unsigned long timer_count;
			unsigned int timer[2];
	} PR_timer;

        T4CON = 0;
        T5CON = 0;
        TMR4 = 0;
        TMR5 = 0;
        T4CONbits.T32 = 1;                                      //32 bit timer
        T4CONbits.TCKPS = 0b00;                          	//Divide by 1

        maximum_seconds_check = 0xFFFFFFFF / FCY;
        if (seconds > maximum_seconds_check) seconds = maximum_seconds_check;
        PR_timer.timer_count = (unsigned long)(seconds * FCY);

        PR4 = PR_timer.timer[0];
        PR5 = PR_timer.timer[1];

        TMR5HLD = 0;
        //reset the trigger flag
        timer_trigger_flag = 0;
        T4CONbits.TON = 1;                                      //Start timer
}

//----------------------------------
// Timer setup
// Entry: Number of seconds to wait
// Exit: None but timer is started
//----------------------------------
void RX_timer_setup(double seconds)
{
    unsigned long maximum_seconds_check;
	union {
			unsigned long timer_count;
			unsigned int timer[2];
	} PR_timer;

        T2CON = 0;
        T3CON = 0;
        TMR2 = 0;
        TMR3 = 0;
        T2CONbits.T32 = 1;                                      //32 bit timer
        T2CONbits.TCKPS = 0b00;                          	//Divide by 1

        maximum_seconds_check = 0xFFFFFFFF / FCY;
        if (seconds > maximum_seconds_check) seconds = maximum_seconds_check;
        PR_timer.timer_count = (unsigned long)(seconds * FCY);

        PR2 = PR_timer.timer[0];
        PR3 = PR_timer.timer[1];

        TMR3HLD = 0;
        //reset the trigger flag
        RX_timer_trigger_flag = 0;
        T2CONbits.TON = 1;                                      //Start timer
}


//************************************************************** Misc routines *************************************************************

//-----------------------------
// Initialise the power sensor
// Entry: None
// Exit: None
//-----------------------------
void BPG_init(void)
{
                                       //Default power on pin setup
        bank_power_control = 0;
        Argo_select = 1;             
        Lithium_select = 1;
        FRAM_select = 1;     
        LED_active = 0;        
        LED_power = 1;
        relay1_control = relay_off;
        relay2_control = relay_off;
        RS485_direction = 0;
        RS485_control = 1;
        MCP2515_CAN_control = 1;
        regulator_CAN_control = 0;
        diode_control = 0;  //Relay 1 should not activate diode control
        pre_charge = 0;
                                        //Initialise the onboard interfaces and hardware
        PIC_ADC_init();
        I2C1_init();  

        SPI_init();
        CAN_reset(Argo_CAN, 250);
        CAN_reset(Lithium_CAN, Lithium_baud);

        UART_init(19200, UART_RS485);
        delay_ms(500);
                                        //Sensor initialise
        check_virgin_FRAM();        

        delay_ms(100);                         
        EUI48 = EUI48_read();

        unique_16bit_ID = (unsigned short int)(EUI48 & 0xFFFF);
        FRAM_update_active(0,1);
        FRAM_unique_ID(unique_16bit_ID,1);                         
        Lithium_present = 0;



}



//-----------------------------
// Read 48bit EUI
// Entry: none
// Exit: 48bit value in 64bits
//-----------------------------
unsigned long long int EUI48_read(void)
{
	unsigned char general;
    union {
        unsigned long long int EUI48_64bit;
        unsigned char EUI48_bytes[8];
    } EUI48_value;

        general = EUI48_address;
        general &= ~(1 << 0);		//clear bit to write
        I2C1_idle();
        I2C1_start();
        I2C1_write(general);
        Nop();
        Nop();
        I2C1_write(0xFA);
        Nop();
        Nop();
        I2C1_restart();
        general |= 1 << 0;
        I2C1_write(general);
        Nop();
        Nop();
        EUI48_value.EUI48_bytes[5] = I2C1_read();
        I2C1_ack();
        EUI48_value.EUI48_bytes[4] = I2C1_read();
        I2C1_ack();
        EUI48_value.EUI48_bytes[3] = I2C1_read();
        I2C1_ack();
        EUI48_value.EUI48_bytes[2] = I2C1_read();
        I2C1_ack();
        EUI48_value.EUI48_bytes[1] = I2C1_read();
        I2C1_ack();
        EUI48_value.EUI48_bytes[0] = I2C1_read();
        I2C1_notack();
        I2C1_stop();
        EUI48_value.EUI48_bytes[6] = 0;
        EUI48_value.EUI48_bytes[7] = 0;

        return EUI48_value.EUI48_64bit;
}


//---------------------------------------------
// Reset variables in FRAM and copy to globals
// Entry: None
// Exit: None but FRAM varaibles are reset
//---------------------------------------------
void factory_reset(void)
{
        FRAM_battery_manufacturer(0,2);
        FRAM_battery_model(0,2);
        FRAM_interface_type(0,2);
        FRAM_use_interface_io(0,2);
        FRAM_allow_remote_switch(0,2);
        FRAM_last_switch_position(0,2);
        FRAM_low_voltage_disconnect(0,2);
        FRAM_high_voltage_disconnect(0,2);
        FRAM_voltage_time_hysteresis(0,2);
        FRAM_scan_only_paired_bank_current(0,2);
        FRAM_low_voltage_reconnect(0,2);
        FRAM_high_voltage_reconnect(0,2);
        FRAM_voltage_measurement_sleep_peek(0,2);
        FRAM_no_Argo_activity_sleep(0,2);
        FRAM_pair_index(0,2);
        FRAM_name_index(0,2);
        FRAM_high_temperature_disconnect(0,2);
        FRAM_low_temperature_disconnect(0,2);
        FRAM_temperature_time_hysteresis(0,2);
        FRAM_bank_class_to_scan(0,2);
        FRAM_low_temperature_reconnect(0,2);
        FRAM_high_temperature_reconnect(0,2);
        FRAM_high_current_disconnect(0,2);
        FRAM_current_time_hysteresis(0,2);
        FRAM_io_activation_delay(0,2);
        FRAM_io_trigger_voltage(0,2);
        FRAM_OCV_SoC_reset_enable(0,2);
        FRAM_time_before_OCV_SoC_reset(0,2);
        FRAM_OCV_SoC_reset_current_limit(0,2);
        FRAM_charge_override_time_hysteresis(0,2);
        FRAM_fan_activation_temperature(0,2);         
        FRAM_fan_deactivation_temperature(0,2);       
        FRAM_block_count(0,2);
        load_FRAM_into_globals();
}



//-------------
// I2C traps
// Entry: None
// Exit: None
//-------------
void time_out_trap(void)
{
        T1CON = 0;
        T1CONbits.TCKPS = 0b01;
        TMR1 = 0;
        PR1 = 0xFFFF;
        IFS0bits.T1IF = 0;       
        T1CONbits.TON = 1;
}
//-----------------------------------
// Check for J1939/2K interface data
// Entry: None
// Exit: Buffer status
//-----------------------------------
unsigned char incoming_RS232_poll(void)
{
    unsigned short int buffer_status;
    char buffer_string[255];
    
        buffer_status = RS232_receive_string(buffer_string, sizeof(buffer_string));    

        if (buffer_status != STRING_BUFFER_EMPTY)
        {
            //Process serial string
        }
        return buffer_status;
}

//-----------------------------------
// Check for J1939/2K interface data
// Entry: None
// Exit: Buffer status
//-----------------------------------
unsigned char incoming_RS485_poll(void)
{
        if (BPG_data[0].battery_manufacturer == TORQEEDO)
        {
            
        }
        return 0;
}

//-----------------------------------
// Check for J1939/2K interface data
// Entry: None
// Exit: Buffer status
//-----------------------------------
unsigned char incoming_Lithium_poll(void)
{
    unsigned char buffer_status;
    
        buffer_status = CAN_buffer_check(Lithium_CAN);    

        if (buffer_status != CAN_BUFFER_EMPTY)
        {
            BPG_data[0].battery_CAN_present = 1;  
            if (FRAM_battery_manufacturer(0,0) == TORQEEDO)
            {
                if (CAN_buffer_store[0][Lithium_CAN].CAN_PGN != 0) torqeedo_CAN_process(CAN_buffer_store[0][Lithium_CAN]);
                if (CAN_buffer_store[1][Lithium_CAN].CAN_PGN != 0) torqeedo_CAN_process(CAN_buffer_store[1][Lithium_CAN]);
            }
        }
        return buffer_status;
}

//-------------------------------
// Check for Argo interface data
// Entry: None
// Exit: Buffer status
//-------------------------------
unsigned char incoming_Argo_poll(void)
{
    unsigned char buffer_status;    
    
        buffer_status = CAN_buffer_check(Argo_CAN);    

        if (buffer_status != CAN_BUFFER_EMPTY)
        {
            
            if (CAN_buffer_store[0][Argo_CAN].CAN_PGN != 0) CAN_data_process(CAN_buffer_store[0][Argo_CAN]);
            if (CAN_buffer_store[1][Argo_CAN].CAN_PGN != 0) CAN_data_process(CAN_buffer_store[1][Argo_CAN]);
        }

        return buffer_status;
}

double read_PCB_temperature(void)
{
    signed short int temperature;
    unsigned char LSB_data, MSB_data, general;
    double calculated_temperature;
    
        general = TEMP_address;
        general &= ~(1 << 0);		//clear bit to write
        I2C1_idle();
        I2C1_start();
        I2C1_write(general);       
        I2C1_ackstatus();
        I2C1_write(0x01);           //Config register
        I2C1_ackstatus();
        I2C1_write(0x00);           //Normal operation mode
        I2C1_restart();
        I2C1_write(general); 
        I2C1_ackstatus();
        I2C1_write(0x00);           //Stored temperature address
        I2C1_ackstatus();
        I2C1_restart();
        general = TEMP_address;        
        general |= 1 << 0;			//set bit for read       
        I2C1_write(general);     
        I2C1_ackstatus();
        MSB_data = I2C1_read();        
        I2C1_ack();
        LSB_data = I2C1_read();        
        I2C1_notack();
        I2C1_stop();
        temperature = MSB_data << 8;
        temperature = temperature + LSB_data;
        temperature = temperature >> 4;
        calculated_temperature = temperature * 0.0625;
        return calculated_temperature;
}

//------------------------------------------
// Check for battery bank data and harvest
// Entry: CAN data to use in the extraction
// Exit: Global struct updated
//------------------------------------------
void extract_fcoil_data(CAN_data_packet PGN_data_extract, unsigned short int buffer_index)
{
    double general_calculation;   
    union both1 {
        unsigned short int short_int;
        struct {
            unsigned b0:1, b1:1, b2:1, b3:1, b4:1, b5:1, b6:1, b7:1, b8:1, b9:1, b10:1, b11:1, b12:1, b13:1, b14:1, b15:1;
        } bits;
    } full_data_set;
    
    union {
        signed short int data_int;
    	unsigned char data_bytes[2];
    } signed_byte_construct;
    union {
			unsigned short int data_int;
			unsigned char data_bytes[2];
	} unsigned_byte_construct;        
    unsigned long message_address;
    unsigned char message_number;
    unsigned long PGN_number;

    
    PGN_number = PGN_data_extract.CAN_PGN;

   
       if (PGN_data_extract.CAN_PGN != 0)
        {            
           full_data_set.short_int = fcoil_data[buffer_index].complete_data_set;        
           //Data harvest
            message_number = 0;
            message_address = (fcoil_device_class * 0x10000) + (message_number * 0x800000) + (PGN_data_message * 0x8000000); 
            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {
                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[1];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[2];
                general_calculation = (double)signed_byte_construct.data_int * 0.01;
                fcoil_data[buffer_index].generator_voltage = general_calculation;

                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[3];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[4];
                general_calculation = (double)signed_byte_construct.data_int * 0.025;
                fcoil_data[buffer_index].generator_current = general_calculation;                

                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[5];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[6];
                general_calculation = (double)signed_byte_construct.data_int * 0.05;
                fcoil_data[buffer_index].diode_temperature = general_calculation;                

                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[7];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[8];
                general_calculation = (double)signed_byte_construct.data_int * 0.05;
                fcoil_data[buffer_index].generator_temperature = general_calculation;                
                
                full_data_set.bits.b0 = 1;          //1
                fcoil_data[buffer_index].complete_data_set = full_data_set.short_int;      
            }         
            
            message_number = 1;
            message_address = (fcoil_device_class * 0x10000) + (message_number * 0x800000) + (PGN_data_message * 0x8000000); 
            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {                
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[1];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[2];       
                fcoil_data[buffer_index].PWM_raw = unsigned_byte_construct.data_int;                
                fcoil_data[buffer_index].PWM_percent = PGN_data_extract.CAN_byte[3];
                fcoil_data[buffer_index].alarm = PGN_data_extract.CAN_byte[4];
                fcoil_data[buffer_index].native_in_gear = PGN_data_extract.CAN_byte[5];        
                fcoil_data[buffer_index].J1939_available = PGN_data_extract.CAN_byte[6];
                fcoil_data[buffer_index].blower_fan_state = PGN_data_extract.CAN_byte[7];               
                fcoil_data[buffer_index].diode_fan_state = PGN_data_extract.CAN_byte[8];

                full_data_set.bits.b1 = 1;          //3
                fcoil_data[buffer_index].complete_data_set = full_data_set.short_int;                      
            }
            
            message_number = 2;
            message_address = (fcoil_device_class * 0x10000) + (message_number * 0x800000) + (PGN_data_message * 0x8000000); 
            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[1];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[2];
                general_calculation = (double)unsigned_byte_construct.data_int * 0.1;
                fcoil_data[buffer_index].fuel_pps = general_calculation;
                
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[3];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[4];
                fcoil_data[buffer_index].generator_RPM = unsigned_byte_construct.data_int;
                

                
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[5];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[6];
                general_calculation = (double)unsigned_byte_construct.data_int * 0.01;
                fcoil_data[buffer_index].available_engine_power = general_calculation;
                
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[7];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[8];
                fcoil_data[buffer_index].engine_RPM_J1939 = unsigned_byte_construct.data_int;
              
                full_data_set.bits.b2 = 1;      //7
                fcoil_data[buffer_index].complete_data_set = full_data_set.short_int;      
            }                 
            
            message_number = 3;
            message_address = (fcoil_device_class * 0x10000) + (message_number * 0x800000) + (PGN_data_message * 0x8000000); 
            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {
                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[1];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[2];
                general_calculation = (double)signed_byte_construct.data_int * 0.01;
                fcoil_data[buffer_index].target_voltage = general_calculation;

                fcoil_data[buffer_index].charge_mode = PGN_data_extract.CAN_byte[3];
                fcoil_data[buffer_index].number_of_banks = PGN_data_extract.CAN_byte[4];

                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[5];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[6];
                general_calculation = (double)signed_byte_construct.data_int * 0.001;
                fcoil_data[buffer_index].field_current = general_calculation;
                
                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[7];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[8];
                general_calculation = (double)signed_byte_construct.data_int * 0.01;
                fcoil_data[buffer_index].field_voltage = general_calculation;
                
                full_data_set.bits.b3 = 1;      //15
                fcoil_data[buffer_index].complete_data_set = full_data_set.short_int;      
            }                              
            
            message_number = 4;
            message_address = (fcoil_device_class * 0x10000) + (message_number * 0x800000) + (PGN_data_message * 0x8000000); 
            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {
                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[1];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[2];
                general_calculation = (double)signed_byte_construct.data_int * 0.05;
                fcoil_data[buffer_index].engine_temperature_J1939 = general_calculation;

                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[3];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[4];
                general_calculation = (double)unsigned_byte_construct.data_int * 0.001;
                fcoil_data[buffer_index].fuel_rate_J1939 = general_calculation;

                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[5];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[6];
                fcoil_data[buffer_index].oil_pressure_J1939 = unsigned_byte_construct.data_int;

                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[7];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[8];
                general_calculation = (double)signed_byte_construct.data_int * 0.05;
                fcoil_data[buffer_index].coolant_temperature_J1939 = general_calculation;
                
                full_data_set.bits.b4 = 1;      //31
                fcoil_data[buffer_index].complete_data_set = full_data_set.short_int;      
            }                        
            
            message_number = 5;
            message_address = (fcoil_device_class * 0x10000) + (message_number * 0x800000) + (PGN_data_message * 0x8000000); 
            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {               
                
                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[1];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[2];
                general_calculation = (double)signed_byte_construct.data_int * 0.05;
                fcoil_data[buffer_index].ambient_temperature = general_calculation;

                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[3];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[4];
                general_calculation = (double)signed_byte_construct.data_int * 0.05;
                fcoil_data[buffer_index].engine_temperature = general_calculation;

                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[5];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[6];
                general_calculation = (double)signed_byte_construct.data_int * 0.05;
                fcoil_data[buffer_index].PCB_temperature = general_calculation;

                fcoil_data[buffer_index].run_mode = PGN_data_extract.CAN_byte[7];
                fcoil_data[buffer_index].cyclical_counter = PGN_data_extract.CAN_byte[8];

                full_data_set.bits.b5 = 1;      //63
                fcoil_data[buffer_index].complete_data_set = full_data_set.short_int;      
            }             
            
            message_number = 6;
            message_address = (fcoil_device_class * 0x10000) + (message_number * 0x800000) + (PGN_data_message * 0x8000000); 
            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {               
                
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[1];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[2];                
                fcoil_data[buffer_index].fuel_level = unsigned_byte_construct.data_int;

                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[3];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[4];
                general_calculation = (double)signed_byte_construct.data_int * 0.01;
                fcoil_data[buffer_index].maximum_generated_voltage = general_calculation;

                fcoil_data[buffer_index].update_parameter_status = PGN_data_extract.CAN_byte[5];
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[6];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[7];
                
                fcoil_data[buffer_index].firmware = unsigned_byte_construct.data_int;
                fcoil_data[buffer_index].J1939_in_gear = PGN_data_extract.CAN_byte[8];

                full_data_set.bits.b6 = 1;      //127
                fcoil_data[buffer_index].complete_data_set = full_data_set.short_int;      
            }             
            message_number = 7;
            message_address = (fcoil_device_class * 0x10000) + (message_number * 0x800000) + (PGN_data_message * 0x8000000); 
            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {                              
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[1];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[2];
                general_calculation = (double)signed_byte_construct.data_int * 0.01;
                fcoil_data[buffer_index].prop_load = general_calculation;

                fcoil_data[buffer_index].clock_minute = PGN_data_extract.CAN_byte[3];
                fcoil_data[buffer_index].clock_hour = PGN_data_extract.CAN_byte[4];
                fcoil_data[buffer_index].clock_day = PGN_data_extract.CAN_byte[5];
                fcoil_data[buffer_index].clock_month = PGN_data_extract.CAN_byte[6];
                fcoil_data[buffer_index].clock_year = PGN_data_extract.CAN_byte[7];
                fcoil_data[buffer_index].pair_index = PGN_data_extract.CAN_byte[8];
                full_data_set.bits.b7 = 1;      //255
                fcoil_data[buffer_index].complete_data_set = full_data_set.short_int;      
            }             
            
            
            
            // Parameter harvest
            full_data_set.short_int = fcoil_data[buffer_index].complete_parameter_set;
            message_number = 0;
            message_address = (fcoil_device_class * 0x10000) + (message_number * 0x800000) + (PGN_current_parameter_message * 0x8000000);                                   
            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[1];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[2];
                general_calculation = (double)unsigned_byte_construct.data_int * 0.01;
                fcoil_data[buffer_index].maximum_engine_power = general_calculation;
                fcoil_data[buffer_index].field_name_index = PGN_data_extract.CAN_byte[3];
        
                fcoil_data[buffer_index].run_mode = PGN_data_extract.CAN_byte[4];
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[5];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[6];
                fcoil_data[buffer_index].maximum_engine_RPM = unsigned_byte_construct.data_int;
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[7];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[8];
                fcoil_data[buffer_index].minimum_engine_RPM = unsigned_byte_construct.data_int;                               
                
                full_data_set.bits.b0 = 1;      //1
                fcoil_data[buffer_index].complete_parameter_set = full_data_set.short_int;                                                                      
            }
            
            message_number = 1;
            message_address = (fcoil_device_class * 0x10000) + (message_number * 0x800000) + (PGN_current_parameter_message * 0x8000000); 
            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[1];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[2];
                fcoil_data[buffer_index].maximum_generator_power = unsigned_byte_construct.data_int; 
                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[3];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[4];
                general_calculation = (double)signed_byte_construct.data_int * 0.05;
                fcoil_data[buffer_index].nominal_generator_temperature = general_calculation;
                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[5];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[6];
                general_calculation = (double)signed_byte_construct.data_int * 0.05;
                fcoil_data[buffer_index].maximum_generator_temperature = general_calculation;
                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[7];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[8];
                general_calculation = (double)signed_byte_construct.data_int * 0.05;
                fcoil_data[buffer_index].emergency_generator_temperature = general_calculation;

                full_data_set.bits.b1 = 1;      //3
                fcoil_data[buffer_index].complete_parameter_set = full_data_set.short_int;                                                                      

            }
            
            message_number = 2;
            message_address = (fcoil_device_class * 0x10000) + (message_number * 0x800000) + (PGN_current_parameter_message * 0x8000000); 
            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {
                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[1];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[2];
                general_calculation = (double)signed_byte_construct.data_int * 0.025; 
                fcoil_data[buffer_index].maximum_generator_current = general_calculation;
                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[3];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[4];
                general_calculation = (double)signed_byte_construct.data_int * 0.05;
                fcoil_data[buffer_index].diode_fan_activation_temperature = general_calculation;
                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[5];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[6];
                general_calculation = (double)signed_byte_construct.data_int * 0.05;
                fcoil_data[buffer_index].diode_fan_max_speed_temperature = general_calculation;
                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[7];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[8];
                general_calculation = (double)signed_byte_construct.data_int * 0.05;
                fcoil_data[buffer_index].blower_fan_activation_temperature = general_calculation;
                full_data_set.bits.b2 = 1;      //7
                fcoil_data[buffer_index].complete_parameter_set = full_data_set.short_int;                                                                                      
            }     
            
            message_number = 3;
            message_address = (fcoil_device_class * 0x10000) + (message_number * 0x800000) + (PGN_current_parameter_message * 0x8000000); 
            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {
                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[1];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[2];
                general_calculation = (double)signed_byte_construct.data_int * 0.01;
                fcoil_data[buffer_index].maximum_charge_voltage = general_calculation;
                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[3];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[4];
                general_calculation = (double)signed_byte_construct.data_int * 0.01;
                fcoil_data[buffer_index].minimum_battery_voltage_to_charge = general_calculation;
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[5];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[6];
                general_calculation = (double)unsigned_byte_construct.data_int * 0.001;
                fcoil_data[buffer_index].generator_to_engine_pulley_ratio = general_calculation;
                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[7];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[8];
                general_calculation = (double)signed_byte_construct.data_int * 0.05;
                fcoil_data[buffer_index].minimum_generator_temperature = general_calculation;
                full_data_set.bits.b3 = 1;      //15
                fcoil_data[buffer_index].complete_parameter_set = full_data_set.short_int;                                                                      
                
            }
            
            message_number = 4;
            message_address = (fcoil_device_class * 0x10000) + (message_number * 0x800000) + (PGN_current_parameter_message * 0x8000000); 
            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {
                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[1];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[2];
                general_calculation = (double)signed_byte_construct.data_int * 0.005;
                fcoil_data[buffer_index].engine_start_temperature = general_calculation;
                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[3];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[4];
                general_calculation = (double)signed_byte_construct.data_int * 0.005;
                fcoil_data[buffer_index].maximum_diode_temperature = general_calculation;
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[5];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[6];
                fcoil_data[buffer_index].maximum_oil_pressure = unsigned_byte_construct.data_int;
                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[7];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[8];
                general_calculation = (double)signed_byte_construct.data_int * 0.005;
                fcoil_data[buffer_index].maximum_coolant_temperature = general_calculation;
                full_data_set.bits.b4 = 1;      //31
                fcoil_data[buffer_index].complete_parameter_set = full_data_set.short_int;                                                                      
            }
       
            message_number = 5;
            message_address = (fcoil_device_class * 0x10000) + (message_number * 0x800000) + (PGN_current_parameter_message * 0x8000000); 
            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[1];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[2];                
                fcoil_data[buffer_index].maximum_fuel_level = unsigned_byte_construct.data_int;
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[3];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[4];
                fcoil_data[buffer_index].maximum_field_coil_power = unsigned_byte_construct.data_int;
                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[5];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[6];
                general_calculation = (double)signed_byte_construct.data_int * 0.01;
                fcoil_data[buffer_index].default_float_safety_voltage = general_calculation;
                fcoil_data[buffer_index].use_1939_gear_detect = PGN_data_extract.CAN_byte[7];
                fcoil_data[buffer_index].pair_index = PGN_data_extract.CAN_byte[8];
                full_data_set.bits.b5 = 1;      //63
                fcoil_data[buffer_index].complete_parameter_set = full_data_set.short_int;                                                                      
            }       

            message_number = 6;
            message_address = (fcoil_device_class * 0x10000) + (message_number * 0x800000) + (PGN_current_parameter_message * 0x8000000); 

            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[1];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[2];
                general_calculation = (double)unsigned_byte_construct.data_int * 0.001;
                fcoil_data[buffer_index].pulse_to_RPM_ratio = general_calculation;                
                full_data_set.bits.b6 = 1;      //127
                fcoil_data[buffer_index].complete_parameter_set = full_data_set.short_int;                 
            }
       }    
}

//------------------------------------------
// Check for battery bank data and harvest
// Entry: CAN data to use in the extraction
// Exit: Global struct updated
//------------------------------------------
void extract_bank_data(CAN_data_packet PGN_data_extract, unsigned short int buffer_index)
{
    double general_calculation, general_maths;   
    union both1 {
        unsigned short int short_int;
        struct {
            unsigned b0:1, b1:1, b2:1, b3:1, b4:1, b5:1, b6:1, b7:1, b8:1, b9:1, b10:1, b11:1, b12:1, b13:1, b14:1, b15:1;
        } bits;
    } full_data_set;
    
    union {
        signed short int data_int;
    	unsigned char data_bytes[2];
    } signed_byte_construct;
    union {
			unsigned short int data_int;
			unsigned char data_bytes[2];
	} unsigned_byte_construct;        
    unsigned long message_address;
    unsigned char message_number;
    unsigned long PGN_number;
    unsigned char bank_device_class;
    
    PGN_number = PGN_data_extract.CAN_PGN;

   
       if (PGN_data_extract.CAN_PGN != 0)
        {            
           full_data_set.short_int = bank_data[buffer_index].complete_data_set;
           bank_device_class = bank_data[buffer_index].CAN_device_class;
           bank_data[buffer_index].device_class_set = bank_device_class;
           //Data harvest
            message_number = 0;
            message_address = (bank_device_class * 0x10000) + (message_number * 0x800000) + (PGN_data_message * 0x8000000); 
            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {
                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[1];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[2];
                general_calculation = (double)signed_byte_construct.data_int * 0.01;
                bank_data[buffer_index].bank_voltage = general_calculation;

                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[3];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[4];
                general_calculation = (double)signed_byte_construct.data_int * 0.025;
                bank_data[buffer_index].DC_current = general_calculation;                
                
                general_calculation = (double)PGN_data_extract.CAN_byte[5] * 0.5;
                bank_data[buffer_index].actual_SoC = general_calculation;
                general_calculation = (double)PGN_data_extract.CAN_byte[6] * 0.5;
                bank_data[buffer_index].reference_SoC = general_calculation;  
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[7];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[8];
                bank_data[buffer_index].time_to_empty = unsigned_byte_construct.data_int;    
                
                full_data_set.bits.b0 = 1;          //1
                bank_data[buffer_index].complete_data_set = full_data_set.short_int;      
            }         
            
            message_number = 1;
            message_address = (bank_device_class * 0x10000) + (message_number * 0x800000) + (PGN_data_message * 0x8000000); 
            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {
                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[1];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[2];
                general_calculation = (double)signed_byte_construct.data_int * 0.005;
                bank_data[buffer_index].block_temperature1 = general_calculation;

                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[3];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[4];
                general_calculation = (double)signed_byte_construct.data_int * 0.005;
                bank_data[buffer_index].block_temperature2 = general_calculation;

                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[5];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[6];
                general_calculation = (double)signed_byte_construct.data_int * 0.005;
                bank_data[buffer_index].block_temperature3 = general_calculation;

                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[7];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[8];
                general_calculation = (double)signed_byte_construct.data_int * 0.005;
                bank_data[buffer_index].block_temperature4 = general_calculation; 

                full_data_set.bits.b1 = 1;          //3
                bank_data[buffer_index].complete_data_set = full_data_set.short_int;                      
            }
            
            message_number = 2;
            message_address = (bank_device_class * 0x10000) + (message_number * 0x800000) + (PGN_data_message * 0x8000000); 
            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {
                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[1];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[2];
                general_calculation = (double)signed_byte_construct.data_int * 0.01;
                bank_data[buffer_index].block_voltage1 = general_calculation;

                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[3];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[4];
                general_calculation = (double)signed_byte_construct.data_int * 0.01;
                bank_data[buffer_index].block_voltage2 = general_calculation;

                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[5];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[6];
                general_calculation = (double)signed_byte_construct.data_int * 0.01;
                bank_data[buffer_index].block_voltage3 = general_calculation;

                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[7];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[8];
                general_calculation = (double)signed_byte_construct.data_int * 0.01;
                bank_data[buffer_index].block_voltage4 = general_calculation;  
                
                full_data_set.bits.b2 = 1;      //7
                bank_data[buffer_index].complete_data_set = full_data_set.short_int;      
            }                 
            
            message_number = 3;
            message_address = (bank_device_class * 0x10000) + (message_number * 0x800000) + (PGN_data_message * 0x8000000); 
            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {
               
                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[1];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[2];
                general_calculation = (double)signed_byte_construct.data_int * 0.01;
                bank_data[buffer_index].absorption_charge_voltage = general_calculation;

                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[3];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[4];
                general_calculation = (double)signed_byte_construct.data_int * 0.01;
                bank_data[buffer_index].float_charge_voltage = general_calculation;

                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[5];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[6];
                general_calculation = (double)signed_byte_construct.data_int * 0.01;
                bank_data[buffer_index].equalisation_voltage = general_calculation;
                               
                bank_data[buffer_index].charge_mode = PGN_data_extract.CAN_byte[7];
                bank_data[buffer_index].cyclical_counter = PGN_data_extract.CAN_byte[8];

                full_data_set.bits.b3 = 1;      //15
                bank_data[buffer_index].complete_data_set = full_data_set.short_int;      
            }                              
            
            message_number = 4;
            message_address = (bank_device_class * 0x10000) + (message_number * 0x800000) + (PGN_data_message * 0x8000000); 
            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[1];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[2];
                bank_data[buffer_index].time_till_float = unsigned_byte_construct.data_int;
                bank_data[buffer_index].sensor_name_index = PGN_data_extract.CAN_byte[3];
                bank_data[buffer_index].updated_parameter_status = PGN_data_extract.CAN_byte[4];
                
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[5];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[6];
                general_calculation = (double)unsigned_byte_construct.data_int * 0.001;
                bank_data[buffer_index].firmware_version = general_calculation;

                bank_data[buffer_index].current_polarity = PGN_data_extract.CAN_byte[7];
                bank_data[buffer_index].alarm = PGN_data_extract.CAN_byte[8];

                full_data_set.bits.b4 = 1;      //31
                bank_data[buffer_index].complete_data_set = full_data_set.short_int;      
            }                        
            
            message_number = 5;
            message_address = (bank_device_class * 0x10000) + (message_number * 0x800000) + (PGN_data_message * 0x8000000); 
            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {               
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[1];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[2];
                general_calculation = (double)unsigned_byte_construct.data_int * 0.1;
                bank_data[buffer_index].lifetime_kWh_in = general_calculation;
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[3];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[4];
                general_calculation = (double)unsigned_byte_construct.data_int * 0.1;
                bank_data[buffer_index].lifetime_kWh_out = general_calculation;
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[5];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[6];
                general_calculation = (double)unsigned_byte_construct.data_int * 0.01;
                bank_data[buffer_index].kWh_in_since_float = general_calculation;
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[7];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[8];
                general_calculation = (double)unsigned_byte_construct.data_int * 0.01;
                bank_data[buffer_index].kWh_out_since_float = general_calculation;

                full_data_set.bits.b5 = 1;      //63
                bank_data[buffer_index].complete_data_set = full_data_set.short_int;      
            }             
            // Parameter harvest
            full_data_set.short_int = bank_data[buffer_index].complete_parameter_set;
            message_number = 0;
            message_address = (bank_device_class * 0x10000) + (message_number * 0x800000) + (PGN_current_parameter_message * 0x8000000);                                   
            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {
                
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[1];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[2];
                bank_data[buffer_index].bank_Ah_capacity = unsigned_byte_construct.data_int;
                general_calculation = (double)PGN_data_extract.CAN_byte[3] * 0.5;
                bank_data[buffer_index].charge_efficiency = (unsigned char)general_calculation;
                general_calculation = (double)PGN_data_extract.CAN_byte[4] * 0.5;
                bank_data[buffer_index].discharge_efficiency = (unsigned char)general_calculation;
                bank_data[buffer_index].battery_chemistry = PGN_data_extract.CAN_byte[5];
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[6];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[7];
                general_calculation = (double)unsigned_byte_construct.data_int * 0.002;
                bank_data[buffer_index].nominal_block_voltage = general_calculation;
                bank_data[buffer_index].rated_discharge_time = PGN_data_extract.CAN_byte[8];
                full_data_set.bits.b0 = 1;      //1
                bank_data[buffer_index].complete_parameter_set = full_data_set.short_int;                                                                      
            }
            
            message_number = 1;
            message_address = (bank_device_class * 0x10000) + (message_number * 0x800000) + (PGN_current_parameter_message * 0x8000000); 
            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[1];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[2];
                general_calculation = (double)unsigned_byte_construct.data_int * 0.002;
                bank_data[buffer_index].absorption_voltage = general_calculation;
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[3];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[4];
                general_calculation = (double)unsigned_byte_construct.data_int * 0.002;
                bank_data[buffer_index].float_voltage = general_calculation;
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[5];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[6];
                general_calculation = (double)unsigned_byte_construct.data_int * 0.00003;
                bank_data[buffer_index].Peukerts_constant = general_calculation;
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[7];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[8];
                general_calculation = (double)unsigned_byte_construct.data_int * 0.015;
                bank_data[buffer_index].temperature_compensation = general_calculation;
                full_data_set.bits.b1 = 1;      //3
                bank_data[buffer_index].complete_parameter_set = full_data_set.short_int;                                                                      

            }
            
            message_number = 2;
            message_address = (bank_device_class * 0x10000) + (message_number * 0x800000) + (PGN_current_parameter_message * 0x8000000); 
            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {
                general_calculation = (double)PGN_data_extract.CAN_byte[1] * 0.5;
                bank_data[buffer_index].min_SoC = (unsigned char)general_calculation;
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[2];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[3];                
                bank_data[buffer_index].lifetime_kWh = unsigned_byte_construct.data_int;
                bank_data[buffer_index].time_before_OCV_reset = PGN_data_extract.CAN_byte[4];                
                bank_data[buffer_index].max_time_at_absorption = PGN_data_extract.CAN_byte[5];                
                bank_data[buffer_index].max_bulk_current = PGN_data_extract.CAN_byte[6];                
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[7];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[8];
                general_calculation = (double)unsigned_byte_construct.data_int * 0.001;
                bank_data[buffer_index].full_charge_current = general_calculation;
                full_data_set.bits.b2 = 1;      //7
                bank_data[buffer_index].complete_parameter_set = full_data_set.short_int;                                                                                      
            }     
            
            message_number = 3;
            message_address = (bank_device_class * 0x10000) + (message_number * 0x800000) + (PGN_current_parameter_message * 0x8000000); 
            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {
                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[1];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[2];
                general_calculation = (double)signed_byte_construct.data_int * 0.005;
                bank_data[buffer_index].min_datum_temperature = general_calculation;
                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[3];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[4];
                general_calculation = (double)signed_byte_construct.data_int * 0.005;
                bank_data[buffer_index].max_datum_temperature = general_calculation;
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[5];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[6];
                general_calculation = (double)unsigned_byte_construct.data_int * 0.000153;
                bank_data[buffer_index].OCV_current_limit = general_calculation;
                bank_data[buffer_index].pair_index = PGN_data_extract.CAN_byte[7];
                bank_data[buffer_index].number_of_blocks = PGN_data_extract.CAN_byte[8];
                full_data_set.bits.b3 = 1;      //15
                bank_data[buffer_index].complete_parameter_set = full_data_set.short_int;                                                                      
                
            }
            
            message_number = 4;
            message_address = (bank_device_class * 0x10000) + (message_number * 0x800000) + (PGN_current_parameter_message * 0x8000000); 
            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {
                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[1];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[2];
                general_calculation = (double)signed_byte_construct.data_int * 0.005;
                bank_data[buffer_index].minimum_absorption_temperature = general_calculation;
                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[3];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[4];
                general_calculation = (double)signed_byte_construct.data_int * 0.005;
                bank_data[buffer_index].maximum_absorption_temperature = general_calculation;
                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[5];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[6];
                general_calculation = (double)signed_byte_construct.data_int * 0.005;
                bank_data[buffer_index].minimum_float_temperature = general_calculation;
                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[7];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[8];
                general_calculation = (double)signed_byte_construct.data_int * 0.005;
                bank_data[buffer_index].maximum_float_temperature = general_calculation;
                full_data_set.bits.b4 = 1;      //31
                bank_data[buffer_index].complete_parameter_set = full_data_set.short_int;                                                                      
            }
       
            message_number = 5;
            message_address = (bank_device_class * 0x10000) + (message_number * 0x800000) + (PGN_current_parameter_message * 0x8000000); 
            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[1];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[2];
                general_calculation = (double)unsigned_byte_construct.data_int * 0.002;
                bank_data[buffer_index].minimum_absorption_block_voltage = general_calculation;
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[3];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[4];
                general_calculation = (double)unsigned_byte_construct.data_int * 0.002;
                bank_data[buffer_index].maximum_absorption_block_voltage = general_calculation;
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[5];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[6];
                general_calculation = (double)unsigned_byte_construct.data_int * 0.002;
                bank_data[buffer_index].minimum_float_block_voltage = general_calculation;
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[7];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[8];
                general_calculation = (double)unsigned_byte_construct.data_int * 0.002;
                bank_data[buffer_index].maximum_float_block_voltage = general_calculation;
                full_data_set.bits.b5 = 1;      //63
                bank_data[buffer_index].complete_parameter_set = full_data_set.short_int;                                                                      
            }       

            message_number = 6;
            message_address = (bank_device_class * 0x10000) + (message_number * 0x800000) + (PGN_current_parameter_message * 0x8000000); 

            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[1];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[2];
                general_calculation = (double)unsigned_byte_construct.data_int * 0.002;
                bank_data[buffer_index].centre_block_voltage_trigger = general_calculation;
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[3];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[4];
                general_calculation = (double)unsigned_byte_construct.data_int * 0.001;
                bank_data[buffer_index].current_threshold_trigger = general_calculation;
                bank_data[buffer_index].sensor_name_index = PGN_data_extract.CAN_byte[5];
                bank_data[buffer_index].current_polarity = PGN_data_extract.CAN_byte[6];                
                bank_data[buffer_index].device_class_set = PGN_data_extract.CAN_byte[8];
                
                full_data_set.bits.b6 = 1;      //127
                bank_data[buffer_index].complete_parameter_set = full_data_set.short_int; 
                
            }
            
            message_number = 7;
            message_address = (bank_device_class * 0x10000) + (message_number * 0x800000) + (PGN_current_parameter_message * 0x8000000); 

            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {            
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[1];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[2];
                bank_data[buffer_index].battery_manufacturer = unsigned_byte_construct.data_int;
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[3];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[4];            
                bank_data[buffer_index].battery_model = unsigned_byte_construct.data_int;
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[5];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[6];
                general_maths = (double)unsigned_byte_construct.data_int * 0.002;
                bank_data[buffer_index].Lithium_bulk_target_voltage = general_maths;
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[7];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[8];
                general_maths = (double)unsigned_byte_construct.data_int * 0.002;
                bank_data[buffer_index].Lithium_float_target_voltage = general_maths;
                full_data_set.bits.b7 = 1;
                bank_data[buffer_index].complete_parameter_set = full_data_set.short_int; 
            }

            message_number = 8;
            message_address = (bank_device_class * 0x10000) + (message_number * 0x800000) + (PGN_current_parameter_message * 0x8000000); 

            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {    
                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[1];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[2];
                general_maths = (double)signed_byte_construct.data_int * 0.005;
                bank_data[buffer_index].Lithium_min_charge_temperature = general_maths;
                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[3];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[4];
                general_maths = (double)signed_byte_construct.data_int * 0.005;
                bank_data[buffer_index].Lithium_max_charge_temperature = general_maths;
                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[5];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[6];
                general_maths = (double)signed_byte_construct.data_int * 0.005;
                bank_data[buffer_index].Lithium_min_discharge_temperature = general_maths;
                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[7];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[8];
                general_maths = (double)signed_byte_construct.data_int * 0.005;
                bank_data[buffer_index].Lithium_max_discharge_temperature = general_maths;
                full_data_set.bits.b8 = 1;
                bank_data[buffer_index].complete_parameter_set = full_data_set.short_int; 
            }

            message_number = 9;
            message_address = (bank_device_class * 0x10000) + (message_number * 0x800000) + (PGN_current_parameter_message * 0x8000000); 

            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {    
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[1];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[2];
                general_maths = (double)unsigned_byte_construct.data_int * 0.002;
                bank_data[buffer_index].Lithium_min_voltage = general_maths;
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[3];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[4];
                general_maths = (double)unsigned_byte_construct.data_int * 0.002;
                bank_data[buffer_index].Lithium_max_voltage = general_maths;
                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[5];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[6];
                general_maths = (double)signed_byte_construct.data_int * 0.025;
                bank_data[buffer_index].Lithium_max_discharge_current = general_maths;
                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[7];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[8];
                general_maths = (double)signed_byte_construct.data_int * 0.025;
                bank_data[buffer_index].Lithium_max_charge_current = general_maths;
                full_data_set.bits.b9 = 1;
                bank_data[buffer_index].complete_parameter_set = full_data_set.short_int;                 
            }    

            message_number = 10;
            message_address = (bank_device_class * 0x10000) + (message_number * 0x800000) + (PGN_current_parameter_message * 0x8000000); 

            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[1];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[2];
                general_maths = (double)unsigned_byte_construct.data_int * 0.002;
                bank_data[buffer_index].Lithium_cycle_reset_SoC = general_maths;
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[3];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[4];
                general_maths = (double)unsigned_byte_construct.data_int * 0.002;
                bank_data[buffer_index].Lithium_float_trip_accuracy = general_maths;
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[5];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[6];
                general_maths = (double)unsigned_byte_construct.data_int * 0.002;
                bank_data[buffer_index].Lithium_safe_fallback_voltage = general_maths;
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[7];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[8];
                general_maths = (double)unsigned_byte_construct.data_int * 0.002;
                bank_data[buffer_index].warning_voltage = general_maths;
                full_data_set.bits.b10 = 1;
                bank_data[buffer_index].complete_parameter_set = full_data_set.short_int;                 
            }    

            message_number = 11;
            message_address = (bank_device_class * 0x10000) + (message_number * 0x800000) + (PGN_current_parameter_message * 0x8000000); 

            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {                
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[1];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[2];
                general_maths = (double)unsigned_byte_construct.data_int * 0.002;
                bank_data[buffer_index].disconnect_voltage = general_maths;
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[3];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[4];
                general_maths = (double)unsigned_byte_construct.data_int * 0.002;
                bank_data[buffer_index].warning_SoC = general_maths;
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[5];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[6];
                general_maths = (double)unsigned_byte_construct.data_int * 0.002;
                bank_data[buffer_index].disconnect_SoC = general_maths;
                bank_data[buffer_index].use_pair_index = PGN_data_extract.CAN_byte[7];
                full_data_set.bits.b11 = 1;
                bank_data[buffer_index].complete_parameter_set = full_data_set.short_int;                 
            }
                              
            
       }    
}

//-----------------------------------
// Sample and store attached sensors
// Entry: CAN packet
// Exit: Success or not
//-----------------------------------
unsigned char CAN_data_process(CAN_data_packet CAN_incoming_data)
{
    signed short int ID_search;
    unsigned long filtered_unique_ID;
    signed short int ID_already_exists;
    volatile unsigned long CAN_device_class;
    unsigned long message_number, message_type;

    
        ID_already_exists = -1;
        if (CAN_incoming_data.CAN_PGN == 0) return CAN_ACC_INVALID_DATA;

        filtered_unique_ID = (CAN_incoming_data.CAN_PGN & 0xFFFF);
        CAN_device_class = (CAN_incoming_data.CAN_PGN & 0x007F0000) >> 16;
        message_number = (CAN_incoming_data.CAN_PGN & 0x7800000) >> 23;
        message_type = (CAN_incoming_data.CAN_PGN & 0x18000000) >> 27; 
                       
        if ((CAN_device_class == bank_48V_device_class) || (CAN_device_class == bank_24V_device_class) || (CAN_device_class == bank_12V_device_class))
        {
            ID_already_exists = -1;   
 
            for (ID_search = 0; ID_search < bank_sensor_max; ID_search++)
            {
                if (bank_data[ID_search].ID_number == filtered_unique_ID)
                {
                    bank_data[ID_search].CAN_device_class = CAN_device_class;
                    ID_already_exists = ID_search;
                    break;
                }
            }

            if (ID_already_exists == -1)
            {
                for (ID_search = 0; ID_search < bank_sensor_max; ID_search++)
                {
                    if ((bank_data[ID_search].ID_number == 0) && (ID_already_exists == -1))
                    {
                        bank_data[ID_search].ID_number = filtered_unique_ID;
                        bank_data[ID_search].CAN_device_class = CAN_device_class;
                        bank_data[ID_search].last_message_received = (CAN_incoming_data.CAN_PGN & 0x07800000) >> 23;
                        ID_already_exists = ID_search;                           
                        break;
                    }
                }
            }
            if (ID_already_exists == -1) return CAN_ACC_FULL;
            extract_bank_data(CAN_incoming_data, ID_already_exists);
        }
        
        if (CAN_device_class == fcoil_device_class)
        {

            ID_already_exists = -1;   
 
            for (ID_search = 0; ID_search < fcoil_max; ID_search++)
            {
                if (fcoil_data[ID_search].ID_number == filtered_unique_ID)
                {
                    fcoil_data[ID_search].CAN_device_class = CAN_device_class;
                    ID_already_exists = ID_search;
                    break;
                }
            }

            if (ID_already_exists == -1)
            {
                for (ID_search = 0; ID_search < fcoil_max; ID_search++)
                {
                    if ((fcoil_data[ID_search].ID_number == 0) && (ID_already_exists == -1))
                    {
                        fcoil_data[ID_search].ID_number = filtered_unique_ID;
                        fcoil_data[ID_search].CAN_device_class = CAN_device_class;
                        fcoil_data[ID_search].last_message_received = (CAN_incoming_data.CAN_PGN & 0x07800000) >> 23;
                        ID_already_exists = ID_search;                           
                        break;
                    }
                }
            }
            if (ID_already_exists == -1) return CAN_ACC_FULL;
            if (message_type < 2) extract_fcoil_data(CAN_incoming_data, ID_already_exists);                                    
        }      
        
        if (CAN_device_class == BPG_device_class)
        {

            if (message_type == 2)
            {
                if (filtered_unique_ID == unique_16bit_ID) extract_BPG_new_parameters(CAN_incoming_data, ID_already_exists);  
                return CAN_ACC_SUCCESS;
            }
            
            ID_already_exists = -1;   
 
            for (ID_search = 1; ID_search < BPG_max; ID_search++)
            {
                if (BPG_data[ID_search].ID_number == filtered_unique_ID)
                {
                    BPG_data[ID_search].CAN_device_class = CAN_device_class;
                    ID_already_exists = ID_search;
                    break;
                }
            }

            if (ID_already_exists == -1)
            {
                for (ID_search = 1; ID_search < BPG_max; ID_search++)
                {
                    if ((BPG_data[ID_search].ID_number == 0) && (ID_already_exists == -1))
                    {
                        BPG_data[ID_search].ID_number = filtered_unique_ID;
                        BPG_data[ID_search].CAN_device_class = CAN_device_class;
                        BPG_data[ID_search].last_message_received = (CAN_incoming_data.CAN_PGN & 0x07800000) >> 23;
                        ID_already_exists = ID_search;                           
                        break;
                    }
                }
            }
            if (ID_already_exists == -1) return CAN_ACC_FULL;
            if (message_type < 2) extract_BPG_data(CAN_incoming_data, ID_already_exists);                                    
                             
        }        
        if (CAN_device_class == TQ_BMS_device_class)
        {        
            if (message_type == 2)
            {
                if (filtered_unique_ID == unique_16bit_ID) extract_TQ_BMS_new_parameters(CAN_incoming_data);  
                return CAN_ACC_SUCCESS;
            }        
        }
        
        
        if ((CAN_device_class == panic_device_class) && (message_type == 2))
        {      
            process_panic(CAN_incoming_data);
        }
        
       
    return CAN_ACC_SUCCESS;
}


void extract_BPG_data(CAN_data_packet PGN_data_extract, unsigned short int buffer_index)
{
    double general_calculation;   
    union both1 {
        unsigned short int short_int;
        struct {
            unsigned b0:1, b1:1, b2:1, b3:1, b4:1, b5:1, b6:1, b7:1, b8:1, b9:1, b10:1, b11:1, b12:1, b13:1, b14:1, b15:1;
        } bits;
    } full_data_set;
    
    union {
        signed short int data_int;
    	unsigned char data_bytes[2];
    } signed_byte_construct;
    union {
			unsigned short int data_int;
			unsigned char data_bytes[2];
	} unsigned_byte_construct;        
    unsigned long message_address;
    unsigned char message_number;
    unsigned long PGN_number;
    unsigned char bank_device_class;
    
        PGN_number = PGN_data_extract.CAN_PGN;
   
        if (PGN_data_extract.CAN_PGN != 0)
        {            
           full_data_set.short_int = BPG_data[buffer_index].complete_data_set;
           bank_device_class = BPG_data[buffer_index].CAN_device_class;
           
           //Data harvest
            message_number = 0;
            message_address = (BPG_device_class * 0x10000) + (message_number * 0x800000) + (PGN_data_message * 0x8000000); 
            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {
                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[1];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[2];
                general_calculation = (double)signed_byte_construct.data_int * 0.01;
                BPG_data[buffer_index].bank_switch_voltage = general_calculation;

                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[3];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[4];
                general_calculation = (double)signed_byte_construct.data_int * 0.025;
                BPG_data[buffer_index].maximum_bank_current = general_calculation;                
                
                BPG_data[buffer_index].current_switch_position = PGN_data_extract.CAN_byte[5];
                BPG_data[buffer_index].last_disconnect_reason = PGN_data_extract.CAN_byte[6];
                
                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[7];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[8];
                general_calculation = (double)signed_byte_construct.data_int * 0.005;
                BPG_data[buffer_index].ambient_temperature = general_calculation;                
                
                full_data_set.bits.b0 = 1;          //1
                BPG_data[buffer_index].complete_data_set = full_data_set.short_int;      
            }   
            
            message_number = 1;
            message_address = (BPG_device_class * 0x10000) + (message_number * 0x800000) + (PGN_data_message * 0x8000000); 
            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {
                
                BPG_data[buffer_index].cyclical_counter = PGN_data_extract.CAN_byte[1];
                BPG_data[buffer_index].name_index = PGN_data_extract.CAN_byte[2];
                BPG_data[buffer_index].pair_index = PGN_data_extract.CAN_byte[3];
                BPG_data[buffer_index].alarm = PGN_data_extract.CAN_byte[4];
                
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[5];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[6];
                general_calculation = (double)unsigned_byte_construct.data_int * 0.001;
                BPG_data[buffer_index].IO_signal1_voltage_no_discharge = general_calculation;

                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[7];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[8];
                general_calculation = (double)unsigned_byte_construct.data_int * 0.001;
                BPG_data[buffer_index].IO_signal2_voltage_no_charge = general_calculation;                
                
                full_data_set.bits.b1 = 1;          //2
                BPG_data[buffer_index].complete_data_set = full_data_set.short_int;      
            }   

            message_number = 2;
            message_address = (BPG_device_class * 0x10000) + (message_number * 0x800000) + (PGN_data_message * 0x8000000); 
            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {
                
                BPG_data[buffer_index].RS232_data_present = PGN_data_extract.CAN_byte[1];
                BPG_data[buffer_index].RS485_data_present = PGN_data_extract.CAN_byte[2];
                BPG_data[buffer_index].battery_CAN_present = PGN_data_extract.CAN_byte[3];
                
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[4];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[5];
                BPG_data[buffer_index].firmware = unsigned_byte_construct.data_int;
                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[6];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[7];
                general_calculation = (double)signed_byte_construct.data_int * 0.005;
                BPG_data[buffer_index].battery_thermistor_1 = general_calculation;
                BPG_data[buffer_index].fan_state = PGN_data_extract.CAN_byte[8];                
                
                full_data_set.bits.b2 = 1;          //4
                BPG_data[buffer_index].complete_data_set = full_data_set.short_int;      
            }   
            
            message_number = 3;
            message_address = (BPG_device_class * 0x10000) + (message_number * 0x800000) + (PGN_data_message * 0x8000000); 
            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {               
                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[1];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[2];
                general_calculation = (double)signed_byte_construct.data_int * 0.005;
                BPG_data[buffer_index].battery_thermistor_2 = general_calculation;
                BPG_data[buffer_index].updated_parameter_status = PGN_data_extract.CAN_byte[3];
                
                BPG_data[buffer_index].BMS_block_count = PGN_data_extract.CAN_byte[4];                
                BPG_data[buffer_index].BMS_live_block_count = PGN_data_extract.CAN_byte[5];                
                full_data_set.bits.b3 = 1;          //4
                BPG_data[buffer_index].complete_data_set = full_data_set.short_int;      
            }   
            //message 4 is the log download
            
            //parameter harvest
            full_data_set.short_int = BPG_data[buffer_index].complete_parameter_set;
            message_number = 0;
            message_address = (BPG_device_class * 0x10000) + (message_number * 0x800000) + (PGN_current_parameter_message * 0x8000000);                                   
            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {
                
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[1];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[2];
                BPG_data[buffer_index].battery_manufacturer = unsigned_byte_construct.data_int;
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[3];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[4];
                BPG_data[buffer_index].battery_model = unsigned_byte_construct.data_int;
                BPG_data[buffer_index].interface_type_to_use = PGN_data_extract.CAN_byte[5];
                BPG_data[buffer_index].use_IO_interface = PGN_data_extract.CAN_byte[6];
                BPG_data[buffer_index].allow_remote_switching = PGN_data_extract.CAN_byte[7];
                BPG_data[buffer_index].pair_index = PGN_data_extract.CAN_byte[8];                
                
                full_data_set.bits.b0 = 1;      //1
                BPG_data[buffer_index].complete_parameter_set = full_data_set.short_int;                                                                      
            }

            message_number = 1;
            message_address = (BPG_device_class * 0x10000) + (message_number * 0x800000) + (PGN_current_parameter_message * 0x8000000);                                   
            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[1];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[2];
                general_calculation = (double)unsigned_byte_construct.data_int * 0.001;
                BPG_data[buffer_index].low_voltage_disconnect = general_calculation;
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[3];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[4];
                general_calculation = (double)unsigned_byte_construct.data_int * 0.001;
                BPG_data[buffer_index].high_voltage_disconnect = general_calculation;
                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[5];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[6];
                general_calculation = (double)signed_byte_construct.data_int * 0.005;
                BPG_data[buffer_index].low_temperature_disconnect = general_calculation;
                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[7];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[8];
                general_calculation = (double)signed_byte_construct.data_int * 0.005;
                BPG_data[buffer_index].high_temperature_disconnect = general_calculation;

                full_data_set.bits.b1 = 1;      //2
                BPG_data[buffer_index].complete_parameter_set = full_data_set.short_int;                                                                      
            }
            
            message_number = 2;
            message_address = (BPG_device_class * 0x10000) + (message_number * 0x800000) + (PGN_current_parameter_message * 0x8000000);                                   
            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[1];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[2];
                general_calculation = (double)unsigned_byte_construct.data_int * 0.001;
                BPG_data[buffer_index].low_voltage_reconnect = general_calculation;
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[3];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[4];
                general_calculation = (double)unsigned_byte_construct.data_int * 0.001;
                BPG_data[buffer_index].high_voltage_reconnect = general_calculation;
                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[5];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[6];
                general_calculation = (double)signed_byte_construct.data_int * 0.005;
                BPG_data[buffer_index].low_temperature_reconnect = general_calculation;
                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[7];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[8];
                general_calculation = (double)signed_byte_construct.data_int * 0.005;
                BPG_data[buffer_index].high_temperature_reconnect = general_calculation;

                full_data_set.bits.b2 = 1;      //4
                BPG_data[buffer_index].complete_parameter_set = full_data_set.short_int;                                                                      
            }            

            message_number = 3;
            message_address = (BPG_device_class * 0x10000) + (message_number * 0x800000) + (PGN_current_parameter_message * 0x8000000);                                   
            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {
                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[1];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[2];
                general_calculation = (double)signed_byte_construct.data_int * 0.025;
                BPG_data[buffer_index].high_current_disconnect = general_calculation;
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[3];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[4];
                general_calculation = (double)unsigned_byte_construct.data_int * 0.001;
                BPG_data[buffer_index].BMS_IO_trigger_voltage = general_calculation;
                BPG_data[buffer_index].voltage_time_hysteresis = PGN_data_extract.CAN_byte[5];
                BPG_data[buffer_index].temperature_time_hysteresis = PGN_data_extract.CAN_byte[6];
                BPG_data[buffer_index].current_time_hysteresis = PGN_data_extract.CAN_byte[7];
                BPG_data[buffer_index].BMS_IO_disconnect_delay = PGN_data_extract.CAN_byte[8];               
                
                full_data_set.bits.b3 = 1;      //8
                BPG_data[buffer_index].complete_parameter_set = full_data_set.short_int;                                                                      
            }            

            message_number = 4;
            message_address = (BPG_device_class * 0x10000) + (message_number * 0x800000) + (PGN_current_parameter_message * 0x8000000);                                   
            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {
                BPG_data[buffer_index].scan_only_paired_bank_current = PGN_data_extract.CAN_byte[1];
                BPG_data[buffer_index].bank_class_currents_to_scan = PGN_data_extract.CAN_byte[2];
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[3];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[4];
                BPG_data[buffer_index].voltage_measurement_wake_interval = unsigned_byte_construct.data_int;
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[5];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[6];
                BPG_data[buffer_index].no_Argo_activity_sleep_interval = unsigned_byte_construct.data_int;

                full_data_set.bits.b4 = 1;      //8
                BPG_data[buffer_index].complete_parameter_set = full_data_set.short_int;                                                                      
            }   
            
            message_number = 5;
            message_address = (BPG_device_class * 0x10000) + (message_number * 0x800000) + (PGN_current_parameter_message * 0x8000000);                                   
            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {
                BPG_data[buffer_index].enable_OCV_SoC_reset = PGN_data_extract.CAN_byte[1];
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[2];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[3];                
                BPG_data[buffer_index].time_before_OCV_SoC_reset = unsigned_byte_construct.data_int;
                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[4];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[5];
                general_calculation = (double)signed_byte_construct.data_int * 0.000153;
                BPG_data[buffer_index].OCV_SoC_reset_current_limit = general_calculation;
                BPG_data[buffer_index].name_index = PGN_data_extract.CAN_byte[6];
                BPG_data[buffer_index].charge_override_time_hysteresis = PGN_data_extract.CAN_byte[7];
                full_data_set.bits.b5 = 1;      //16
                BPG_data[buffer_index].complete_parameter_set = full_data_set.short_int;                                                                      
            }                
        }
}

void extract_BPG_new_parameters(CAN_data_packet PGN_data_extract, unsigned short int buffer_index)
{
    union {
        signed short int data_int;
    	unsigned char data_bytes[2];
    } signed_byte_construct;
    union {
			unsigned short int data_int;
			unsigned char data_bytes[2];
	} unsigned_byte_construct;        

    union both1 {
        unsigned short int short_int;
        struct {
            unsigned b0:1, b1:1, b2:1, b3:1, b4:1, b5:1, b6:1, b7:1, b8:1, b9:1, b10:1, b11:1, b12:1, b13:1, b14:1, b15:1;
        } bits;
    } full_data_set;
    unsigned long message_address;
    unsigned char message_number;
    double general_calculation;
     
            full_data_set.short_int = new_parameters.complete_parameter_set;
            message_number = 0;
            message_address = (BPG_device_class * 0x10000) + (message_number * 0x800000) + (PGN_new_parameter_message * 0x8000000);                                   
            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {
                
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[1];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[2];
                new_parameters.battery_manufacturer = unsigned_byte_construct.data_int;
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[3];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[4];
                new_parameters.battery_model = unsigned_byte_construct.data_int;
                new_parameters.interface_type_to_use = PGN_data_extract.CAN_byte[5];
                new_parameters.use_IO_interface = PGN_data_extract.CAN_byte[6];
                new_parameters.allow_remote_switching = PGN_data_extract.CAN_byte[7];
                new_parameters.pair_index = PGN_data_extract.CAN_byte[8];                
                
                full_data_set.bits.b0 = 1;      //1
                new_parameters.complete_parameter_set = full_data_set.short_int;                                                                      
            }

            message_number = 1;
            message_address = (BPG_device_class * 0x10000) + (message_number * 0x800000) + (PGN_new_parameter_message * 0x8000000);                                   
            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[1];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[2];
                general_calculation = (double)unsigned_byte_construct.data_int;
                new_parameters.low_voltage_disconnect = general_calculation * 0.001;
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[3];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[4];
                general_calculation = (double)unsigned_byte_construct.data_int;
                new_parameters.high_voltage_disconnect = general_calculation * 0.001;
                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[5];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[6];
                general_calculation = (double)signed_byte_construct.data_int;
                new_parameters.low_temperature_disconnect = general_calculation * 0.005;
                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[7];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[8];
                general_calculation = (double)signed_byte_construct.data_int;
                new_parameters.high_temperature_disconnect = general_calculation * 0.005;

                full_data_set.bits.b1 = 1;      //2
                new_parameters.complete_parameter_set = full_data_set.short_int;                                                                      
            }
            
            message_number = 2;
            message_address = (BPG_device_class * 0x10000) + (message_number * 0x800000) + (PGN_new_parameter_message * 0x8000000);                                   
            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[1];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[2];
                general_calculation = (double)unsigned_byte_construct.data_int;
                new_parameters.low_voltage_reconnect = general_calculation * 0.001;
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[3];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[4];
                general_calculation = (double)unsigned_byte_construct.data_int;
                new_parameters.high_voltage_reconnect = general_calculation * 0.001;
                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[5];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[6];
                general_calculation = (double)signed_byte_construct.data_int;
                new_parameters.low_temperature_reconnect = general_calculation * 0.005;
                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[7];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[8];
                general_calculation = (double)signed_byte_construct.data_int;
                new_parameters.high_temperature_reconnect = general_calculation * 0.005;

                full_data_set.bits.b2 = 1;      //4
                new_parameters.complete_parameter_set = full_data_set.short_int;                                                                      
            }            

            message_number = 3;
            message_address = (BPG_device_class * 0x10000) + (message_number * 0x800000) + (PGN_new_parameter_message * 0x8000000);                                   
            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {
                signed_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[1];
                signed_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[2];
                general_calculation = (double)signed_byte_construct.data_int;
                new_parameters.high_current_disconnect = general_calculation * 0.025;
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[3];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[4];
                general_calculation = (double)unsigned_byte_construct.data_int;
                new_parameters.BMS_IO_trigger_voltage = general_calculation * 0.001;
                new_parameters.voltage_time_hysteresis = PGN_data_extract.CAN_byte[5];
                new_parameters.temperature_time_hysteresis = PGN_data_extract.CAN_byte[6];
                new_parameters.current_time_hysteresis = PGN_data_extract.CAN_byte[7];
                new_parameters.BMS_IO_disconnect_delay = PGN_data_extract.CAN_byte[8];               
                
                full_data_set.bits.b3 = 1;      //8
                new_parameters.complete_parameter_set = full_data_set.short_int;                                                                      
            }            

            message_number = 4;
            message_address = (BPG_device_class * 0x10000) + (message_number * 0x800000) + (PGN_new_parameter_message * 0x8000000);                                   
            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {
                new_parameters.scan_only_paired_bank_current = PGN_data_extract.CAN_byte[1];
                new_parameters.bank_class_currents_to_scan = PGN_data_extract.CAN_byte[2];
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[3];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[4];
                new_parameters.voltage_measurement_wake_interval = unsigned_byte_construct.data_int;
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[5];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[6];
                new_parameters.no_Argo_activity_sleep_interval = unsigned_byte_construct.data_int;
                new_parameters.fan_activation_temperature = PGN_data_extract.CAN_byte[7];
                new_parameters.fan_deactivation_temperature = PGN_data_extract.CAN_byte[8];
                full_data_set.bits.b4 = 1;      //16
                new_parameters.complete_parameter_set = full_data_set.short_int;                                                                      
            }   
            
            message_number = 5;
            message_address = (BPG_device_class * 0x10000) + (message_number * 0x800000) + (PGN_new_parameter_message * 0x8000000);                                   
            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {
                new_parameters.enable_OCV_SoC_reset = PGN_data_extract.CAN_byte[1];
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[2];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[3];                
                new_parameters.time_before_OCV_SoC_reset = unsigned_byte_construct.data_int;
                unsigned_byte_construct.data_bytes[0] = PGN_data_extract.CAN_byte[4];
                unsigned_byte_construct.data_bytes[1] = PGN_data_extract.CAN_byte[5];
                general_calculation = (double)unsigned_byte_construct.data_int;
                new_parameters.OCV_SoC_reset_current_limit = general_calculation * 0.000153;
                new_parameters.name_index = PGN_data_extract.CAN_byte[6];
                new_parameters.charge_override_time_hysteresis = PGN_data_extract.CAN_byte[7];
                
                full_data_set.bits.b5 = 1;      //32
                new_parameters.complete_parameter_set = full_data_set.short_int;                                                                      
            }          
               
            message_number = 6;
            message_address = (BPG_device_class * 0x10000) + (message_number * 0x800000) + (PGN_new_parameter_message * 0x8000000);                                   
            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {            
                if (PGN_data_extract.CAN_byte[1])
                {
                    if (switch_position == 1) operate_switch(0, GLOBAL_NO_ACTION);
                }
                if (PGN_data_extract.CAN_byte[2]) 
                {
                    factory_reset();
                    n_second_delay(60);
                    new_parameters.complete_parameter_set = 0;
                }
                sleep_mode = PGN_data_extract.CAN_byte[3];
                if (PGN_data_extract.CAN_byte[4]) download_log();
                if (PGN_data_extract.CAN_byte[5])
                {
                    if (switch_position == 0) operate_switch(1, GLOBAL_NO_ACTION);
                }     
                if (PGN_data_extract.CAN_byte[6]) 
                {
                    FRAM_update_active(4,1);
                    start_boot(boot_start);
                }
                if (PGN_data_extract.CAN_byte[7])                 
                {
                    if (BPG_data[0].interface_type_to_use == INTERFACE_CAN) BMS_CAN_enumerate();                    
                    if (BPG_data[0].interface_type_to_use == INTERFACE_RS485) BMS_RS485_enumerate();                    
                }
                
            }
}



void operate_switch(unsigned char position, unsigned char global_operation)
{

    if (switch_stuck != SWITCH_OK)
    {
        if (switch_stuck == SWITCH_STUCK_OFF) alarm_condition = ALARM_SWITCH_STUCK_OFF;
        if (switch_stuck == SWITCH_STUCK_ON) alarm_condition = ALARM_SWITCH_STUCK_ON;     
        if (switch_stuck == SWITCH_POWER_OFF) alarm_condition = ALARM_BATTERY_MISSING;
        if (switch_retry < stuck_switch_retry_attempts) 
        {
            switch_retry++;
        } else {
            if ((BPG_data[0].bank_switch_voltage > battery_missing_voltage) && (switch_stuck == SWITCH_POWER_OFF)) 
            {
                switch_stuck = SWITCH_OK;
                if (alarm_condition == ALARM_BATTERY_MISSING) alarm_condition = ALARM_NONE;
            } else {
                return;
            }
        }        
    }
    
    if (position)
    {
        if (use_pre_charge)
        {
            pre_charge = 1;
            n_second_delay(5);                
            pre_charge = 0;
            delay_ms(100);            
        }
        pre_charge = 0;
        diode_control = 1;
        delay_ms(200);
        relay1_control = relay_on;
        delay_ms(200);
        relay1_control = relay_off;
        delay_ms(200);
        diode_control = 0;        
        
        FRAM_last_switch_position(1,1);
        LED_switch_position = 1;
        if (switch_position == 0)
        {
            switch_stuck = SWITCH_STUCK_OFF;
            if (BPG_data[0].bank_switch_voltage < battery_missing_voltage) switch_stuck = SWITCH_POWER_OFF;
        } else {
            switch_stuck = SWITCH_OK;
            if ((alarm_condition == ALARM_SWITCH_STUCK_OFF) || (alarm_condition == ALARM_SWITCH_STUCK_ON) || (alarm_condition == ALARM_BATTERY_MISSING)) alarm_condition = ALARM_NONE;
        }
    } else {
        pre_charge = 0;
        diode_control = 0;
        delay_ms(200);        
        relay2_control = relay_on;                 //Was on and is now off              
        delay_ms(200);
        relay2_control = relay_off;
        FRAM_last_switch_position(0,1);
        if ((global_operation == GLOBAL_OFF) && (link_switches_enabled)) send_panic_off();        
        LED_switch_position = 0;
        if (switch_position)
        {
            switch_stuck = SWITCH_STUCK_ON;
            if (BPG_data[0].bank_switch_voltage < battery_missing_voltage) switch_stuck = SWITCH_POWER_OFF;
        } else {
            switch_stuck = SWITCH_OK;
            if ((alarm_condition == ALARM_SWITCH_STUCK_OFF) || (alarm_condition == ALARM_SWITCH_STUCK_ON) || (alarm_condition == ALARM_BATTERY_MISSING)) alarm_condition = ALARM_NONE;
        }
    }
}


double scan_bank_current(unsigned char current_search)
{
    double high_current_found, low_current_found, read_current;
    unsigned char bank_sensor_search;
        
        high_current_found = 0;
        low_current_found = 1000;
        for (bank_sensor_search = 0; bank_sensor_search < bank_sensor_max; bank_sensor_search++)
        {
            if ((bank_data[bank_sensor_search].ID_number != 0) && (bank_data[bank_sensor_search].CAN_device_class == FRAM_bank_class_to_scan(0,0)) && (bank_data[bank_sensor_search].complete_data_set == bank_data_message_complete))
            {
                if (bank_data[bank_sensor_search].cyclical_counter == bank_data[bank_sensor_search].cyclical_counter_compare)
                {
                    bank_data[bank_sensor_search].sensor_time_out++;
                } else {
                    bank_data[bank_sensor_search].cyclical_counter_compare = bank_data[bank_sensor_search].cyclical_counter;
                    bank_data[bank_sensor_search].sensor_time_out = 0;
                }

                if (bank_data[bank_sensor_search].sensor_time_out > module_disconnect_timeout)
                {                   
                    bank_data[bank_sensor_search].ID_number = 0;   
                    bank_data[bank_sensor_search].complete_data_set = 0;   
                    bank_data[bank_sensor_search].sensor_time_out = 0;   
                } else {
                    read_current = fabs(bank_data[bank_sensor_search].DC_current);
                    if (FRAM_scan_only_paired_bank_current(0,0))
                    {                        
                        if ((read_current > high_current_found) && (bank_data[bank_sensor_search].pair_index == FRAM_pair_index(0,0))) high_current_found = read_current;
                        if ((read_current < low_current_found) && (bank_data[bank_sensor_search].pair_index == FRAM_pair_index(0,0))) low_current_found = read_current;
                    } else {
                        if (read_current > high_current_found) high_current_found = read_current;
                        if (read_current < low_current_found) low_current_found = read_current;
                    }
                }
            }
        }                  
    
        if (current_search == CURRENT_MAX) return high_current_found;
        if (current_search == CURRENT_MIN) return low_current_found;
        return 0;
    }


void send_BPG_data(void)
{
	unsigned char message_number;
    double general_maths;
    union {
        signed short int data_int;
        unsigned char data_bytes[2];
    } byte_split;
    union {
        unsigned short int data_int;
        unsigned char data_bytes[2];
    } byte_split2;
       
        message_number = 0;
        CAN_send_packet.CAN_PGN = unique_16bit_ID + (BPG_device_class * 0x10000) + (message_number * 0x800000) + (PGN_data_message * 0x8000000);
        
        general_maths = BPG_data[0].bank_switch_voltage * 100;
        byte_split.data_int = (signed short int)general_maths;
        CAN_send_packet.CAN_byte[0] = byte_split.data_bytes[0];
        CAN_send_packet.CAN_byte[1] = byte_split.data_bytes[1];

        general_maths = BPG_data[0].maximum_bank_current * 40;
        byte_split.data_int = (signed short int)general_maths;
        CAN_send_packet.CAN_byte[2] = byte_split.data_bytes[0];
        CAN_send_packet.CAN_byte[3] = byte_split.data_bytes[1];      
        
        CAN_send_packet.CAN_byte[4] = BPG_data[0].current_switch_position;
        CAN_send_packet.CAN_byte[5] = BPG_data[0].last_disconnect_reason;               

        general_maths = BPG_data[0].ambient_temperature * 200;
        byte_split.data_int = (signed short int)general_maths;
        CAN_send_packet.CAN_byte[6] = byte_split.data_bytes[0];
        CAN_send_packet.CAN_byte[7] = byte_split.data_bytes[1];

        CAN_send(CAN_send_packet, Argo_CAN);
        
        message_number = 1;
        CAN_send_packet.CAN_PGN = unique_16bit_ID + (BPG_device_class * 0x10000) + (message_number * 0x800000) + (PGN_data_message * 0x8000000);            
        
        CAN_send_packet.CAN_byte[0] = BPG_data[0].cyclical_counter;
        BPG_data[0].cyclical_counter++;
        
        CAN_send_packet.CAN_byte[1] = BPG_data[0].name_index;               
        CAN_send_packet.CAN_byte[2] = BPG_data[0].pair_index;               
        CAN_send_packet.CAN_byte[3] = BPG_data[0].alarm;               

        general_maths = BPG_data[0].IO_signal1_voltage_no_discharge * 1000;
        byte_split2.data_int = (unsigned short int)general_maths;
        CAN_send_packet.CAN_byte[4] = byte_split2.data_bytes[0];
        CAN_send_packet.CAN_byte[5] = byte_split2.data_bytes[1];

        general_maths = BPG_data[0].IO_signal2_voltage_no_charge * 1000;
        byte_split2.data_int = (unsigned short int)general_maths;
        CAN_send_packet.CAN_byte[6] = byte_split2.data_bytes[0];
        CAN_send_packet.CAN_byte[7] = byte_split2.data_bytes[1];
        
        CAN_send(CAN_send_packet, Argo_CAN);
        
        message_number = 2;
        CAN_send_packet.CAN_PGN = unique_16bit_ID + (BPG_device_class * 0x10000) + (message_number * 0x800000) + (PGN_data_message * 0x8000000);            
               
        CAN_send_packet.CAN_byte[0] = BPG_data[0].RS232_data_present;               
        CAN_send_packet.CAN_byte[1] = BPG_data[0].RS485_data_present;               
        CAN_send_packet.CAN_byte[2] = BPG_data[0].battery_CAN_present;               

        byte_split2.data_int = firmware_revision;
        CAN_send_packet.CAN_byte[3] = byte_split2.data_bytes[0];
        CAN_send_packet.CAN_byte[4] = byte_split2.data_bytes[1];

        general_maths = BPG_data[0].battery_thermistor_1 * 200;
        byte_split.data_int = (signed short int)general_maths;
        CAN_send_packet.CAN_byte[5] = byte_split.data_bytes[0];
        CAN_send_packet.CAN_byte[6] = byte_split.data_bytes[1];
        
        CAN_send_packet.CAN_byte[7] = BPG_data[0].fan_state; 
        
        CAN_send(CAN_send_packet, Argo_CAN);

        message_number = 3;
        CAN_send_packet.CAN_PGN = unique_16bit_ID + (BPG_device_class * 0x10000) + (message_number * 0x800000) + (PGN_data_message * 0x8000000);            
               
        general_maths = BPG_data[0].battery_thermistor_2 * 200;
        byte_split.data_int = (signed short int)general_maths;
        CAN_send_packet.CAN_byte[0] = byte_split.data_bytes[0];
        CAN_send_packet.CAN_byte[1] = byte_split.data_bytes[1];
        
        CAN_send_packet.CAN_byte[2] = BPG_data[0].updated_parameter_status; 
        CAN_send_packet.CAN_byte[3] = BPG_data[0].BMS_block_count;
        CAN_send_packet.CAN_byte[4] = BPG_data[0].BMS_live_block_count;
        CAN_send_packet.CAN_byte[5] = 0;
        CAN_send_packet.CAN_byte[6] = 0;
        CAN_send_packet.CAN_byte[7] = 0;
        
        CAN_send(CAN_send_packet, Argo_CAN);
        //message 4is logging download
        
        
}

void send_BPG_parameters(void)
{
 	unsigned char message_number;
    double general_maths;
    union {
        signed short int data_int;
        unsigned char data_bytes[2];
    } signed_byte_split;
    union {
        unsigned short int data_int;
        unsigned char data_bytes[2];
    } unsigned_byte_split;
       
        message_number = 0;
        CAN_send_packet.CAN_PGN = unique_16bit_ID + (BPG_device_class * 0x10000) + (message_number * 0x800000) + (PGN_current_parameter_message * 0x8000000);

        unsigned_byte_split.data_int = BPG_data[0].battery_manufacturer;
        CAN_send_packet.CAN_byte[0] = unsigned_byte_split.data_bytes[0];
        CAN_send_packet.CAN_byte[1] = unsigned_byte_split.data_bytes[1];
        unsigned_byte_split.data_int = BPG_data[0].battery_model;
        CAN_send_packet.CAN_byte[2] = unsigned_byte_split.data_bytes[0];
        CAN_send_packet.CAN_byte[3] = unsigned_byte_split.data_bytes[1];
        
        CAN_send_packet.CAN_byte[4] = BPG_data[0].interface_type_to_use;
        CAN_send_packet.CAN_byte[5] = BPG_data[0].use_IO_interface;
        CAN_send_packet.CAN_byte[6] = BPG_data[0].allow_remote_switching;
        CAN_send_packet.CAN_byte[7] = BPG_data[0].pair_index;

        CAN_send(CAN_send_packet, Argo_CAN);   

        message_number = 1;
        CAN_send_packet.CAN_PGN = unique_16bit_ID + (BPG_device_class * 0x10000) + (message_number * 0x800000) + (PGN_current_parameter_message * 0x8000000);

        general_maths = BPG_data[0].low_voltage_disconnect * 1000;
        unsigned_byte_split.data_int = (unsigned short int)general_maths;
        CAN_send_packet.CAN_byte[0] = unsigned_byte_split.data_bytes[0];
        CAN_send_packet.CAN_byte[1] = unsigned_byte_split.data_bytes[1];
        general_maths = BPG_data[0].high_voltage_disconnect * 1000;
        unsigned_byte_split.data_int = (unsigned short int)general_maths;
        CAN_send_packet.CAN_byte[2] = unsigned_byte_split.data_bytes[0];
        CAN_send_packet.CAN_byte[3] = unsigned_byte_split.data_bytes[1];
        general_maths = BPG_data[0].low_temperature_disconnect * 200;
        signed_byte_split.data_int = (signed short int)general_maths;
        CAN_send_packet.CAN_byte[4] = signed_byte_split.data_bytes[0];
        CAN_send_packet.CAN_byte[5] = signed_byte_split.data_bytes[1];
        general_maths = BPG_data[0].high_temperature_disconnect * 200;
        signed_byte_split.data_int = (signed short int)general_maths;
        CAN_send_packet.CAN_byte[6] = signed_byte_split.data_bytes[0];
        CAN_send_packet.CAN_byte[7] = signed_byte_split.data_bytes[1];               
        
        CAN_send(CAN_send_packet, Argo_CAN);   

        message_number = 2;
        CAN_send_packet.CAN_PGN = unique_16bit_ID + (BPG_device_class * 0x10000) + (message_number * 0x800000) + (PGN_current_parameter_message * 0x8000000);

        general_maths = BPG_data[0].low_voltage_reconnect * 1000;
        unsigned_byte_split.data_int = (unsigned short int)general_maths;
        CAN_send_packet.CAN_byte[0] = unsigned_byte_split.data_bytes[0];
        CAN_send_packet.CAN_byte[1] = unsigned_byte_split.data_bytes[1];
        general_maths = BPG_data[0].high_voltage_reconnect * 1000;
        unsigned_byte_split.data_int = (unsigned short int)general_maths;
        CAN_send_packet.CAN_byte[2] = unsigned_byte_split.data_bytes[0];
        CAN_send_packet.CAN_byte[3] = unsigned_byte_split.data_bytes[1];
        general_maths = BPG_data[0].low_temperature_reconnect * 200;
        signed_byte_split.data_int = (signed short int)general_maths;
        CAN_send_packet.CAN_byte[4] = signed_byte_split.data_bytes[0];
        CAN_send_packet.CAN_byte[5] = signed_byte_split.data_bytes[1];
        general_maths = BPG_data[0].high_temperature_reconnect * 200;
        signed_byte_split.data_int = (signed short int)general_maths;
        CAN_send_packet.CAN_byte[6] = signed_byte_split.data_bytes[0];
        CAN_send_packet.CAN_byte[7] = signed_byte_split.data_bytes[1];               
        
        CAN_send(CAN_send_packet, Argo_CAN);   

        message_number = 3;
        CAN_send_packet.CAN_PGN = unique_16bit_ID + (BPG_device_class * 0x10000) + (message_number * 0x800000) + (PGN_current_parameter_message * 0x8000000);

        general_maths = BPG_data[0].high_current_disconnect * 40;
        signed_byte_split.data_int = (signed short int)general_maths;
        CAN_send_packet.CAN_byte[0] = signed_byte_split.data_bytes[0];
        CAN_send_packet.CAN_byte[1] = signed_byte_split.data_bytes[1];
        general_maths = BPG_data[0].BMS_IO_trigger_voltage * 1000;
        unsigned_byte_split.data_int = (unsigned short int)general_maths;
        CAN_send_packet.CAN_byte[2] = unsigned_byte_split.data_bytes[0];
        CAN_send_packet.CAN_byte[3] = unsigned_byte_split.data_bytes[1];
        
        CAN_send_packet.CAN_byte[4] = BPG_data[0].voltage_time_hysteresis;               
        CAN_send_packet.CAN_byte[5] = BPG_data[0].temperature_time_hysteresis;               
        CAN_send_packet.CAN_byte[6] = BPG_data[0].current_time_hysteresis;               
        CAN_send_packet.CAN_byte[7] = BPG_data[0].BMS_IO_disconnect_delay;               
        
        CAN_send(CAN_send_packet, Argo_CAN);   

        message_number = 4;
        CAN_send_packet.CAN_PGN = unique_16bit_ID + (BPG_device_class * 0x10000) + (message_number * 0x800000) + (PGN_current_parameter_message * 0x8000000);
      
        CAN_send_packet.CAN_byte[0] = BPG_data[0].scan_only_paired_bank_current;               
        CAN_send_packet.CAN_byte[1] = BPG_data[0].bank_class_currents_to_scan;               

        unsigned_byte_split.data_int = BPG_data[0].voltage_measurement_wake_interval;
        CAN_send_packet.CAN_byte[2] = unsigned_byte_split.data_bytes[0];
        CAN_send_packet.CAN_byte[3] = unsigned_byte_split.data_bytes[1];
        unsigned_byte_split.data_int = BPG_data[0].no_Argo_activity_sleep_interval;
        CAN_send_packet.CAN_byte[4] = unsigned_byte_split.data_bytes[0];
        CAN_send_packet.CAN_byte[5] = unsigned_byte_split.data_bytes[1];
        
        CAN_send_packet.CAN_byte[6] = BPG_data[0].fan_activation_temperature;               
        CAN_send_packet.CAN_byte[7] = BPG_data[0].fan_deactivation_temperature;                              
        
        CAN_send(CAN_send_packet, Argo_CAN);           

        message_number = 5;
        CAN_send_packet.CAN_PGN = unique_16bit_ID + (BPG_device_class * 0x10000) + (message_number * 0x800000) + (PGN_current_parameter_message * 0x8000000);
      
        CAN_send_packet.CAN_byte[0] = BPG_data[0].enable_OCV_SoC_reset;                       

        unsigned_byte_split.data_int = BPG_data[0].time_before_OCV_SoC_reset;
        CAN_send_packet.CAN_byte[1] = unsigned_byte_split.data_bytes[0];
        CAN_send_packet.CAN_byte[2] = unsigned_byte_split.data_bytes[1];
        
        general_maths = BPG_data[0].OCV_SoC_reset_current_limit * 6536;
        unsigned_byte_split.data_int = (unsigned short int)general_maths;
        CAN_send_packet.CAN_byte[3] = unsigned_byte_split.data_bytes[0];
        CAN_send_packet.CAN_byte[4] = unsigned_byte_split.data_bytes[1];
        
        CAN_send_packet.CAN_byte[5] = BPG_data[0].name_index;               
        CAN_send_packet.CAN_byte[6] = BPG_data[0].charge_override_time_hysteresis;               
        CAN_send_packet.CAN_byte[7] = 0;               
        
        CAN_send(CAN_send_packet, Argo_CAN);   
}


void load_FRAM_into_globals(void)
{
        BPG_data[0].battery_manufacturer = FRAM_battery_manufacturer(0,0);
        BPG_data[0].battery_model = FRAM_battery_model(0,0);
        BPG_data[0].interface_type_to_use = FRAM_interface_type(0,0);
        BPG_data[0].use_IO_interface = FRAM_use_interface_io(0,0);
        BPG_data[0].allow_remote_switching = FRAM_allow_remote_switch(0,0);
        
        BPG_data[0].low_voltage_disconnect = FRAM_low_voltage_disconnect(0,0);
        BPG_data[0].high_voltage_disconnect = FRAM_high_voltage_disconnect(0,0);
        BPG_data[0].voltage_time_hysteresis = FRAM_voltage_time_hysteresis(0,0);
        BPG_data[0].scan_only_paired_bank_current = FRAM_scan_only_paired_bank_current(0,0);
        BPG_data[0].low_voltage_reconnect = FRAM_low_voltage_reconnect(0,0);
        BPG_data[0].high_voltage_reconnect = FRAM_high_voltage_reconnect(0,0);
        BPG_data[0].voltage_measurement_wake_interval = FRAM_voltage_measurement_sleep_peek(0,0);
        BPG_data[0].no_Argo_activity_sleep_interval = FRAM_no_Argo_activity_sleep(0,0);
        BPG_data[0].pair_index = FRAM_pair_index(0,0);
        BPG_data[0].name_index = FRAM_name_index(0,0);
        BPG_data[0].high_temperature_disconnect = FRAM_high_temperature_disconnect(0,0);
        BPG_data[0].low_temperature_disconnect = FRAM_low_temperature_disconnect(0,0);
        BPG_data[0].temperature_time_hysteresis = FRAM_temperature_time_hysteresis(0,0);
        BPG_data[0].bank_class_currents_to_scan = FRAM_bank_class_to_scan(0,0);
        BPG_data[0].low_temperature_reconnect = FRAM_low_temperature_reconnect(0,0);
        BPG_data[0].high_temperature_reconnect = FRAM_high_temperature_reconnect(0,0);
        BPG_data[0].high_current_disconnect = FRAM_high_current_disconnect(0,0);
        BPG_data[0].current_time_hysteresis = FRAM_current_time_hysteresis(0,0);
        BPG_data[0].BMS_IO_disconnect_delay = FRAM_io_activation_delay(0,0);
        BPG_data[0].BMS_IO_trigger_voltage = FRAM_io_trigger_voltage(0,0);
        BPG_data[0].enable_OCV_SoC_reset = FRAM_OCV_SoC_reset_enable(0,0);
        BPG_data[0].time_before_OCV_SoC_reset = FRAM_time_before_OCV_SoC_reset(0,0);
        BPG_data[0].OCV_SoC_reset_current_limit = FRAM_OCV_SoC_reset_current_limit(0,0);                
        BPG_data[0].charge_override_time_hysteresis = FRAM_charge_override_time_hysteresis(0,0);
        BPG_data[0].fan_activation_temperature = FRAM_fan_activation_temperature(0,0);                
        BPG_data[0].fan_deactivation_temperature = FRAM_fan_deactivation_temperature(0,0);   
        
        BPG_data[0].BMS_block_count = FRAM_block_count(0,0);
        BPG_data[0].ID_number = unique_16bit_ID;
        BPG_data[0].CAN_device_class = BPG_device_class;       
        
        power_options();

}

void process_panic(CAN_data_packet PGN_data_extract)
{
    unsigned long message_address;
    unsigned char message_number;
    unsigned long PGN_number;

    
        PGN_number = PGN_data_extract.CAN_PGN;

       if (PGN_data_extract.CAN_PGN != 0)
        {                                  
            message_number = 0;
            message_address = (panic_device_class * 0x10000) + (message_number * 0x800000) + (PGN_new_parameter_message * 0x8000000); 
            
            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {
                panic_commands.stop_generator = PGN_data_extract.CAN_byte[1];
                panic_commands.start_generator = PGN_data_extract.CAN_byte[2];
                panic_commands.disconnect_battery_bank = PGN_data_extract.CAN_byte[3];
                panic_commands.reconnect_battery_bank = PGN_data_extract.CAN_byte[4];
                panic_commands.system_sleep = PGN_data_extract.CAN_byte[5];
                panic_commands.pause_CAN_bus_activity = PGN_data_extract.CAN_byte[6];
                panic_commands.resume_CAN_bus_activity = PGN_data_extract.CAN_byte[7];
                panic_commands.reconnect_battery_bank_alarm_override = PGN_data_extract.CAN_byte[8];
                sleep_mode = panic_commands.system_sleep;
            }
       }
}
unsigned short int check_for_generation(void)
{
    unsigned short int generator_RPM;
    unsigned char ID_loop;
    
        generator_RPM = 0;
         for (ID_loop = 0; ID_loop < fcoil_max; ID_loop++)
        {
            if ((fcoil_data[ID_loop].generator_RPM > 100) && (fcoil_data[ID_loop].generator_RPM > generator_RPM)) generator_RPM = fcoil_data[ID_loop].generator_RPM;
        }
    return generator_RPM;
}

void collate_alarms(void)
{
    unsigned char ID_loop, message_number;
    unsigned char fcoil_summary, bank_48V_summary, bank_24V_summary, bank_12V_summary, BPG_summary, LCD_summary, battery_charger_24V_summary, battery_charger_12V_summary, BMS_manufacturer_summary;
    unsigned short int BMS_error_summary, BMS_warning_summary;
     union {
        unsigned short int data_int;
        unsigned char data_bytes[2];
    } unsigned_byte_split;
   
        fcoil_summary = 0;
        bank_48V_summary = 0;
        bank_24V_summary = 0;
        bank_12V_summary = 0;
        BPG_summary = 0;
        LCD_summary = 0;
        battery_charger_24V_summary = 0;
        battery_charger_12V_summary = 0;
        BMS_manufacturer_summary = 0;
        BMS_error_summary = 0;
        BMS_warning_summary = 0;
        
        
        
        message_number = 2;
    
    
        for (ID_loop = 0; ID_loop < fcoil_max; ID_loop++)
        {
            if ((fcoil_data[ID_loop].alarm) && (fcoil_data[ID_loop].ID_number != 0))
            {
                fcoil_summary = fcoil_data[ID_loop].alarm;
                CAN_send_packet.CAN_PGN = unique_16bit_ID + (panic_device_class * 0x10000) + (message_number * 0x800000) + (PGN_data_message * 0x8000000);
                CAN_send_packet.CAN_byte[0] = fcoil_data[ID_loop].CAN_device_class;                
                unsigned_byte_split.data_int = fcoil_data[ID_loop].ID_number;
                CAN_send_packet.CAN_byte[1] = unsigned_byte_split.data_bytes[0];
                CAN_send_packet.CAN_byte[2] = unsigned_byte_split.data_bytes[1];
                CAN_send_packet.CAN_byte[3] = fcoil_data[ID_loop].alarm;
                CAN_send_packet.CAN_byte[4] = fcoil_data[ID_loop].field_name_index;
                CAN_send_packet.CAN_byte[5] = fcoil_data[ID_loop].pair_index;
                CAN_send_packet.CAN_byte[6] = 0;
                CAN_send_packet.CAN_byte[7] = 0;               
                CAN_send(CAN_send_packet, Argo_CAN);   
                message_number++;
            }
        }
        for (ID_loop = 0; ID_loop < bank_sensor_max; ID_loop++)
        {
            if ((bank_data[ID_loop].alarm) && (bank_data[ID_loop].ID_number != 0))
            {
                if (bank_data[ID_loop].CAN_device_class == bank_48V_device_class) bank_48V_summary = bank_data[ID_loop].alarm;
                if (bank_data[ID_loop].CAN_device_class == bank_24V_device_class) bank_24V_summary = bank_data[ID_loop].alarm;
                if (bank_data[ID_loop].CAN_device_class == bank_12V_device_class) bank_12V_summary = bank_data[ID_loop].alarm;
                
                CAN_send_packet.CAN_PGN = unique_16bit_ID + (panic_device_class * 0x10000) + (message_number * 0x800000) + (PGN_data_message * 0x8000000);
                CAN_send_packet.CAN_byte[0] = bank_data[ID_loop].CAN_device_class;                
                unsigned_byte_split.data_int = bank_data[ID_loop].ID_number;
                CAN_send_packet.CAN_byte[1] = unsigned_byte_split.data_bytes[0];
                CAN_send_packet.CAN_byte[2] = unsigned_byte_split.data_bytes[1];
                CAN_send_packet.CAN_byte[3] = bank_data[ID_loop].alarm;
                CAN_send_packet.CAN_byte[4] = bank_data[ID_loop].sensor_name_index;
                CAN_send_packet.CAN_byte[5] = bank_data[ID_loop].pair_index;
                CAN_send_packet.CAN_byte[6] = 0;
                CAN_send_packet.CAN_byte[7] = 0;               
                CAN_send(CAN_send_packet, Argo_CAN);   
                message_number++;
            }
        }

        for (ID_loop = 0; ID_loop < BPG_max; ID_loop++)
        {
            if ((BPG_data[ID_loop].alarm) && (BPG_data[ID_loop].ID_number != 0))
            {
                BPG_summary = BPG_data[ID_loop].alarm;
                CAN_send_packet.CAN_PGN = unique_16bit_ID + (panic_device_class * 0x10000) + (message_number * 0x800000) + (PGN_data_message * 0x8000000);
                CAN_send_packet.CAN_byte[0] = BPG_data[ID_loop].CAN_device_class;                
                unsigned_byte_split.data_int = BPG_data[ID_loop].ID_number;
                CAN_send_packet.CAN_byte[1] = unsigned_byte_split.data_bytes[0];
                CAN_send_packet.CAN_byte[2] = unsigned_byte_split.data_bytes[1];
                CAN_send_packet.CAN_byte[3] = BPG_data[ID_loop].alarm;
                CAN_send_packet.CAN_byte[4] = BPG_data[ID_loop].name_index;
                CAN_send_packet.CAN_byte[5] = BPG_data[ID_loop].pair_index;
                CAN_send_packet.CAN_byte[6] = 0;
                CAN_send_packet.CAN_byte[7] = 0;               
                CAN_send(CAN_send_packet, Argo_CAN);   
                message_number++;
            }
        }
        
        if (panic_commands.system_sleep == 0) 
        {
            message_number = 0;
            CAN_send_packet.CAN_PGN = unique_16bit_ID + (panic_device_class * 0x10000) + (message_number * 0x800000) + (PGN_data_message * 0x8000000);
            CAN_send_packet.CAN_byte[0] = battery_charger_12V_summary;
            CAN_send_packet.CAN_byte[1] = battery_charger_24V_summary;
            CAN_send_packet.CAN_byte[2] = fcoil_summary;
            CAN_send_packet.CAN_byte[3] = bank_48V_summary;
            CAN_send_packet.CAN_byte[4] = bank_24V_summary;
            CAN_send_packet.CAN_byte[5] = bank_12V_summary;
            CAN_send_packet.CAN_byte[6] = LCD_summary;
            CAN_send_packet.CAN_byte[7] = BPG_summary;               
            CAN_send(CAN_send_packet, Argo_CAN);  
            
            message_number = 1;
            CAN_send_packet.CAN_PGN = unique_16bit_ID + (panic_device_class * 0x10000) + (message_number * 0x800000) + (PGN_data_message * 0x8000000);
            CAN_send_packet.CAN_byte[0] = BMS_manufacturer_summary;
            unsigned_byte_split.data_int = BMS_warning_summary;
            CAN_send_packet.CAN_byte[1] = unsigned_byte_split.data_bytes[0];
            CAN_send_packet.CAN_byte[2] = unsigned_byte_split.data_bytes[1];
            unsigned_byte_split.data_int = BMS_error_summary;
            CAN_send_packet.CAN_byte[3] = unsigned_byte_split.data_bytes[0];
            CAN_send_packet.CAN_byte[4] = unsigned_byte_split.data_bytes[1];        
            CAN_send_packet.CAN_byte[5] = 0;
            CAN_send_packet.CAN_byte[6] = 0;
            CAN_send_packet.CAN_byte[7] = 0;                          
            CAN_send(CAN_send_packet, Argo_CAN);             
        }      
}


void copy_new_parameters(void)
{
        FRAM_battery_manufacturer((unsigned short int)new_parameters.battery_manufacturer,1);
        FRAM_battery_model((unsigned short int)new_parameters.battery_model,1);
        FRAM_interface_type((unsigned char)new_parameters.interface_type_to_use,1);
        FRAM_use_interface_io((unsigned char)new_parameters.use_IO_interface,1);
        FRAM_allow_remote_switch((unsigned char)new_parameters.allow_remote_switching,1);
        
        FRAM_low_voltage_disconnect((double)new_parameters.low_voltage_disconnect,1);
        FRAM_high_voltage_disconnect((double)new_parameters.high_voltage_disconnect,1);
        FRAM_voltage_time_hysteresis((unsigned char)new_parameters.voltage_time_hysteresis,1);
        FRAM_scan_only_paired_bank_current((unsigned char)new_parameters.scan_only_paired_bank_current,1);
        FRAM_low_voltage_reconnect((double)new_parameters.low_voltage_reconnect,1);
        FRAM_high_voltage_reconnect((double)new_parameters.high_voltage_reconnect,1);
        FRAM_voltage_measurement_sleep_peek((unsigned short int)new_parameters.voltage_measurement_wake_interval,1);
        FRAM_no_Argo_activity_sleep((unsigned short int)new_parameters.no_Argo_activity_sleep_interval,1);
        FRAM_pair_index((unsigned char)new_parameters.pair_index,1);
        FRAM_name_index((unsigned char)new_parameters.name_index,1);
        FRAM_high_temperature_disconnect((double)new_parameters.high_temperature_disconnect,1);
        FRAM_low_temperature_disconnect((double)new_parameters.low_temperature_disconnect,1);
        FRAM_temperature_time_hysteresis((unsigned char)new_parameters.temperature_time_hysteresis,1);
        FRAM_bank_class_to_scan((unsigned char)new_parameters.bank_class_currents_to_scan,1);
        FRAM_low_temperature_reconnect((double)new_parameters.low_temperature_reconnect,1);
        FRAM_high_temperature_reconnect((double)new_parameters.high_temperature_reconnect,1);
        FRAM_high_current_disconnect((double)new_parameters.high_current_disconnect,1);
        FRAM_current_time_hysteresis((unsigned char)new_parameters.current_time_hysteresis,1);
        FRAM_io_activation_delay((unsigned char)new_parameters.BMS_IO_disconnect_delay,1);
        FRAM_io_trigger_voltage((double)new_parameters.BMS_IO_trigger_voltage,1);
        FRAM_OCV_SoC_reset_enable((unsigned char)new_parameters.enable_OCV_SoC_reset,1);
        FRAM_time_before_OCV_SoC_reset((unsigned short int)new_parameters.time_before_OCV_SoC_reset,1);
        FRAM_OCV_SoC_reset_current_limit((double)new_parameters.OCV_SoC_reset_current_limit,1);                
        FRAM_charge_override_time_hysteresis((unsigned char)new_parameters.charge_override_time_hysteresis,1);
        FRAM_fan_activation_temperature((signed char)new_parameters.fan_activation_temperature,1);
        FRAM_fan_deactivation_temperature((signed char)new_parameters.fan_deactivation_temperature,1);
        load_FRAM_into_globals();
}

void check_factory_reset_switch(void)
{
    unsigned char reset_loop;
    
        if (factory_reset_switch == 0)
        {
            Nop();
            factory_reset();
            reset_loop = 10;
            do {                         //Flash LED to indicate full factory reset has occured
                LED_active = !LED_active;
                delay_ms(250);
                reset_loop--;
            } while (reset_loop);

        }    
}


void write_log(unsigned char log_data)
{
    unsigned short int log_pointer;
 
        log_pointer = (unsigned short int)FRAM_log_pointer(0,0);
        
        if ((log_pointer >= FRAM_log_start) && (log_pointer <= FRAM_log_end)) FRAM_write_byte(log_pointer, log_data); // Never overwrite FRAM variables
        log_pointer++;
        if (log_pointer > FRAM_log_end) log_pointer = FRAM_log_start;        
        
        FRAM_log_pointer(log_pointer,1);

}

void download_log(void)
{
    unsigned char chunk_counter;    
    unsigned char data_chunk[6];
    unsigned short int log_cycle;
    unsigned short int log_pointer, start_pointer;
    unsigned char message_number;
 
    
        log_cycle = 0;

        log_pointer = FRAM_log_pointer(0,0);
        start_pointer = log_pointer;        
        do {
            chunk_counter = 0;       
            memset(&data_chunk,255,sizeof(data_chunk));
            do {
                data_chunk[chunk_counter] = FRAM_read_byte(log_pointer);
                chunk_counter++;           
                log_pointer++;
                if (log_pointer > FRAM_log_end) log_pointer = FRAM_log_start;

            } while ((chunk_counter < 8) && (log_pointer != start_pointer));
            
            message_number = 6;
            CAN_send_packet.CAN_PGN = unique_16bit_ID + (BPG_device_class * 0x10000) + (message_number * 0x800000);    
            
            for (chunk_counter = 0; chunk_counter < 8; chunk_counter++)
            {
                CAN_send_packet.CAN_byte[chunk_counter] = data_chunk[chunk_counter];
            }
            CAN_send(CAN_send_packet,Argo_CAN);       
            
            
        } while (log_pointer != start_pointer);
        
        CAN_send_packet.CAN_byte[0] = 0xAA;
        CAN_send_packet.CAN_byte[1] = 0x55;
        CAN_send_packet.CAN_byte[2] = 0xAA;
        CAN_send_packet.CAN_byte[3] = 0x55;
        CAN_send_packet.CAN_byte[4] = 0xAA;
        CAN_send_packet.CAN_byte[5] = 0x55;
        CAN_send_packet.CAN_byte[6] = 0xAA;
        CAN_send_packet.CAN_byte[7] = 0x55;
        CAN_send(CAN_send_packet, Argo_CAN);       
}

void send_charge_control(unsigned char operation)
{
    unsigned char message_number;
    
        message_number = 0;
        CAN_send_packet.CAN_PGN = unique_16bit_ID + (panic_device_class * 0x10000) + (message_number * 0x800000) + (PGN_new_parameter_message * 0x8000000);
        CAN_send_packet.CAN_byte[0] = 0;
        if (operation == CHARGE_STOP) CAN_send_packet.CAN_byte[0] = 1;
        CAN_send_packet.CAN_byte[1] = 0;
        if (operation == CHARGE_START) CAN_send_packet.CAN_byte[1] = 1;        
        CAN_send_packet.CAN_byte[2] = 0;
        CAN_send_packet.CAN_byte[3] = 0;
        CAN_send_packet.CAN_byte[4] = 0;
        CAN_send_packet.CAN_byte[5] = 0;
        CAN_send_packet.CAN_byte[6] = 0;
        CAN_send_packet.CAN_byte[7] = 0;               
        CAN_send(CAN_send_packet, Argo_CAN);    
}

void start_boot(uint16_t applicationAddress)
{
	asm("goto w0");
}

void torqeedo_CAN_process(CAN_data_packet PGN_data_extract)
{
    union {
        signed short int data_int;
        unsigned char data_bytes[2];
    } signed_byte_split;
     union {
        unsigned short int data_int;
        unsigned char data_bytes[2];
    } unsigned_byte_split;   
    
        if (PGN_data_extract.CAN_PGN == 0x19F21480)
        {
            signed_byte_split.data_bytes[0] = PGN_data_extract.CAN_byte[2];
            signed_byte_split.data_bytes[1] = PGN_data_extract.CAN_byte[3];
            TQ_BMS_data.block_cell_pack_voltage = signed_byte_split.data_int / 10;
            signed_byte_split.data_bytes[0] = PGN_data_extract.CAN_byte[4];
            signed_byte_split.data_bytes[1] = PGN_data_extract.CAN_byte[5];
            TQ_BMS_data.block_current = signed_byte_split.data_int / 10;
            signed_byte_split.data_bytes[0] = PGN_data_extract.CAN_byte[6];
            signed_byte_split.data_bytes[1] = PGN_data_extract.CAN_byte[7];
            TQ_BMS_data.block_maximum_cell_pack_temperature = signed_byte_split.data_int / 100;            
        }
        if (PGN_data_extract.CAN_PGN == 0x19F21280)
        {
            if (PGN_data_extract.CAN_byte[2] == 0x0B)
            {
                TQ_BMS_data.block_SoC = PGN_data_extract.CAN_byte[6];
            }
        }
        if (PGN_data_extract.CAN_PGN == 0x18FF4480)
        {
            TQ_BMS_data.block_status = PGN_data_extract.CAN_byte[4];            
            BPG_data[0].BMS_live_block_count = PGN_data_extract.CAN_byte[5];            
        }    
        if (PGN_data_extract.CAN_PGN == 0x18FF4180)
        {
            unsigned_byte_split.data_bytes[0] = PGN_data_extract.CAN_byte[3];
            unsigned_byte_split.data_bytes[1] = PGN_data_extract.CAN_byte[4];
            TQ_BMS_data.block_warnings = unsigned_byte_split.data_int;         
            unsigned_byte_split.data_bytes[0] = PGN_data_extract.CAN_byte[5];
            unsigned_byte_split.data_bytes[1] = PGN_data_extract.CAN_byte[6];
            BPG_data[0].BMS_error = unsigned_byte_split.data_int; 
        }
        if (PGN_data_extract.CAN_PGN == 0x18FF5180)
        {
            if (PGN_data_extract.CAN_byte[2] == 2) BPG_data[0].BMS_block_count = PGN_data_extract.CAN_byte[3];
        }
        
}

void send_BMS_CAN_command(unsigned char command)
{
    if (FRAM_battery_manufacturer(0,0) == TORQEEDO)
    {
        CAN_send_packet.CAN_PGN = 0x18FF6155;
        CAN_send_packet.CAN_byte[0] = 2;        
        CAN_send_packet.CAN_byte[1] = 0;                 
        CAN_send_packet.CAN_byte[2] = 0;   
        if (command == BMS_BANK_ON) CAN_send_packet.CAN_byte[2] = 1;   
        CAN_send_packet.CAN_byte[3] = 0;  
        CAN_send_packet.CAN_byte[4] = 0;                 
        CAN_send_packet.CAN_byte[5] = 0;   
        CAN_send_packet.CAN_byte[6] = 0;            
        CAN_send_packet.CAN_byte[7] = 0;               
        
        CAN_send(CAN_send_packet, Lithium_CAN);      
    }
}

void purge_offline_modules(void)
{
    unsigned char module_search;
        for (module_search = 0; module_search < bank_sensor_max; module_search++)
        {
            if ((bank_data[module_search].ID_number != 0) && ((bank_data[module_search].CAN_device_class == bank_48V_device_class) || (bank_data[module_search].CAN_device_class == bank_24V_device_class) || (bank_data[module_search].CAN_device_class == bank_12V_device_class)) && (bank_data[module_search].complete_data_set == bank_data_message_complete))
            {
                if (bank_data[module_search].cyclical_counter == bank_data[module_search].cyclical_counter_compare)
                {
                    bank_data[module_search].sensor_time_out++;
                } else {
                    bank_data[module_search].cyclical_counter_compare = bank_data[module_search].cyclical_counter;
                    bank_data[module_search].sensor_time_out = 0;
                }

                if (bank_data[module_search].sensor_time_out > module_disconnect_timeout)
                {                   
                    bank_data[module_search].ID_number = 0;   
                    bank_data[module_search].complete_data_set = 0;   
                    bank_data[module_search].sensor_time_out = 0;   
                }
            }
        }   
        for (module_search = 0; module_search < BPG_max; module_search++)
        {
            if ((BPG_data[module_search].ID_number != 0) && (BPG_data[module_search].CAN_device_class == BPG_device_class) && (BPG_data[module_search].complete_data_set == BPG_data_message_complete))
            {
                if (BPG_data[module_search].cyclical_counter == BPG_data[module_search].cyclical_counter_compare)
                {
                    BPG_data[module_search].sensor_time_out++;
                } else {
                    BPG_data[module_search].cyclical_counter_compare = BPG_data[module_search].cyclical_counter;
                    BPG_data[module_search].sensor_time_out = 0;
                }

                if (BPG_data[module_search].sensor_time_out > module_disconnect_timeout)
                {                   
                    BPG_data[module_search].ID_number = 0;   
                    BPG_data[module_search].complete_data_set = 0;   
                    BPG_data[module_search].sensor_time_out = 0;   
                }
            }
        }    
        for (module_search = 0; module_search < fcoil_max; module_search++)
        {
            if ((fcoil_data[module_search].ID_number != 0) && (fcoil_data[module_search].CAN_device_class == fcoil_device_class) && (fcoil_data[module_search].complete_data_set == field_coil_data_message_complete))
            {
                if (fcoil_data[module_search].cyclical_counter == fcoil_data[module_search].cyclical_counter_compare)
                {
                    fcoil_data[module_search].sensor_time_out++;
                } else {
                    fcoil_data[module_search].cyclical_counter_compare = fcoil_data[module_search].cyclical_counter;
                    fcoil_data[module_search].sensor_time_out = 0;
                }

                if (fcoil_data[module_search].sensor_time_out > module_disconnect_timeout)
                {                   
                    fcoil_data[module_search].ID_number = 0;   
                    fcoil_data[module_search].complete_data_set = 0;   
                    fcoil_data[module_search].sensor_time_out = 0;   
                }
            }
        }      
}

void send_panic_off(void)
{
    unsigned char message_number, transmit_loop;
   
        message_number = 0;      

        for (transmit_loop = 0; transmit_loop < 20; transmit_loop++)
        {
            CAN_send_packet.CAN_PGN = unique_16bit_ID + (panic_device_class * 0x10000) + (message_number * 0x800000) + (PGN_new_parameter_message * 0x8000000);  
            CAN_send_packet.CAN_byte[0] = 0;
            CAN_send_packet.CAN_byte[1] = 0;
            CAN_send_packet.CAN_byte[2] = 1;
            CAN_send_packet.CAN_byte[3] = 0;
            CAN_send_packet.CAN_byte[4] = 0;
            CAN_send_packet.CAN_byte[5] = 0;
            CAN_send_packet.CAN_byte[6] = 0;
            CAN_send_packet.CAN_byte[7] = 0;
            CAN_send(CAN_send_packet, Argo_CAN);       
        }       
}

void BMS_CAN_enumerate(void)
{
        if (BPG_data[0].battery_manufacturer == TORQEEDO)
        {
            FRAM_block_count(0,1);
            if ((alarm_condition == ALARM_BMS_ENUM_FAILED) || (alarm_condition == ALARM_BMS_BLOCK_MISSING) || (alarm_condition == ALARM_BMS_ENUM_TIMEOUT)) alarm_condition = ALARM_NONE;
            BPG_data[0].BMS_block_count = 0;

            CAN_send_packet.CAN_PGN = 0x18FF6155;
            CAN_send_packet.CAN_byte[0] = 0;
            CAN_send_packet.CAN_byte[1] = 0;
            CAN_send_packet.CAN_byte[2] = 0;
            CAN_send_packet.CAN_byte[3] = 0;
            CAN_send_packet.CAN_byte[4] = 0;
            CAN_send_packet.CAN_byte[5] = 0;
            CAN_send_packet.CAN_byte[6] = 0;
            CAN_send_packet.CAN_byte[7] = 0;
            CAN_send(CAN_send_packet, Lithium_CAN);     
            timer_setup(180);                              
            do {
                incoming_Lithium_poll();
                if (TQ_BMS_data.block_status == 6) BPG_data[0].BMS_block_count = 0;
            } while ((BPG_data[0].BMS_block_count == 0) && (timer_trigger_flag == 0));

            if (BPG_data[0].BMS_block_count != 0)
            {
                CAN_send_packet.CAN_PGN = 0x18FF6155;        
                CAN_send_packet.CAN_byte[0] = 1;
                CAN_send_packet.CAN_byte[1] = 2;
                CAN_send_packet.CAN_byte[2] = 0;
                CAN_send_packet.CAN_byte[3] = 0;
                CAN_send_packet.CAN_byte[4] = 0;
                CAN_send_packet.CAN_byte[5] = 0;
                CAN_send_packet.CAN_byte[6] = 0;
                CAN_send_packet.CAN_byte[7] = 0;
                CAN_send(CAN_send_packet, Lithium_CAN);     
            } else {
                alarm_condition = ALARM_BMS_ENUM_TIMEOUT;
            }
            if (alarm_condition == ALARM_NONE)
            {
                if ((BPG_data[0].BMS_live_block_count != 0) && (BPG_data[0].BMS_block_count != 0))
                {
                    if (BPG_data[0].BMS_live_block_count != BPG_data[0].BMS_block_count) alarm_condition = ALARM_BMS_BLOCK_MISSING;
                }
                if (BPG_data[0].BMS_block_count < 2) alarm_condition = ALARM_BMS_ENUM_FAILED;
            }
            FRAM_block_count(BPG_data[0].BMS_block_count,1);
            timer_setup(1);            
        }
}

void BMS_RS485_enumerate(void)
{
    unsigned char group_search, battery_count, command_result, sub_group_search, battery_search, final_battery_count;

    
        if (BPG_data[0].battery_manufacturer == TORQEEDO)
        {
            LED_active = 0;
            LED_power = 0;

            FRAM_block_count(0,1);
            if (alarm_condition == ALARM_BMS_ENUM_FAILED) alarm_condition = ALARM_NONE;
            battery_count = 0;
            torqeedo_data_TX[0] = 0xAC;
            torqeedo_data_TX[1] = 0x80;
            torqeedo_data_TX[2] = 0x80;
            //3 - Group search index
            //4 - Sub group search index
            torqeedo_data_TX[5] = 0xFF;
            //6 - Sub group search 0x00 = off, 0xFF = on
            //7 - battery_count
            //8 - CRC
            torqeedo_data_TX[9] = 0xAD;
            torqeedo_data_TX[10] = 0xFF;
             
            for (group_search = 0; group_search < 255; group_search++)
            {
                torqeedo_data_TX[3] = group_search + 1;
                torqeedo_data_TX[4] = 0x00;                      //Sub group search index is 0
                torqeedo_data_TX[6] = 0x00;                      //Clear the sub group search flag, looking for groups first          
                torqeedo_data_TX[7] = battery_count;
                torqeedo_data_TX[8] = CRC8(8);                
                RS485_send_string(10);
                command_result = RS485_receive_string();        //response is 5 bytes long (zero based))
                if (command_result == RS485_DATA_OK)
                {                    
                    delay_ms(25);
                    torqeedo_data_TX[6] = 0xFF;                  //Set the sub group search flag
                    if (battery_count == 0) battery_count = 1;   //Only increments once, subsequent discoveries automatically incremented
                    torqeedo_data_TX[7] = battery_count;                    
                    for (sub_group_search = 0; sub_group_search < 255; sub_group_search++)
                    {
                        torqeedo_data_TX[4] = sub_group_search + 1;
                        torqeedo_data_TX[8] = CRC8(8);                        
                        RS485_send_string(10);
                        command_result = RS485_receive_string();
                        if (command_result == RS485_DATA_OK) 
                        {
                            delay_ms(25);
                            battery_count++;                      //Battery found, move on to the next until the sub group is exhausted
                            torqeedo_data_TX[7] = battery_count;                    
                        }
                    }
                }
            }
            delay_ms(100);
            if (battery_count > 0)
            {
                final_battery_count = 0;
                torqeedo_data_TX[0] = 0xAC;
                //1 - Battery number to check 0x81 to start up to 0x8F
                torqeedo_data_TX[2] = 0x01;
                //3 - CRC
                torqeedo_data_TX[4] = 0xAD;
                torqeedo_data_TX[5] = 0xFF;    

                for (battery_search = 0; battery_search < 0x10; battery_search++)
                {
                    torqeedo_data_TX[1] = 0x81 + battery_search;
                    torqeedo_data_TX[3] = CRC8(3);
                    RS485_send_string(5);
                    command_result = RS485_receive_string(); //response is 17 bytes long (zero based))
                    if (command_result == RS485_DATA_OK)
                    {                    
                        delay_ms(25);      
                        final_battery_count++;
                    } 
                }
                if (final_battery_count != battery_count) alarm_condition = ALARM_BMS_ENUM_FAILED;
                BPG_data[0].BMS_block_count = final_battery_count;
                FRAM_block_count(BPG_data[0].BMS_block_count,1);
            } else {
                BPG_data[0].BMS_block_count = 0;
                alarm_condition = ALARM_BMS_ENUM_FAILED;            
            }
            LED_active = 0;
            LED_power = 1;

        }
}



unsigned char CRC8(unsigned char CRC_end)
{
    unsigned char CRC, data_search, current_byte, byte_calculation, bit_cycle;
     
        CRC = 0;
        for (data_search = 1; data_search < CRC_end; data_search++)
        {
            current_byte = torqeedo_data_TX[data_search];
            for (bit_cycle = 0; bit_cycle < 8; bit_cycle++)
            {
                byte_calculation = (CRC ^ current_byte) & 0x01;
                CRC >>= 1;
                if (byte_calculation) CRC ^= 0x8C;                 
                current_byte >>= 1;
            }
        }
        return CRC;
}

unsigned char BMS_query(unsigned char battery_to_ask)
{
    unsigned char battery_active, command_result;
    double general_maths;
     union {
        unsigned short int data_int;
        unsigned char data_bytes[2];
    } unsigned_byte_split;
     union {
        signed short int data_int;
        unsigned char data_bytes[2];
    } signed_byte_split;

        battery_active = 0;
        if (BPG_data[0].battery_manufacturer == TORQEEDO)
        {
            torqeedo_data_TX[0] = 0xAC;
            torqeedo_data_TX[1] = 0x80 + battery_to_ask; //1 - Battery number to check 0x81 to start up to 0x8F            
            torqeedo_data_TX[2] = TQ_STATUS_RECORD;                  //Status 0x04
            //3 - CRC
            torqeedo_data_TX[4] = 0xAD;
            torqeedo_data_TX[5] = 0xFF;    

            torqeedo_data_TX[3] = CRC8(3);
            RS485_send_string(5);
            command_result = RS485_receive_string(); //response is 17 bytes long (zero based))
            if (command_result == RS485_DATA_OK)
            {                    
                battery_active = 1;
                //extract data  
                unsigned_byte_split.data_bytes[1] = torqeedo_data_RX[3];
                unsigned_byte_split.data_bytes[0] = torqeedo_data_RX[4];
                TQ_BMS_data.block_status = unsigned_byte_split.data_int;
                unsigned_byte_split.data_bytes[1] = torqeedo_data_RX[5];
                unsigned_byte_split.data_bytes[0] = torqeedo_data_RX[6];
                TQ_BMS_data.block_warnings = unsigned_byte_split.data_int;
                
                if (TQ_BMS_data.block_warnings)
                {
                    alarm_condition = ALARM_BMS_WARNING;
                } else {
                    if (alarm_condition == ALARM_BMS_WARNING) alarm_condition = ALARM_NONE;
                }
                
                
                unsigned_byte_split.data_bytes[1] = torqeedo_data_RX[7];
                unsigned_byte_split.data_bytes[0] = torqeedo_data_RX[8];
                TQ_BMS_data.block_errors = unsigned_byte_split.data_int;                
                signed_byte_split.data_bytes[1] = torqeedo_data_RX[9];
                signed_byte_split.data_bytes[0] = torqeedo_data_RX[10];
                general_maths = signed_byte_split.data_int * 0.1;
                TQ_BMS_data.block_maximum_cell_pack_temperature = general_maths;
                unsigned_byte_split.data_bytes[1] = torqeedo_data_RX[13];
                unsigned_byte_split.data_bytes[0] = torqeedo_data_RX[14];
                general_maths = unsigned_byte_split.data_int * 0.1;
                TQ_BMS_data.block_cell_pack_voltage = general_maths;
                signed_byte_split.data_bytes[1] = torqeedo_data_RX[15];
                signed_byte_split.data_bytes[0] = torqeedo_data_RX[16];
                general_maths = signed_byte_split.data_int * 0.01;
                TQ_BMS_data.block_current = general_maths;
                unsigned_byte_split.data_bytes[1] = torqeedo_data_RX[23];
                unsigned_byte_split.data_bytes[0] = torqeedo_data_RX[24];
                TQ_BMS_data.block_SoC = unsigned_byte_split.data_int;
            }           

            torqeedo_data_TX[2] = TQ_TEMPERATURE_RECORD;                  //Temp 0x20
            //3 - CRC
            torqeedo_data_TX[3] = CRC8(3);
            RS485_send_string(5);
            command_result = RS485_receive_string(); //response is 17 bytes long (zero based))
            if (command_result == RS485_DATA_OK)
            {                    
                battery_active = 1;
                //extract data
                signed_byte_split.data_bytes[1] = torqeedo_data_RX[3];
                signed_byte_split.data_bytes[0] = torqeedo_data_RX[4];
                general_maths = signed_byte_split.data_int * 0.1;
                TQ_BMS_data.block_cell_pack_temperature1 = general_maths;
                signed_byte_split.data_bytes[1] = torqeedo_data_RX[5];
                signed_byte_split.data_bytes[0] = torqeedo_data_RX[6];
                general_maths = signed_byte_split.data_int * 0.1;
                TQ_BMS_data.block_cell_pack_temperature2 = general_maths;
            }

            torqeedo_data_TX[2] = TQ_VOLTAGE_RECORD;                  //voltage 0x21
            //3 - CRC
            torqeedo_data_TX[3] = CRC8(3);
            RS485_send_string(5);
            command_result = RS485_receive_string(); //response is 17 bytes long (zero based))
            if (command_result == RS485_DATA_OK)
            {                    
                battery_active = 1;
                //extract data
                unsigned_byte_split.data_bytes[1] = torqeedo_data_RX[3];
                unsigned_byte_split.data_bytes[0] = torqeedo_data_RX[4];
                TQ_BMS_data.block_cell_pack_voltage1 = unsigned_byte_split.data_int;                
                unsigned_byte_split.data_bytes[1] = torqeedo_data_RX[5];
                unsigned_byte_split.data_bytes[0] = torqeedo_data_RX[6];
                TQ_BMS_data.block_cell_pack_voltage2 = unsigned_byte_split.data_int;                
                unsigned_byte_split.data_bytes[1] = torqeedo_data_RX[7];
                unsigned_byte_split.data_bytes[0] = torqeedo_data_RX[8];
                TQ_BMS_data.block_cell_pack_voltage3 = unsigned_byte_split.data_int;                
                unsigned_byte_split.data_bytes[1] = torqeedo_data_RX[9];
                unsigned_byte_split.data_bytes[0] = torqeedo_data_RX[10];
                TQ_BMS_data.block_cell_pack_voltage4 = unsigned_byte_split.data_int;                
                unsigned_byte_split.data_bytes[1] = torqeedo_data_RX[11];
                unsigned_byte_split.data_bytes[0] = torqeedo_data_RX[12];
                TQ_BMS_data.block_cell_pack_voltage5 = unsigned_byte_split.data_int;                
                unsigned_byte_split.data_bytes[1] = torqeedo_data_RX[13];
                unsigned_byte_split.data_bytes[0] = torqeedo_data_RX[14];
                TQ_BMS_data.block_cell_pack_voltage6 = unsigned_byte_split.data_int;                
                unsigned_byte_split.data_bytes[1] = torqeedo_data_RX[15];
                unsigned_byte_split.data_bytes[0] = torqeedo_data_RX[16];
                TQ_BMS_data.block_cell_pack_voltage7 = unsigned_byte_split.data_int;                
            }           

            torqeedo_data_TX[2] = TQ_ERROR_COUNT_RECORD;                  //Error 0x22
            //3 - CRC
            torqeedo_data_TX[3] = CRC8(3);
            RS485_send_string(5);
            command_result = RS485_receive_string(); //response is 17 bytes long (zero based))
            if (command_result == RS485_DATA_OK)
            {                    
                battery_active = 1;
                //extract data
                unsigned_byte_split.data_bytes[1] = torqeedo_data_RX[3];
                unsigned_byte_split.data_bytes[0] = torqeedo_data_RX[4];
                TQ_BMS_data.block_discharge_current_high = unsigned_byte_split.data_int;                
                unsigned_byte_split.data_bytes[1] = torqeedo_data_RX[5];
                unsigned_byte_split.data_bytes[0] = torqeedo_data_RX[6];
                TQ_BMS_data.block_discharge_cell_temperature_high = unsigned_byte_split.data_int;                
                unsigned_byte_split.data_bytes[1] = torqeedo_data_RX[9];
                unsigned_byte_split.data_bytes[0] = torqeedo_data_RX[10];
                TQ_BMS_data.block_discharge_cell_temperature_low = unsigned_byte_split.data_int;                
                unsigned_byte_split.data_bytes[1] = torqeedo_data_RX[13];
                unsigned_byte_split.data_bytes[0] = torqeedo_data_RX[14];
                TQ_BMS_data.block_charge_current_high = unsigned_byte_split.data_int;                
                unsigned_byte_split.data_bytes[1] = torqeedo_data_RX[15];
                unsigned_byte_split.data_bytes[0] = torqeedo_data_RX[16];
                TQ_BMS_data.block_charge_cell_temperature_high = unsigned_byte_split.data_int;                
                unsigned_byte_split.data_bytes[1] = torqeedo_data_RX[17];
                unsigned_byte_split.data_bytes[0] = torqeedo_data_RX[18];
                TQ_BMS_data.block_charge_cell_temperature_low = unsigned_byte_split.data_int;                
                unsigned_byte_split.data_bytes[1] = torqeedo_data_RX[19];
                unsigned_byte_split.data_bytes[0] = torqeedo_data_RX[20];
                TQ_BMS_data.block_charge_voltage_high = unsigned_byte_split.data_int;                
                unsigned_byte_split.data_bytes[1] = torqeedo_data_RX[31];
                unsigned_byte_split.data_bytes[0] = torqeedo_data_RX[32];
                TQ_BMS_data.block_deep_discharge = unsigned_byte_split.data_int;                                              
            }            

            torqeedo_data_TX[2] = TQ_ID_GROUP_RECORD;
            //3 - CRC
            torqeedo_data_TX[3] = CRC8(3);
            RS485_send_string(5);
            command_result = RS485_receive_string(); //response is 17 bytes long (zero based))
            if (battery_to_ask > 0)
            {
                if (command_result == RS485_DATA_OK)
                {                    
                    TQ_BMS_ID_group[battery_to_ask - 1].group = torqeedo_data_RX[5];
                    TQ_BMS_ID_group[battery_to_ask - 1].ID = torqeedo_data_RX[6];    
                    TQ_BMS_ID_group[battery_to_ask - 1].present = 1;                         
                } else {
                    TQ_BMS_ID_group[battery_to_ask - 1].present = 0;                         
                }
            }
        }
        return battery_active;
}

void power_options(void)
{
        MCP2515_CAN_control = 1;
        
        RS485_control = 1;      
        if (BPG_data[0].interface_type_to_use == INTERFACE_RS485) RS485_control = 0;       
        
        if (BPG_data[0].interface_type_to_use == INTERFACE_CAN) 
        {
            MCP2515_CAN_control = 0;       
            regulator_CAN_control = 1;
        }
}

void extract_TQ_BMS_new_parameters(CAN_data_packet PGN_data_extract)
{

    unsigned long message_address;
    unsigned char message_number;

     

            message_number = 0;
            message_address = (TQ_BMS_device_class * 0x10000) + (message_number * 0x800000) + (PGN_new_parameter_message * 0x8000000);                                   
            if ((PGN_data_extract.CAN_PGN & 0xFFFF0000) == message_address) 
            {
                
                if (PGN_data_extract.CAN_byte[1]) 
                {
                    if (BPG_data[0].interface_type_to_use == INTERFACE_CAN) BMS_CAN_enumerate();                    
                    if (BPG_data[0].interface_type_to_use == INTERFACE_RS485) BMS_RS485_enumerate();   
                }
                if (PGN_data_extract.CAN_byte[2]) 
                {
                    if (BPG_data[0].interface_type_to_use == INTERFACE_RS485) 
                    {
                        send_BMS_RS485_command(BMS_48HR_ON);   
                        send_BMS_RS485_command(BMS_BANK_OFF);   
                    }
                }
                if (PGN_data_extract.CAN_byte[3]) 
                {
                    if (BPG_data[0].interface_type_to_use == INTERFACE_RS485)
                    {
                        send_BMS_RS485_command(BMS_BANK_ON);  
                        n_second_delay(5);
                        send_BMS_RS485_command(BMS_48HR_OFF);   
                    }
                }
                
                
                
                
                if (PGN_data_extract.CAN_byte[4]) 
                {
                    if (BPG_data[0].interface_type_to_use == INTERFACE_CAN) send_BMS_CAN_command(BMS_BANK_OFF);
                    if (BPG_data[0].interface_type_to_use == INTERFACE_RS485) send_BMS_RS485_command(BMS_BANK_OFF);   
                }
                if (PGN_data_extract.CAN_byte[5]) 
                {
                    if (BPG_data[0].interface_type_to_use == INTERFACE_CAN) send_BMS_CAN_command(BMS_BANK_ON);
                    if (BPG_data[0].interface_type_to_use == INTERFACE_RS485) send_BMS_RS485_command(BMS_BANK_ON);   
                }
                if (PGN_data_extract.CAN_byte[6]) 
                {
                    if (BPG_data[0].interface_type_to_use == INTERFACE_RS485) send_BMS_RS485_command(BMS_48HR_ON);   
                }
                if (PGN_data_extract.CAN_byte[7]) 
                {
                    if (BPG_data[0].interface_type_to_use == INTERFACE_RS485) send_BMS_RS485_command(BMS_48HR_OFF);   
                }
                
            }
}

void TQ_device_send_data(unsigned char block_number)
{
	unsigned char message_number;
    double general_maths;
    union {
        signed short int data_int;
        unsigned char data_bytes[2];
    } signed_byte_split;
    union {
        unsigned short int data_int;
        unsigned char data_bytes[2];
    } unsigned_byte_split;
       
        message_number = 0;
        CAN_send_packet.CAN_PGN = unique_16bit_ID + (TQ_BMS_device_class * 0x10000) + (message_number * 0x800000) + (PGN_data_message * 0x8000000);
        
        CAN_send_packet.CAN_byte[0] = block_number;        
        unsigned_byte_split.data_int = TQ_BMS_data.block_status;
        CAN_send_packet.CAN_byte[1] = unsigned_byte_split.data_bytes[0];
        CAN_send_packet.CAN_byte[2] = unsigned_byte_split.data_bytes[1];
        unsigned_byte_split.data_int = TQ_BMS_data.block_warnings;
        CAN_send_packet.CAN_byte[3] = unsigned_byte_split.data_bytes[0];
        CAN_send_packet.CAN_byte[4] = unsigned_byte_split.data_bytes[1];
        unsigned_byte_split.data_int = TQ_BMS_data.block_errors;
        CAN_send_packet.CAN_byte[5] = unsigned_byte_split.data_bytes[0];
        CAN_send_packet.CAN_byte[6] = unsigned_byte_split.data_bytes[1];
        if (block_number > 0) CAN_send_packet.CAN_byte[7] = TQ_BMS_ID_group[block_number - 1].group;
        CAN_send(CAN_send_packet, Argo_CAN);            

        message_number = 1;
        CAN_send_packet.CAN_PGN = unique_16bit_ID + (TQ_BMS_device_class * 0x10000) + (message_number * 0x800000) + (PGN_data_message * 0x8000000);
        
        CAN_send_packet.CAN_byte[0] = block_number;                
        general_maths = TQ_BMS_data.block_cell_pack_voltage * 10;
        unsigned_byte_split.data_int = (unsigned short int)general_maths;
        CAN_send_packet.CAN_byte[1] = unsigned_byte_split.data_bytes[0];
        CAN_send_packet.CAN_byte[2] = unsigned_byte_split.data_bytes[1];      
        general_maths = TQ_BMS_data.block_current * 100;
        signed_byte_split.data_int = (signed short int)general_maths;
        CAN_send_packet.CAN_byte[3] = signed_byte_split.data_bytes[0];
        CAN_send_packet.CAN_byte[4] = signed_byte_split.data_bytes[1];              
        unsigned_byte_split.data_int = TQ_BMS_data.block_SoC;
        CAN_send_packet.CAN_byte[5] = unsigned_byte_split.data_bytes[0];
        CAN_send_packet.CAN_byte[6] = unsigned_byte_split.data_bytes[1];      
        if (block_number > 0) CAN_send_packet.CAN_byte[7] = TQ_BMS_ID_group[block_number - 1].ID;
        CAN_send(CAN_send_packet, Argo_CAN);            
        
        
        message_number = 2;
        CAN_send_packet.CAN_PGN = unique_16bit_ID + (TQ_BMS_device_class * 0x10000) + (message_number * 0x800000) + (PGN_data_message * 0x8000000);
        
        CAN_send_packet.CAN_byte[0] = block_number;                
        general_maths = TQ_BMS_data.block_cell_pack_temperature1 * 10;
        signed_byte_split.data_int = (signed short int)general_maths;
        CAN_send_packet.CAN_byte[1] = signed_byte_split.data_bytes[0];
        CAN_send_packet.CAN_byte[2] = signed_byte_split.data_bytes[1];      
        general_maths = TQ_BMS_data.block_cell_pack_temperature2 * 10;
        signed_byte_split.data_int = (signed short int)general_maths;
        CAN_send_packet.CAN_byte[3] = signed_byte_split.data_bytes[0];
        CAN_send_packet.CAN_byte[4] = signed_byte_split.data_bytes[1];      
        general_maths = TQ_BMS_data.block_maximum_cell_pack_temperature * 10;
        signed_byte_split.data_int = (signed short int)general_maths;
        CAN_send_packet.CAN_byte[5] = signed_byte_split.data_bytes[0];
        CAN_send_packet.CAN_byte[6] = signed_byte_split.data_bytes[1];      
        if (block_number > 0) CAN_send_packet.CAN_byte[7] = TQ_BMS_ID_group[block_number - 1].present;
        CAN_send(CAN_send_packet, Argo_CAN);            
        
        message_number = 3;
        CAN_send_packet.CAN_PGN = unique_16bit_ID + (TQ_BMS_device_class * 0x10000) + (message_number * 0x800000) + (PGN_data_message * 0x8000000);
        
        CAN_send_packet.CAN_byte[0] = block_number;                
        general_maths = TQ_BMS_data.block_cell_pack_voltage1 * 10;
        unsigned_byte_split.data_int = (unsigned short int)general_maths;
        CAN_send_packet.CAN_byte[1] = unsigned_byte_split.data_bytes[0];
        CAN_send_packet.CAN_byte[2] = unsigned_byte_split.data_bytes[1];      
        general_maths = TQ_BMS_data.block_cell_pack_voltage2 * 10;
        unsigned_byte_split.data_int = (unsigned short int)general_maths;
        CAN_send_packet.CAN_byte[3] = unsigned_byte_split.data_bytes[0];
        CAN_send_packet.CAN_byte[4] = unsigned_byte_split.data_bytes[1];      
        general_maths = TQ_BMS_data.block_cell_pack_voltage3 * 10;
        unsigned_byte_split.data_int = (unsigned short int)general_maths;
        CAN_send_packet.CAN_byte[5] = unsigned_byte_split.data_bytes[0];
        CAN_send_packet.CAN_byte[6] = unsigned_byte_split.data_bytes[1];    
        CAN_send_packet.CAN_byte[7] = 0;
        CAN_send(CAN_send_packet, Argo_CAN);            
        
        message_number = 4;
        CAN_send_packet.CAN_PGN = unique_16bit_ID + (TQ_BMS_device_class * 0x10000) + (message_number * 0x800000) + (PGN_data_message * 0x8000000);
        
        CAN_send_packet.CAN_byte[0] = block_number;                
        general_maths = TQ_BMS_data.block_cell_pack_voltage4 * 10;
        unsigned_byte_split.data_int = (unsigned short int)general_maths;
        CAN_send_packet.CAN_byte[1] = unsigned_byte_split.data_bytes[0];
        CAN_send_packet.CAN_byte[2] = unsigned_byte_split.data_bytes[1];      
        general_maths = TQ_BMS_data.block_cell_pack_voltage5 * 10;
        unsigned_byte_split.data_int = (unsigned short int)general_maths;
        CAN_send_packet.CAN_byte[3] = unsigned_byte_split.data_bytes[0];
        CAN_send_packet.CAN_byte[4] = unsigned_byte_split.data_bytes[1];      
        general_maths = TQ_BMS_data.block_cell_pack_voltage6 * 10;
        unsigned_byte_split.data_int = (unsigned short int)general_maths;
        CAN_send_packet.CAN_byte[5] = unsigned_byte_split.data_bytes[0];
        CAN_send_packet.CAN_byte[6] = unsigned_byte_split.data_bytes[1];    
        CAN_send_packet.CAN_byte[7] = 0;
        CAN_send(CAN_send_packet, Argo_CAN);            
        
        message_number = 5;
        CAN_send_packet.CAN_PGN = unique_16bit_ID + (TQ_BMS_device_class * 0x10000) + (message_number * 0x800000) + (PGN_data_message * 0x8000000);
        
        CAN_send_packet.CAN_byte[0] = block_number;                
        general_maths = TQ_BMS_data.block_cell_pack_voltage7 * 10;
        unsigned_byte_split.data_int = (unsigned short int)general_maths;
        CAN_send_packet.CAN_byte[1] = unsigned_byte_split.data_bytes[0];
        CAN_send_packet.CAN_byte[2] = unsigned_byte_split.data_bytes[1];      
        unsigned_byte_split.data_int = TQ_BMS_data.block_discharge_current_high;
        CAN_send_packet.CAN_byte[3] = unsigned_byte_split.data_bytes[0];
        CAN_send_packet.CAN_byte[4] = unsigned_byte_split.data_bytes[1];      
        unsigned_byte_split.data_int = TQ_BMS_data.block_discharge_cell_temperature_high;
        CAN_send_packet.CAN_byte[5] = unsigned_byte_split.data_bytes[0];
        CAN_send_packet.CAN_byte[6] = unsigned_byte_split.data_bytes[1];      
        CAN_send_packet.CAN_byte[7] = 0;
        CAN_send(CAN_send_packet, Argo_CAN);            
        
        message_number = 6;
        CAN_send_packet.CAN_PGN = unique_16bit_ID + (TQ_BMS_device_class * 0x10000) + (message_number * 0x800000) + (PGN_data_message * 0x8000000);
        
        CAN_send_packet.CAN_byte[0] = block_number;                
        unsigned_byte_split.data_int = TQ_BMS_data.block_discharge_cell_temperature_low;
        CAN_send_packet.CAN_byte[1] = unsigned_byte_split.data_bytes[0];
        CAN_send_packet.CAN_byte[2] = unsigned_byte_split.data_bytes[1];      
        unsigned_byte_split.data_int = TQ_BMS_data.block_charge_current_high;
        CAN_send_packet.CAN_byte[3] = unsigned_byte_split.data_bytes[0];
        CAN_send_packet.CAN_byte[4] = unsigned_byte_split.data_bytes[1];      
        unsigned_byte_split.data_int = TQ_BMS_data.block_charge_cell_temperature_high;
        CAN_send_packet.CAN_byte[5] = unsigned_byte_split.data_bytes[0];
        CAN_send_packet.CAN_byte[6] = unsigned_byte_split.data_bytes[1];      
        CAN_send_packet.CAN_byte[7] = 0;
        CAN_send(CAN_send_packet, Argo_CAN);            

        message_number = 7;
        CAN_send_packet.CAN_PGN = unique_16bit_ID + (TQ_BMS_device_class * 0x10000) + (message_number * 0x800000) + (PGN_data_message * 0x8000000);
        
        CAN_send_packet.CAN_byte[0] = block_number;                
        unsigned_byte_split.data_int = TQ_BMS_data.block_charge_cell_temperature_low;
        CAN_send_packet.CAN_byte[1] = unsigned_byte_split.data_bytes[0];
        CAN_send_packet.CAN_byte[2] = unsigned_byte_split.data_bytes[1];      
        unsigned_byte_split.data_int = TQ_BMS_data.block_charge_voltage_high;
        CAN_send_packet.CAN_byte[3] = unsigned_byte_split.data_bytes[0];
        CAN_send_packet.CAN_byte[4] = unsigned_byte_split.data_bytes[1];      
        unsigned_byte_split.data_int = TQ_BMS_data.block_deep_discharge;
        CAN_send_packet.CAN_byte[5] = unsigned_byte_split.data_bytes[0];
        CAN_send_packet.CAN_byte[6] = unsigned_byte_split.data_bytes[1];      
        CAN_send_packet.CAN_byte[7] = 0;
        CAN_send(CAN_send_packet, Argo_CAN);            
        
        
}


void send_BMS_RS485_command(unsigned char command)
{
    unsigned char block_loop, command_result;

        if (command == BMS_BANK_ON)
        {
            LED_active = 1;
            LED_power = 1;
            bank_power_control = 1;                             
            n_second_delay(2);

            //Wake up the BMS by pressing the power button whilst sending data
            torqeedo_data_TX[0] = 0xAC;
            torqeedo_data_TX[1] = 0x80;   
            torqeedo_data_TX[2] = 1;    
            //3 - CRC
            torqeedo_data_TX[4] = 0xAD;
            torqeedo_data_TX[5] = 0xFF;    

            torqeedo_data_TX[3] = CRC8(3);
            RS485_send_string(5);

            bank_power_control = 0;
            LED_active = 0;
            LED_power = 1;      

        }
        if (command == BMS_BANK_OFF)
        {
            LED_active = 1;
            LED_power = 1;
            //Turn on the batteries then off. If they were off already then a simple turn off would turn them on!
            bank_power_control = 1;                             
            n_second_delay(2);       
            bank_power_control = 0;        
            n_second_delay(1);

            bank_power_control = 1;
            n_second_delay(8);
            bank_power_control = 0;
            LED_active = 0;
            LED_power = 1;
        }

        if (command == BMS_48HR_OFF)
        {
            for (block_loop = 0; block_loop < BPG_data[0].BMS_block_count; block_loop++)
            {
                torqeedo_data_TX[0] = 0xAC;
                torqeedo_data_TX[1] = 0x81 + block_loop;   
                torqeedo_data_TX[2] = 0x8F;    
                torqeedo_data_TX[3] = 0x23;
                torqeedo_data_TX[4] = 0x45;    

                torqeedo_data_TX[6] = 0xAD;
                torqeedo_data_TX[7] = 0xFF;
                torqeedo_data_TX[5] = CRC8(5);
                RS485_send_string(5);
                command_result = RS485_receive_string();
                n_second_delay(2);
                torqeedo_data_TX[0] = 0xAC;
                torqeedo_data_TX[1] = 0x81 + block_loop;   
                torqeedo_data_TX[2] = 0x90;    
                torqeedo_data_TX[3] = 0x00;
                torqeedo_data_TX[4] = 0x00; //0x30 to turn on    
                torqeedo_data_TX[5] = 0x00;
                torqeedo_data_TX[6] = 0x10;
                torqeedo_data_TX[7] = 0x00;
                torqeedo_data_TX[8] = 0x00;
                torqeedo_data_TX[9] = 0x00;
                torqeedo_data_TX[10] = 0x00;
                                              
                torqeedo_data_TX[12] = 0xAD;
                torqeedo_data_TX[13] = 0xFF;
                torqeedo_data_TX[11] = CRC8(11);
                RS485_send_string(5);
                command_result = RS485_receive_string();
                n_second_delay(1);
            }
        }
        if (command == BMS_48HR_ON)
        {
            for (block_loop = 0; block_loop < BPG_data[0].BMS_block_count; block_loop++)
            {
                torqeedo_data_TX[0] = 0xAC;
                torqeedo_data_TX[1] = 0x81 + block_loop;   
                torqeedo_data_TX[2] = 0x8F;    
                torqeedo_data_TX[3] = 0x23;
                torqeedo_data_TX[4] = 0x45;    

                torqeedo_data_TX[6] = 0xAD;
                torqeedo_data_TX[7] = 0xFF;
                torqeedo_data_TX[5] = CRC8(5);
                RS485_send_string(5);
                command_result = RS485_receive_string();
                n_second_delay(2);
                torqeedo_data_TX[0] = 0xAC;
                torqeedo_data_TX[1] = 0x81 + block_loop;   
                torqeedo_data_TX[2] = 0x90;    
                torqeedo_data_TX[3] = 0x00;
                torqeedo_data_TX[4] = 0x30; //0x30 to turn on    
                torqeedo_data_TX[5] = 0x00;
                torqeedo_data_TX[6] = 0x10;
                torqeedo_data_TX[7] = 0x00;
                torqeedo_data_TX[8] = 0x00;
                torqeedo_data_TX[9] = 0x00;
                torqeedo_data_TX[10] = 0x00;
                                              
                torqeedo_data_TX[12] = 0xAD;
                torqeedo_data_TX[13] = 0xFF;
                torqeedo_data_TX[11] = CRC8(11);
                RS485_send_string(5);
                command_result = RS485_receive_string();
                n_second_delay(1);
            }
        }

    
}
        