#ifndef VERSION_H
#define	VERSION_H


// ____  _  _  ____  ____  ___  ____  ____  __   
//(_  _)( \( )(_  _)( ___)/ __)(  _ \( ___)(  )  
// _)(_  )  (   )(   )__)( (_-. )   / )__)  )(__ 
//(____)(_)\_) (__) (____)\___/(_)\_)(____)(____)




//Firmware version. Update this every time a new version control entry is added. This data is displayed on the LCD.
// To fix unresolved identifiers delete the contents of this folder: \AppData\Local\mplab_ide\Cache\dev\v3.00\var
#define firmware_revision 1045

//****************************************************************************************************************************
// Version control. Update this value every time you upload to Google Drive
// 
// Last changed: WAG, 10:09 26/10/20
// Firmware version: 1.045
// Major addition: 3.3V energy saving implemented
//                 
// Notes: 
//
//****************************************************************************************************************************

//****************************************************************************************************************************
// Version control. Update this value every time you upload to Google Drive
// 
// Last changed: WAG, 15:09 21/10/20
// Firmware version: 1.044
// Major addition: Pre-charge turned off before diode swap to reduce 3.3V current draw
//                 
// Notes: 
//
//****************************************************************************************************************************


//****************************************************************************************************************************
// Version control. Update this value every time you upload to Google Drive
// 
// Last changed: WAG, 9:13 15/9/20
// Firmware version: 1.043
// Major addition: Battery missing changed from a variable to half the disconnect voltage
//                 
// Notes: 
//
//****************************************************************************************************************************


//****************************************************************************************************************************
// Version control. Update this value every time you upload to Google Drive
// 
// Last changed: WAG, 9:13 11/8/20
// Firmware version: 1.042
// Major addition: HW1.007 min chages for diode supression and pre-chargeApp_BPG_1042
//                 
// Notes: 
//
//****************************************************************************************************************************

//****************************************************************************************************************************
// Version control. Update this value every time you upload to Google Drive
// 
// Last changed: WAG, 9:13 03/7/20
// Firmware version: 1.041
// Major addition: Missing block timeout for TQ added
//                 
// Notes: 
//
//****************************************************************************************************************************

//****************************************************************************************************************************
// Version control. Update this value every time you upload to Google Drive
// 
// Last changed: WAG, 11:00 12/6/20
// Firmware version: 1.040
// Major addition: New boot loader
//                 
// Notes: 
//
//****************************************************************************************************************************


//****************************************************************************************************************************
// Version control. Update this value every time you upload to Google Drive
// 
// Last changed: WAG, 12:47 11/6/20
// Firmware version: 1.039
// Major addition: Option for the remote button to toggle the TQ on off not the contactor. Derived from block count not volts
//                 
// Notes: 
//
//****************************************************************************************************************************

//****************************************************************************************************************************
// Version control. Update this value every time you upload to Google Drive
// 
// Last changed: WAG, 11:47 11/6/20
// Firmware version: 1.038
// Major addition: Option for the remote button to toggle the TQ on off not the contactor
//                 
// Notes: 
//
//****************************************************************************************************************************


//****************************************************************************************************************************
// Version control. Update this value every time you upload to Google Drive
// 
// Last changed: WAG, 12:20 8/6/20
// Firmware version: 1.037
// Major addition: Pre-charge added
//                 
// Notes: 
//
//****************************************************************************************************************************

//****************************************************************************************************************************
// Version control. Update this value every time you upload to Google Drive
// 
// Last changed: WAG, 16:20 21/5/20
// Firmware version: 1.036
// Major addition: Timer interrupt bug fixed. Bootloader pin outs updated
//                 
// Notes: 
//
//****************************************************************************************************************************

//****************************************************************************************************************************
// Version control. Update this value every time you upload to Google Drive
// 
// Last changed: WAG, 12:20 20/5/20
// Firmware version: 1.035
// Major addition: Delay for 48hour turn off and BMS warning error
//                 
// Notes: 
//
//****************************************************************************************************************************


//****************************************************************************************************************************
// Version control. Update this value every time you upload to Google Drive
// 
// Last changed: WAG, 12:16 18/5/20
// Firmware version: 1.034
// Major addition: Present, group and ID flag harvest out of enum and into the query cycle
//                 
// Notes: 
//
//****************************************************************************************************************************


//****************************************************************************************************************************
// Version control. Update this value every time you upload to Google Drive
// 
// Last changed: WAG, 14:46 15/5/20
// Firmware version: 1.033
// Major addition: New present flag
//                 
// Notes: 
//
//****************************************************************************************************************************



//****************************************************************************************************************************
// Version control. Update this value every time you upload to Google Drive
// 
// Last changed: WAG, 13:46 15/5/20
// Firmware version: 1.032
// Major addition: ID/group array 0 based but block was 1 based
//                 
// Notes: 
//
//****************************************************************************************************************************

//****************************************************************************************************************************
// Version control. Update this value every time you upload to Google Drive
// 
// Last changed: WAG, 11:46 14/5/20
// Firmware version: 1.031
// Major addition: 16bit offset bug in TQ data feed. Added ID and group info
//                 
// Notes: 
//
//****************************************************************************************************************************

//****************************************************************************************************************************
// Version control. Update this value every time you upload to Google Drive
// 
// Last changed: WAG, 16:41 6/5/20
// Firmware version: 1.030
// Major addition: live block count bug fixed
//                 
// Notes: 
//
//****************************************************************************************************************************

//****************************************************************************************************************************
// Version control. Update this value every time you upload to Google Drive
// 
// Last changed: WAG, 14:27 6/5/20
// Firmware version: 1.029
// Major addition: 48hour shut off and block count fix
//                 
// Notes: 
//
//****************************************************************************************************************************

//****************************************************************************************************************************
// Version control. Update this value every time you upload to Google Drive
// 
// Last changed: WAG, 11:27 6/5/20
// Firmware version: 1.028
// Major addition: Battery power on/off with BMS wake. Cycle through battery data. Send via Argo.
//                 
// Notes: 
//
//****************************************************************************************************************************

//****************************************************************************************************************************
// Version control. Update this value every time you upload to Google Drive
// 
// Last changed: WAG, 10:20 1/5/20
// Firmware version: 1.027
// Major addition: Device power up options based on interface used.
//                 
// Notes: 
//
//****************************************************************************************************************************


//****************************************************************************************************************************
// Version control. Update this value every time you upload to Google Drive
// 
// Last changed: WAG, 13:20 29/4/20
// Firmware version: 1.026
// Major addition: Fix for premature end of RS485 data when data contains AD FF in the middle
//                 
// Notes: 
//
//****************************************************************************************************************************

//****************************************************************************************************************************
// Version control. Update this value every time you upload to Google Drive
// 
// Last changed: WAG, 15:30 27/4/20
// Firmware version: 1.025
// Major addition: New half duplex RS485. Direction constant round the wrong way. Direction changed for string send not
//                  individual chars. Echo gulp removed.
// Notes: 
//
//****************************************************************************************************************************


//****************************************************************************************************************************
// Version control. Update this value every time you upload to Google Drive
// 
// Last changed: WAG, 10:30 14/2/20
// Firmware version: 1.024
// Major addition: Enumeration alarms added and error checking
//                 
// Notes: 
//
//****************************************************************************************************************************

//****************************************************************************************************************************
// Version control. Update this value every time you upload to Google Drive
// 
// Last changed: WAG, 16:14 12/2/20
// Firmware version: 1.023
// Major addition: RS485 direct TQ battery enumeration added
//                 
// Notes: 
//
//****************************************************************************************************************************

//****************************************************************************************************************************
// Version control. Update this value every time you upload to Google Drive
// 
// Last changed: WAG, 13:08 11/2/20
// Firmware version: 1.022
// Major addition: Enumeration alarms added
//                 
// Notes: 
//
//****************************************************************************************************************************

//****************************************************************************************************************************
// Version control. Update this value every time you upload to Google Drive
// 
// Last changed: WAG, 9:08 6/2/20
// Firmware version: 1.021
// Major addition: Enumerate BMS batteries must wait for a 2 return, takes 2-3 minutes
//                 
// Notes: 
//
//****************************************************************************************************************************

//****************************************************************************************************************************
// Version control. Update this value every time you upload to Google Drive
// 
// Last changed: WAG, 11:26 5/2/20
// Firmware version: 1.020
// Major addition: Enumerate BMS batteries. Updated data PGN and instant parameter PGN for blocks/enum. Device class unset fix. 
//                 FRAM entry for block count.
// Notes: 
//
//****************************************************************************************************************************

//****************************************************************************************************************************
// Version control. Update this value every time you upload to Google Drive
// 
// Last changed: WAG, 10:13 5/2/20
// Firmware version: 1.019
// Major addition: Panic disconnect alarm removed. Remote LED now matches local switch position LED
//
// Notes: 
//
//****************************************************************************************************************************


//****************************************************************************************************************************
// Version control. Update this value every time you upload to Google Drive
// 
// Last changed: WAG, 10:32 15/12/19
// Firmware version: 1.018
// Major addition: Command to disconnect other switches on an alarm to protect bank inbalance.
//
// Notes: 
//
//****************************************************************************************************************************

//****************************************************************************************************************************
// Version control. Update this value every time you upload to Google Drive
// 
// Last changed: WAG, 10:32 7/12/19
// Firmware version: 1.017
// Major addition: BMS autodetect added (see constant toggle) to account for HF not yet supporting the BPG
//
// Notes: 
//
//****************************************************************************************************************************

//****************************************************************************************************************************
// Version control. Update this value every time you upload to Google Drive
// 
// Last changed: WAG, 15:32 22/11/19
// Firmware version: 1.016
// Major addition: Horrible interaction between switch stuck, power off and 30 second low voltage override resolved
//
// Notes: 
//
//****************************************************************************************************************************


//****************************************************************************************************************************
// Version control. Update this value every time you upload to Google Drive
// 
// Last changed: WAG, 11:32 22/11/19
// Firmware version: 1.015
// Major addition: Switch stuck prevents low voltage 30 second reset. <30V triggers missing battery alarm above all others
//
// Notes: 
//
//****************************************************************************************************************************

//****************************************************************************************************************************
// Version control. Update this value every time you upload to Google Drive
// 
// Last changed: WAG, 16:00 21/11/19
// Firmware version: 1.014
// Major addition: Switch stuck resolved by a manual actuation rather than a POR
//
// Notes: 
//
//****************************************************************************************************************************


//****************************************************************************************************************************
// Version control. Update this value every time you upload to Google Drive
// 
// Last changed: WAG, 16:00 7/11/19
// Firmware version: 1.013
// Major addition: TQ CAN comms, new panic PGN for TQ BMS, battery bank BMS on/off control and new BMS BPG PGN messaages
//                 Purge modules that are no loner on the system. Low voltage and temp reset 30 second timer on enable
// Notes: Hayle
//
//****************************************************************************************************************************

//****************************************************************************************************************************
// Version control. Update this value every time you upload to Google Drive
// 
// Last changed: WAG, 12:00 1/11/19
// Firmware version: 1.012
// Major addition: LED off when in power saving mode. Boot loader. RPM check removed and replaced with 5 second HV delay and 
//                 changeable LV delay
//
// Notes: None
//
//****************************************************************************************************************************


//****************************************************************************************************************************
// Version control. Update this value every time you upload to Google Drive
// 
// Last changed: WAG, 10:31 24/10/19
// Firmware version: 1.011
// Major addition: Lower current in idle, new HW1.004
//
// Notes: None
//
//****************************************************************************************************************************

//****************************************************************************************************************************
// Version control. Update this value every time you upload to Google Drive
// 
// Last changed: WAG, 14:40 11/10/19
// Firmware version: 1.010
// Major addition: Stuck switch code. New pin outs for the H-bridge change
//
// Notes: None
//
//****************************************************************************************************************************



//****************************************************************************************************************************
// Version control. Update this value every time you upload to Google Drive
// 
// Last changed: WAG, 9:11 28/08/19
// Firmware version: 1.009
// Major addition: Send charge start on BMS charge line clear
//
// Notes: None
//
//****************************************************************************************************************************

//****************************************************************************************************************************
// Version control. Update this value every time you upload to Google Drive
// 
// Last changed: WAG, 11:30 27/08/19
// Firmware version: 1.008
// Major addition: BPG LED now mirrors microswitch rather than actuation. Victron BMS now OEG charge stop not disconnect.
//                 CAN wake on alarm tweaks
// Notes: None
//
//****************************************************************************************************************************


//****************************************************************************************************************************
// Version control. Update this value every time you upload to Google Drive
// 
// Last changed: WAG, 17:10 9/07/19
// Firmware version: 1.007
// Major addition: Logs added. BPG switch fail alarm and stop switching on failure. Wake on alarm added. BMS IO swapped
//                 Ambient temperature now use constants  
// Notes: None
//
//****************************************************************************************************************************


//****************************************************************************************************************************
// Version control. Update this value every time you upload to Google Drive
// 
// Last changed: WAG, 12:36 13/05/19
// Firmware version: 1.006
// Major addition: V1.001 fork
//                 
// Notes: None
//
//****************************************************************************************************************************

//****************************************************************************************************************************
// Version control. Update this value every time you upload to Google Drive
// 
// Last changed: WAG, 11:36 03/05/19
// Firmware version: 1.005
// Major addition: 12bit ADC set, calibrated
//                 
// Notes: None
//
//****************************************************************************************************************************

//****************************************************************************************************************************
// Version control. Update this value every time you upload to Google Drive
// 
// Last changed: WAG, 10:36 30/04/19
// Firmware version: 1.004
// Major addition: ID not set so panic PGN never sent
//                 
// Notes: None
//
//****************************************************************************************************************************

//****************************************************************************************************************************
// Version control. Update this value every time you upload to Google Drive
// 
// Last changed: WAG, 14:36 17/04/19
// Firmware version: 1.003
// Major addition: Panic PGN reaction and low voltage charge scenario added. Collated alarm added. charge override time added.
//                 Provision for second PCB iteration parameters and data points
// Notes: None
//
//****************************************************************************************************************************

//****************************************************************************************************************************
// Version control. Update this value every time you upload to Google Drive
// 
// Last changed: WAG, 16:22 02/04/19
// Firmware version: 1.002
// Major addition: CAN comms added
// Notes: None
//
//****************************************************************************************************************************

//****************************************************************************************************************************
// Version control. Update this value every time you upload to Google Drive
// 
// Last changed: WAG, 16:06 29/03/19
// Firmware version: 1.001
// Major addition: PCB temp fixed. RA4 inversion removed. Secondary osc disabled. BMS flags inverted
// Notes: None
//
//****************************************************************************************************************************

//****************************************************************************************************************************
// Version control. Update this value every time you upload to Google Drive
// 
// Last changed: WAG, 14:06 28/02/19
// Firmware version: 1.000
// Major addition: Start based on Field coil code
// Notes: None
//
//****************************************************************************************************************************



#endif	

