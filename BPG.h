#ifndef BPG_H
#define BPG_H

// I2C addresses
#define EUI48_address                   0b10100000
#define TEMP_address                    0b10010000

#define auto_detect_BMS_enabled                 1
#define link_switches_enabled                   0

#define bank_voltage_channel                    6
#define signal1_voltage_channel                 8
#define signal2_voltage_channel                 7
#define battery_thermistor_1_channel            0
#define battery_thermistor_2_channel            1

#define PGN_data_message                        0
#define PGN_current_parameter_message           1
#define PGN_new_parameter_message               2

#define FRAM_log_start                          80
#define FRAM_log_end                           8191

#define bank_sensor_max                         8
#define fcoil_max                               4
#define BPG_max                                 8
#define relay_on                                1               //0 for HW1.002, 1 for HW1.004
#define relay_off                               0               //1 for HW1.002, 0 for HW1.004

#define use_pre_charge                          1
#define remote_switch_controls_TQ               0

#define boot_start                              0x400
#define bank_data_message_complete              63
#define bank_parameter_message_complete         4095
#define field_coil_data_message_complete        255
#define field_coil_parameter_message_complete   127
#define BPG_data_message_complete               31
#define BPG_parameter_message_complete          63
#define alarm_persistence_time                  10
#define thermistor_beta                         3968.0                      //Beta (K) of the thermistor
#define ambient_max                             80
#define ambient_min                             -10
#define emergency_fan_activation_temperature    50
#define emergency_fan_deactivation_temperature  48
#define stuck_switch_retry_attempts             10
#define module_disconnect_timeout               60
#define boot_start                              0x400
#define DC_voltage_ratio                        50.82
#define Lithium_baud                            125

#define block_timeout_period                    120
//Ratios, averages and constants


// Default variables
#define FCY                             16000000UL                   //Internal LRC is PLL to 32MHz, 16MHz FCY is half oscillator frequency. Used by timers, delays and baud calculations

//Useful macros
#define delay_ms(A)                     __delay_ms(A)
#define delay_us(A)                     __delay_us(A)
#define check_bit(number,position)      ((number) & (1<<(position)))
#define hi_word(A)                      (((A) >> 8) & 0xFF)
#define lo_word(A)                      ((A) & 0xFF)
#define square(A)                       (A * A)
#define cube(A)                         (A * A * A)
#define fourth(A)                       (A * A * A * A)
#define fifth(A)                        (A * A * A * A * A)

//Pin assignments constants output

#define pre_charge                      LATBbits.LATB11
#define LED_active                      LATBbits.LATB0
#define LED_power                       LATBbits.LATB0
#define LED_switch_position             LATBbits.LATB15
#define LED_remote                      LATBbits.LATB6

#define relay1_control                  LATCbits.LATC6
#define relay2_control                  LATCbits.LATC7

#define FRAM_select                     LATBbits.LATB3
#define Argo_select                     LATBbits.LATB2
#define Lithium_select                  LATAbits.LATA2

#define RS485_control                   LATAbits.LATA8
#define regulator_CAN_control           LATAbits.LATA9
#define MCP2515_CAN_control             LATAbits.LATA3
#define diode_control                   LATCbits.LATC9

#define RS485_direction                 LATBbits.LATB7
#define bank_power_control              LATCbits.LATC8

//Pin assignments constants input
#define remote_override_switch          PORTAbits.RA4
#define switch_position                 !PORTAbits.RA7
#define factory_reset_switch            PORTAbits.RA10

#define timer_trigger_flag              IFS1bits.T5IF
#define RX_timer_trigger_flag           IFS0bits.T3IF
#define time_out_flag                   IFS0bits.T1IF


//Structs

//CAN data structure
typedef struct {
	unsigned long CAN_PGN;
	unsigned char CAN_byte[9];
} CAN_data_packet;

//Field coil data structure
typedef struct {
    double generator_voltage;
    double generator_current;
	double field_current;
	double field_voltage;
    unsigned short int generator_RPM;
    unsigned short int engine_RPM;
    double ambient_temperature;
    double diode_temperature;
    double engine_temperature;
    double PCB_temperature;
    double generator_temperature;
    unsigned char run_mode;
    double fuel_rate_J1939;           
    double engine_temperature_J1939;
    unsigned short int oil_pressure_J1939;
    double coolant_temperature_J1939;
    double lowest_float_voltage;
    double lowest_absorption_voltage;
    unsigned char charge_mode;
    unsigned char number_of_banks;
    double maximum_generated_voltage;
    unsigned char alarm;
    unsigned char blower_fan_state;
    unsigned char diode_fan_state;   
    unsigned char cyclical_counter;
    unsigned char cyclical_counter_compare;
    unsigned short int sensor_time_out;    
    double trip_voltage;
    double available_engine_power;
    double prop_load;
    double target_voltage;
    double fuel_level;
    unsigned char field_name_index;
    unsigned short int PWM_raw;
    unsigned char PWM_percent;
    unsigned short int fuel_pps;
    unsigned char native_in_gear;
    unsigned char J1939_available;
    unsigned short int engine_RPM_J1939;
    unsigned short int complete_data_set;
    unsigned short int complete_parameter_set;
    unsigned short int ID_number;
    unsigned char last_message_received;
    unsigned char CAN_device_class;
    unsigned short int firmware;
    unsigned char J1939_in_gear;
    unsigned char updated_parameter_status;
    //Parameters
    unsigned short int maximum_engine_power;
    unsigned short int maximum_engine_RPM;
    unsigned short int minimum_engine_RPM;
    unsigned short int maximum_generator_power;
    double nominal_generator_temperature;
    double maximum_generator_temperature;
    double emergency_generator_temperature;
    double maximum_generator_current;
    double diode_fan_activation_temperature;
    double diode_fan_max_speed_temperature;  
    double blower_fan_activation_temperature;
    double maximum_charge_voltage;
    double minimum_battery_voltage_to_charge;
    double generator_to_engine_pulley_ratio;
    double minimum_generator_temperature;
    double engine_start_temperature;
    double default_float_safety_voltage;
    double maximum_oil_pressure;
    double maximum_coolant_temperature;
    double maximum_fuel_level;
    double maximum_diode_temperature;
    unsigned short int maximum_field_coil_power;
    unsigned short int maximum_water_level;
    unsigned char update_parameter_status;
    unsigned char use_1939_gear_detect;
    double prop_RPM_intercept;
    double pulse_to_RPM_ratio;
    unsigned char clock_minute;
    unsigned char clock_hour;
    unsigned char clock_day;
    unsigned char clock_month;
    unsigned char clock_year;    
    unsigned char clock_adjusted;
    unsigned char pair_index;
} field_coil_data;

//Battery monitor Field coil data structure
typedef struct {
    unsigned short int ID_number;
    unsigned char CAN_device_class;
    unsigned char last_message_received;
    unsigned short int complete_data_set;
    unsigned short int complete_parameter_set;
    
	double bank_voltage;
	double DC_current;
    unsigned char actual_SoC;
    unsigned char reference_SoC;
    unsigned short int time_to_empty;
    
    double block_temperature1;
    double block_temperature2;
    double block_temperature3;
    double block_temperature4;
    
    double block_voltage1;
    double block_voltage2;
    double block_voltage3;
    double block_voltage4; 
    
    double absorption_charge_voltage;
    double float_charge_voltage;
    double equalisation_voltage;
    unsigned char charge_mode;
    unsigned char cyclical_counter;
    unsigned char cyclical_counter_compare;
    unsigned short int sensor_time_out;
    
    unsigned short int time_till_float;
    unsigned char sensor_name_index;
    unsigned char updated_parameter_status;
    
    double firmware_version;
    unsigned char current_polarity;
    unsigned char alarm;
    //Parameters
    unsigned char use_pair_index;
    unsigned short int bank_Ah_capacity;
    unsigned char charge_efficiency;
    unsigned char discharge_efficiency;
    unsigned char battery_chemistry;
    double nominal_block_voltage;
    unsigned char rated_discharge_time;
    
    double absorption_voltage;
    double float_voltage;
    double Peukerts_constant;
    double temperature_compensation;

    unsigned char min_SoC;    
    unsigned short int lifetime_kWh;
    unsigned char time_before_OCV_reset;
    unsigned char max_time_at_absorption;   
    unsigned char max_bulk_current;    
    double full_charge_current;

    double min_datum_temperature;
    double max_datum_temperature;
    double OCV_current_limit;
    unsigned char pair_index;
    unsigned char number_of_blocks;
    
    double minimum_absorption_temperature;
    double maximum_absorption_temperature;
    double minimum_float_temperature;
    double maximum_float_temperature;
    
    double minimum_absorption_block_voltage;
    double maximum_absorption_block_voltage;
    double minimum_float_block_voltage;
    double maximum_float_block_voltage;
    
    double centre_block_voltage_trigger;
    double current_threshold_trigger;    
    
    double lifetime_kWh_in;
    double lifetime_kWh_out;
    double kWh_in_since_float;
    double kWh_out_since_float;
    unsigned char device_class_set;
    unsigned char old_device_class_set;

    double warning_voltage;
    double disconnect_voltage;
    double warning_SoC;
    double disconnect_SoC;

// Lithium
    unsigned short int battery_manufacturer;
    double Lithium_bulk_target_voltage;
    double Lithium_float_target_voltage;
    double Lithium_min_charge_temperature;
    double Lithium_max_charge_temperature;
    double Lithium_min_discharge_temperature;
    double Lithium_max_discharge_temperature;
    double Lithium_min_voltage;
    double Lithium_max_voltage;
    double Lithium_max_discharge_current;
    double Lithium_max_charge_current;
    double Lithium_cycle_reset_SoC;
    double Lithium_float_trip_accuracy;
    double Lithium_safe_fallback_voltage;
    unsigned short int battery_model;    
    
} battery_bank_data;

//BPG data structure
typedef struct {
    unsigned short int ID_number;
    unsigned char CAN_device_class;
    unsigned char last_message_received;
    unsigned short int complete_data_set;
    unsigned short int complete_parameter_set;
    double bank_switch_voltage;
    double maximum_bank_current;
    double minimum_bank_current;
    unsigned char current_switch_position;
    unsigned char last_disconnect_reason;
    double ambient_temperature;
    unsigned char cyclical_counter;    
    unsigned char cyclical_counter_compare;
    unsigned short int sensor_time_out;
    unsigned char name_index;
    unsigned char pair_index;
    unsigned char alarm;
    double IO_signal1_voltage_no_discharge;
    double IO_signal2_voltage_no_charge;
    unsigned char RS232_data_present;
    unsigned char RS485_data_present;
    unsigned char battery_CAN_present;
    unsigned short int firmware;
    double battery_thermistor_1;
    double battery_thermistor_2;
    unsigned char fan_state;
    unsigned char updated_parameter_status;
    unsigned char parameter_complete_counter;
    unsigned char BMS_block_count;
    unsigned char BMS_live_block_count;
    unsigned char BMS_error;
    //Parameters
    unsigned short int battery_manufacturer;
    unsigned short int battery_model;
    unsigned char interface_type_to_use;
    unsigned char use_IO_interface;
    unsigned char allow_remote_switching;
    double low_voltage_disconnect;
    double high_voltage_disconnect;
    double low_temperature_disconnect;
    double high_temperature_disconnect;
    double low_voltage_reconnect;
    double high_voltage_reconnect;
    double low_temperature_reconnect;
    double high_temperature_reconnect;
    double high_current_disconnect;
    double BMS_IO_trigger_voltage;
    unsigned char voltage_time_hysteresis;
    unsigned char temperature_time_hysteresis;
    unsigned char current_time_hysteresis;
    unsigned char BMS_IO_disconnect_delay;
    unsigned char scan_only_paired_bank_current;
    unsigned char bank_class_currents_to_scan;
    unsigned short int voltage_measurement_wake_interval;
    unsigned short int no_Argo_activity_sleep_interval;
    signed char fan_activation_temperature;
    signed char fan_deactivation_temperature;    
    unsigned char enable_OCV_SoC_reset;
    unsigned short int time_before_OCV_SoC_reset;
    double OCV_SoC_reset_current_limit;  
    unsigned char charge_override_time_hysteresis;
} battery_protection_gateway_data;

//Parameters
typedef struct {
    unsigned char parameter_complete_counter;
    unsigned char pair_index;
    unsigned char name_index;
    unsigned char complete_parameter_set;
    unsigned short int battery_manufacturer;
    unsigned short int battery_model;
    unsigned char interface_type_to_use;
    unsigned char use_IO_interface;
    unsigned char allow_remote_switching;
    double low_voltage_disconnect;
    double high_voltage_disconnect;
    double low_temperature_disconnect;
    double high_temperature_disconnect;
    double low_voltage_reconnect;
    double high_voltage_reconnect;
    double low_temperature_reconnect;
    double high_temperature_reconnect;
    double high_current_disconnect;
    double BMS_IO_trigger_voltage;
    unsigned char voltage_time_hysteresis;
    unsigned char temperature_time_hysteresis;
    unsigned char current_time_hysteresis;
    unsigned char BMS_IO_disconnect_delay;
    unsigned char scan_only_paired_bank_current;
    unsigned char bank_class_currents_to_scan;
    unsigned short int voltage_measurement_wake_interval;
    unsigned short int no_Argo_activity_sleep_interval;
    signed char fan_activation_temperature;
    signed char fan_deactivation_temperature;
    unsigned char enable_OCV_SoC_reset;
    unsigned short int time_before_OCV_SoC_reset;
    double OCV_SoC_reset_current_limit;      
    unsigned char charge_override_time_hysteresis;
} battery_protection_gateway_parameters;

typedef struct {
    unsigned char stop_generator;
    unsigned char start_generator;
    unsigned char disconnect_battery_bank;
    unsigned char reconnect_battery_bank;
    unsigned char system_sleep;
    unsigned char pause_CAN_bus_activity;
    unsigned char resume_CAN_bus_activity;
    unsigned char reconnect_battery_bank_alarm_override;    
} panic_data;

typedef struct {
    double block_cell_pack_voltage;
    unsigned short int block_status;
    unsigned short int block_warnings;
    unsigned short int block_errors;
    double block_current;
    unsigned short int block_SoC;
    double block_cell_pack_temperature1;
    double block_cell_pack_temperature2;
    double block_maximum_cell_pack_temperature;
    double block_cell_pack_voltage1;
    double block_cell_pack_voltage2;
    double block_cell_pack_voltage3;
    double block_cell_pack_voltage4;
    double block_cell_pack_voltage5;
    double block_cell_pack_voltage6;
    double block_cell_pack_voltage7;
    unsigned short int block_discharge_current_high;
    unsigned short int block_discharge_cell_temperature_high;
    unsigned short int block_discharge_cell_temperature_low;
    unsigned short int block_charge_current_high;
    unsigned short int block_charge_cell_temperature_high;
    unsigned short int block_charge_cell_temperature_low;
    unsigned short int block_charge_voltage_high;
    unsigned short int block_deep_discharge;    
} Torqeedo_BMS_data;

typedef struct {
    unsigned char ID;
    unsigned char group;
    unsigned char present;
} Torqeedo_BMS_ID;

//Procedure and function definitions
int main(void);
void n_second_delay (unsigned char length);
double FRAM_variable_double(unsigned int FRAM_location, double data_value, unsigned char write_enable, double default_value);
unsigned long long int FRAM_variable_RW(unsigned long FRAM_location, unsigned long long int data_size, unsigned long long int data_value, unsigned char write_enable, unsigned long long int defaults);
unsigned char FRAM_read_byte(unsigned int FRAM_address);
void FRAM_write_byte(unsigned int FRAM_address, unsigned char byte);
void check_virgin_FRAM(void);

void UART_init(unsigned long baud, unsigned char UART_channel);

void RS232_send_char(unsigned char send_byte);
unsigned short int RS232_receive_char(unsigned short int timeout);
void RS232_send_string(const char *data_string, unsigned int length);
unsigned char RS232_receive_string(char *data_string, unsigned int length);

void RS485_send_char(unsigned char send_byte);
unsigned short int RS485_receive_char(double timeout);
void RS485_send_string(unsigned char command_length);
unsigned char RS485_receive_string(void);


void CAN_select(unsigned char CAN_interface, unsigned char select_set);
void CAN_reset(unsigned char CAN_interface, unsigned short int baud);

unsigned char CAN_buffer_check(unsigned char CAN_interface);
void CAN_buffer(unsigned char buffer_number, unsigned char CAN_interface);
void CAN_send(CAN_data_packet CAN_send_data, unsigned char CAN_interface);
void SPI_init (void);
unsigned char SPI_write(unsigned char data);
unsigned char SPI_read(void);


void I2C1_init(void);
void I2C1_start(void);
void I2C1_restart(void);
void I2C1_stop(void);
void I2C1_idle(void);
unsigned char I2C1_ackstatus(void);
void I2C1_ack(void);
void I2C1_notack(void);
void I2C1_write(unsigned char byte);
unsigned char I2C1_read(void);


void PIC_ADC_init(void);
double PIC_ADC_voltage_read(unsigned char channel);
double PIC_ADC_temperature_read(unsigned char channel);
double TEMP_read(unsigned char channel);

void timer_setup(double seconds);
void RX_timer_setup(double seconds);

void BPG_init(void);
unsigned long long int EUI48_read(void);

void factory_reset(void);
void time_out_trap(void);

unsigned char incoming_RS232_poll(void);
unsigned char incoming_RS485_poll(void);
unsigned char incoming_Lithium_poll(void);
unsigned char incoming_Argo_poll(void);

unsigned char CAN_data_process(CAN_data_packet CAN_incoming_data);

double read_PCB_temperature(void);
void extract_fcoil_data(CAN_data_packet PGN_data_extract, unsigned short int buffer_index);
void extract_bank_data(CAN_data_packet PGN_data_extract, unsigned short int buffer_index);
void extract_BPG_data(CAN_data_packet PGN_data_extract, unsigned short int buffer_index);

void extract_BPG_new_parameters(CAN_data_packet PGN_data_extract, unsigned short int buffer_index);

void operate_switch(unsigned char position, unsigned char global_operation);
double scan_bank_current(unsigned char current_search);

void send_BPG_data(void);
void send_BPG_parameters(void);
void load_FRAM_into_globals(void);
void process_panic(CAN_data_packet PGN_data_extract);
unsigned short int check_for_generation(void);
void collate_alarms(void);
void copy_new_parameters(void);
void check_factory_reset_switch(void);
void write_log(unsigned char log_data);
void download_log(void);
void send_charge_control(unsigned char operation);
void start_boot(uint16_t applicationAddress);
void torqeedo_CAN_process(CAN_data_packet PGN_data_extract);
void send_BMS_CAN_command(unsigned char command);
void purge_offline_modules(void);
void send_panic_off(void);
void BMS_CAN_enumerate(void);
void BMS_RS485_enumerate(void);
unsigned char CRC8(unsigned char CRC_end);
unsigned char BMS_query(unsigned char battery_to_ask);
void power_options(void);
void extract_TQ_BMS_new_parameters(CAN_data_packet PGN_data_extract);
void TQ_device_send_data(unsigned char block_number);
void send_BMS_RS485_command(unsigned char command);
#endif
